-- MySQL dump 10.13  Distrib 5.6.35, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: dispatchApplication
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `carriers`
--

DROP TABLE IF EXISTS `carriers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carriers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carriers`
--

LOCK TABLES `carriers` WRITE;
/*!40000 ALTER TABLE `carriers` DISABLE KEYS */;
INSERT INTO `carriers` VALUES (1,'Werner Trucking'),(2,'Jimmy Farms'),(3,'james'),(4,'jojpo'),(5,'test'),(6,'876'),(7,'test34');
/*!40000 ALTER TABLE `carriers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `changeLog`
--

DROP TABLE IF EXISTS `changeLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `changeLog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `query` varchar(500) NOT NULL,
  `userID` int(11) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `changeLog`
--

LOCK TABLES `changeLog` WRITE;
/*!40000 ALTER TABLE `changeLog` DISABLE KEYS */;
INSERT INTO `changeLog` VALUES (1,'UPDATE users SET password=\'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\' WHERE ID=\'1\'',1,'2017-11-08 21:44:51'),(2,'UPDATE users SET password=\'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\' WHERE ID=\'1\'',1,'2017-11-08 21:47:17'),(3,'UPDATE users SET password=\'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\' WHERE ID=\'1\'',1,'2017-11-08 21:47:17'),(4,'UPDATE users SET password=\'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\' WHERE ID=\'1\'',1,'2017-11-08 21:51:50'),(5,'UPDATE users SET password=\'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\' WHERE ID=\'1\'',1,'2017-11-08 21:51:50'),(6,'UPDATE users SET password=\'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\' WHERE ID=\'1\'',1,'2017-11-08 21:54:03'),(7,'UPDATE users SET password=\'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\' WHERE ID=\'1\'',1,'2017-11-08 21:54:03'),(8,'UPDATE users SET password=\'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\' WHERE ID=\'1\'',1,'2017-11-08 21:55:00'),(9,'UPDATE users SET password=\'f8ea714d1ed85aa3c9b4928df4fc6c2045adc80820c4dee752142f84bc1ce077\' WHERE ID=\'1\'',0,'2017-11-09 15:27:27'),(10,'UPDATE users SET password=\'f8ea714d1ed85aa3c9b4928df4fc6c2045adc80820c4dee752142f84bc1ce077\' WHERE ID=\'1\'',0,'2017-11-09 15:27:30'),(11,'UPDATE users SET password=\'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\' WHERE ID=\'1\'',0,'2017-11-09 15:27:58'),(12,'UPDATE users SET password=\'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\' WHERE ID=\'1\'',0,'2017-11-09 15:31:40'),(13,'UPDATE users SET password=\'70fbc118d294c2a6b15da065841176185c59ccbac237e890aa96ee810e1259c1\' WHERE ID=\'1\'',1,'2017-11-09 15:35:03'),(14,'UPDATE users SET password=\'70fbc118d294c2a6b15da065841176185c59ccbac237e890aa96ee810e1259c1\' WHERE ID=\'1\'',1,'2017-11-09 19:46:27'),(15,'UPDATE users SET password=\'eb045d78d273107348b0300c01d29b7552d622abbc6faf81b3ec55359aa9950c\' WHERE ID=\'1\'',1,'2017-11-09 19:48:17'),(16,'UPDATE users SET fName=\'Josh\' WHERE ID=\'1\'',1,'2017-11-09 22:39:15'),(17,'UPDATE users SET fName=\'Joshua\' WHERE ID=\'1\'',1,'2017-11-09 22:46:12'),(18,'UPDATE users SET fName=\'Joshua\' WHERE ID=\'1\'',1,'2017-11-09 22:46:12'),(19,'UPDATE users SET fName=\'Joshua\' WHERE ID=\'1\'',1,'2017-11-09 22:46:12'),(20,'UPDATE users SET fName=\'Josh\' WHERE ID=\'1\'',1,'2017-11-09 22:46:22'),(21,'UPDATE users SET accountType=\'admin\' WHERE ID=\'1\'',1,'2017-11-10 14:35:44'),(22,'UPDATE users SET accountType=\'driver\' WHERE ID=\'1\'',1,'2017-11-10 14:35:50'),(23,'UPDATE users SET accountType=\'driver\' WHERE ID=\'1\'',1,'2017-11-10 14:35:50'),(24,'UPDATE users SET accountType=\'admin\' WHERE ID=\'1\'',1,'2017-11-10 14:35:54'),(25,'UPDATE users SET accountType=\'admin\' WHERE ID=\'1\'',1,'2017-11-10 14:35:54'),(26,'UPDATE users SET accountType=\'admin\' WHERE ID=\'1\'',1,'2017-11-10 14:35:54'),(27,'UPDATE users SET accountType=\'admin\' WHERE ID=\'1\'',1,'2017-11-10 14:35:54'),(28,'UPDATE users SET accountType=\'driver\' WHERE ID=\'3\'',1,'2017-11-10 16:22:28'),(29,'UPDATE users SET accountType=\'customer\' WHERE ID=\'3\'',1,'2017-11-10 16:22:39'),(30,'UPDATE users SET email=\'josh@yahoo.com\' WHERE ID=\'3\'',1,'2017-11-10 16:31:15'),(31,'UPDATE users SET name=\'Jimmy Farm\' WHERE ID=\'7\'',1,'2017-11-10 20:38:12'),(32,'UPDATE users SET name=\'Farm Main 2\' WHERE ID=\'3\'',1,'2017-11-13 15:21:35'),(33,'UPDATE users SET name=\'Farm Main 22\' WHERE ID=\'3\'',1,'2017-11-13 19:51:34'),(34,'UPDATE users SET name=\'Farm Main 22\' WHERE ID=\'3\'',1,'2017-11-13 19:51:35'),(35,'DELETE FROM users WHERE ID=\'22\'',1,'2017-11-15 15:05:12'),(36,'DELETE FROM users WHERE ID=\'9\'',1,'2017-11-15 15:05:17'),(37,'DELETE FROM users WHERE ID=\'7\'',1,'2017-11-15 15:27:35'),(38,'DELETE FROM users WHERE ID=\'14\'',1,'2017-11-15 16:59:12'),(39,'UPDATE users SET password=\'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\' WHERE ID=\'5\'',1,'2017-11-15 18:35:30'),(40,'UPDATE customers SET name=\'Farm MainY\' WHERE ID=\'3\'',0,'2017-11-16 18:57:46'),(41,'UPDATE users SET name=\'Farm MainY\' WHERE ID=\'\'',0,'2017-11-16 18:57:46'),(42,'UPDATE customers SET name=\'Tremer\' WHERE ID=\'9\'',0,'2017-11-16 18:59:40'),(43,'UPDATE users SET name=\'Tremer\' WHERE ID=\'23\'',0,'2017-11-16 18:59:40'),(44,'INSERT INTO carriers (name) VALUES (\'test34\')',0,'2017-11-16 20:14:59'),(45,'INSERT INTO users (name,email,password,accountType) VALUES (\'test34\', \'test34@outlook.com\', \'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855\', \'carrier\')',0,'2017-11-16 20:14:59'),(46,'UPDATE jobs SET name=\'Tester\' WHERE ID=\'1\'',0,'2017-11-21 15:59:44'),(47,'UPDATE jobs SET customerID=\'5\' WHERE ID=\'1\'',0,'2017-11-21 16:13:01'),(48,'UPDATE jobs SET gps=\'45192\' WHERE ID=\' 1\'',0,'2017-11-21 16:13:06'),(49,'UPDATE jobs SET status=\'1\' WHERE ID=\'1\'',0,'2017-11-21 18:48:28'),(50,'UPDATE jobs SET status=\'0\' WHERE ID=\'1\'',0,'2017-11-21 18:50:05'),(51,'UPDATE jobs SET status=\'1\' WHERE ID=\'1\'',0,'2017-11-21 18:51:19'),(52,'DELETE FROM jobs WHERE ID=\'6\'',0,'2017-11-21 18:53:44'),(53,'UPDATE jobs SET name=\'Testd\' WHERE ID=\'2\'',0,'2017-11-21 18:54:01'),(54,'DELETE FROM jobs WHERE ID=\'2\'',0,'2017-11-21 18:54:15'),(55,'UPDATE jobs SET name=\'testered\' WHERE ID=\'15\'',0,'2017-11-21 18:56:03'),(56,'UPDATE jobs SET name=\'tester\' WHERE ID=\'15\'',0,'2017-11-21 18:56:41'),(57,'UPDATE jobs SET gps=\'yup\' WHERE ID=\' 15\'',0,'2017-11-21 18:56:54'),(58,'UPDATE jobs SET customerID=\'9\' WHERE ID=\'  15\'',0,'2017-11-21 18:57:00'),(59,'UPDATE drivers SET name=\'Jimmers\' WHERE ID=\'2\'',0,'2017-11-21 20:35:59'),(60,'UPDATE users SET name=\'Jimmers\' WHERE ID=\'5\'',0,'2017-11-21 20:35:59'),(61,'UPDATE origins SET name=\'Bakken Transload Ross ND 587763\' WHERE ID=\'26\'',0,'2017-11-27 14:43:25'),(62,'UPDATE origins SET name=\'Bakken Transload Ross ND 58776\' WHERE ID=\'26\'',0,'2017-11-27 14:43:32'),(63,'UPDATE origins SET phone=\'43\' WHERE ID=\'26\'',0,'2017-11-27 14:52:19'),(64,'UPDATE origins SET phone=\'\' WHERE ID=\' 26\'',0,'2017-11-27 14:52:22'),(65,'INSERT INTO users (name,email,password,accountType,relatedID) VALUES (\'Harmen\', \'\', \'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\', \'admin\', \'0\')',0,'2017-12-04 19:17:44'),(66,'INSERT INTO users (name,email,password,accountType,relatedID) VALUES (\'Harmenre\', \'\', \'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\', \'admin\', \'0\')',0,'2017-12-04 19:17:50'),(67,'INSERT INTO users (name,email,password,accountType,relatedID) VALUES (\'tom\', \'\', \'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\', \'admin\', \'0\')',0,'2017-12-04 19:18:33'),(68,'INSERT INTO users (name,email,password,accountType) VALUES (\'jep\', \'Herrping@jdherring.com\', \'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855\', \'customer\')',0,'2017-12-04 21:25:38'),(69,'INSERT INTO customers (name,sAddress,city,state,zip,mileageRate,demurrageRate,userID) VALUE (\'jep\', \'50023\', \'Fargo\', \'ND\', \'58754\', \'\', \'\', \'30\')',0,'2017-12-04 21:25:38'),(70,'INSERT INTO users (name,email,password,accountType) VALUES (\'yerp\', \'Josh@yerp.com\', \'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855\', \'customer\')',0,'2017-12-04 21:29:50'),(71,'INSERT INTO customers (name,sAddress,city,state,zip,mileageRate,demurrageRate,userID) VALUE (\'yerp\', \'543\', \'543\', \'CT\', \'543\', \'\', \'\', \'31\')',0,'2017-12-04 21:29:50'),(72,'INSERT INTO users (name,email,password,accountType) VALUES (\'yex\', \'josh@yex.com\', \'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855\', \'customer\')',0,'2017-12-04 21:30:32'),(73,'INSERT INTO customers (name,sAddress,city,state,zip,mileageRate,demurrageRate,userID) VALUE (\'yex\', \'tes\', \'tes\', \'CO\', \'6543\', \'\', \'\', \'32\')',0,'2017-12-04 21:30:32'),(74,'INSERT INTO users (name,email,password,accountType) VALUES (\'itim\', \'itim@hotmail.com\', \'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855\', \'customer\')',0,'2017-12-04 22:03:19'),(75,'INSERT INTO customers (name,sAddress,city,state,zip,mileageRate,demurrageRate,userID) VALUE (\'itim\', \'543\', \'543\', \'DC\', \'543\', \'\', \'\', \'33\')',0,'2017-12-04 22:03:19'),(76,'UPDATE jobs SET customerID=\'4\' WHERE ID=\'27\'',0,'2017-12-07 15:53:26'),(77,'DELETE FROM jobs WHERE ID=\'28\'',0,'2017-12-07 18:52:59'),(78,'UPDATE jobs SET status=\'1\' WHERE ID=\'27\'',0,'2017-12-07 18:55:08'),(79,'UPDATE jobs SET status=\'0\' WHERE ID=\'27\'',0,'2017-12-07 18:57:50'),(80,'UPDATE jobs SET status=\'1\' WHERE ID=\'27\'',0,'2017-12-07 18:57:54'),(81,'UPDATE jobs SET status=\'0\' WHERE ID=\'27\'',0,'2017-12-07 18:57:56'),(82,'UPDATE jobs SET status=\'1\' WHERE ID=\'27\'',0,'2017-12-07 18:59:23'),(83,'UPDATE jobs SET status=\'0\' WHERE ID=\'27\'',0,'2017-12-07 18:59:24'),(84,'UPDATE jobs SET status=\'1\' WHERE ID=\'27\'',0,'2017-12-07 18:59:26'),(85,'UPDATE jobs SET status=\'0\' WHERE ID=\'27\'',0,'2017-12-07 18:59:27'),(86,'UPDATE trucks SET name=\'test2\' WHERE ID=\'undefined\'',0,'2017-12-08 20:35:43'),(87,'UPDATE trucks SET name=\'test32\' WHERE ID=\'undefined\'',0,'2017-12-08 20:36:16'),(88,'UPDATE trucks SET name=\'test32\' WHERE ID=\'undefined\'',0,'2017-12-08 20:36:16'),(89,'UPDATE trucks SET name=\'test32\' WHERE ID=\'undefined\'',0,'2017-12-08 20:36:16'),(90,'UPDATE trucks SET name=\'test4\' WHERE ID=\'1\'',0,'2017-12-08 20:39:01'),(91,'UPDATE trucks SET name=\'test5\' WHERE ID=\'1\'',0,'2017-12-08 20:40:36'),(92,'UPDATE trucks SET name=\'test53\' WHERE ID=\'3\'',0,'2017-12-08 20:41:22'),(93,'UPDATE trucks SET name=\'test53\' WHERE ID=\'3\'',0,'2017-12-08 20:41:22'),(94,'UPDATE trailers SET name=\'test13\' WHERE ID=\'2\'',0,'2017-12-08 20:45:15'),(95,'UPDATE products SET name=\'100meshe\' WHERE ID=\'6\'',0,'2017-12-08 20:49:53'),(96,'UPDATE drivers SET carrierID=\'1\' WHERE ID=\'2\'',0,'2017-12-08 21:10:05'),(97,'UPDATE users SET accountType=\'carrier\' WHERE ID=\'10\'',0,'2017-12-11 14:34:00'),(98,'UPDATE users SET accountType=\'carrier\' WHERE ID=\'10\'',0,'2017-12-11 14:34:00'),(99,'UPDATE users SET accountType=\'carrier\' WHERE ID=\'10\'',0,'2017-12-11 14:34:00'),(100,'UPDATE users SET accountType=\'carrier\' WHERE ID=\'10\'',0,'2017-12-11 14:34:00'),(101,'UPDATE users SET accountType=\'carrier\' WHERE ID=\'10\'',0,'2017-12-11 14:34:00'),(102,'INSERT INTO products (name) VALUES (\'Test\')',0,'2017-12-11 15:11:23'),(103,'DELETE FROM products WHERE ID=\'25\'',0,'2017-12-11 15:11:28'),(104,'DELETE FROM origins WHERE ID=\'32\'',0,'2017-12-11 15:18:54'),(105,'INSERT INTO loads (carrierID,jobID,originID,productID,productPO,miles,quantity,directions,notes,truckID,trailerID,driverID,status) VALUES (\'1\', \'15\', \'3\', \'4\', \'54\', \'54\', \'54\', \'54\', \'54\', \'\', \'0\', \'1\', \'0\')',0,'2017-12-11 15:48:22'),(106,'UPDATE drivers SET jobStatus=\'1\'',0,'2017-12-11 15:48:22'),(107,'UPDATE drivers SET jobStatus=\'0\' WHERE ID=\'1\'',0,'2017-12-11 15:50:19'),(108,'UPDATE loads SET driverID=\'4\' WHERE ID=\'1\'',0,'2017-12-11 15:50:19'),(109,'UPDATE loads SET status=\'0\' WHERE ID=\'1\'',0,'2017-12-11 15:50:19'),(110,'UPDATE drivers SET status=\'0\',jobStatus=\'3\' WHERE ID=\'4\'',0,'2017-12-11 16:16:18'),(111,'INSERT INTO mileageRates (customerID, rangeBottom, rangeTop) VALUES (\'1\', \'0\', \'30\')',0,'2017-12-12 15:13:52'),(112,'UPDATE mileageRates SET rangeTop=\'50\' WHERE ID=\'6\'',0,'2017-12-12 16:16:47'),(113,'UPDATE mileageRates SET rangeTop=\'50\' WHERE ID=\'6\'',0,'2017-12-12 16:16:47'),(114,'UPDATE mileageRates SET rangeTop=\'50\' WHERE ID=\'6\'',0,'2017-12-12 16:16:47'),(115,'UPDATE mileageRates SET rate=\'50.00\' WHERE ID=\'6\'',0,'2017-12-12 16:18:31'),(116,'UPDATE mileageRates SET rate=\'50.00\' WHERE ID=\'6\'',0,'2017-12-12 16:18:31'),(117,'UPDATE mileageRates SET rate=\'50.00\' WHERE ID=\'6\'',0,'2017-12-12 16:18:31'),(118,'UPDATE mileageRates SET rate=\'50.00\' WHERE ID=\'6\'',0,'2017-12-12 16:18:31'),(119,'UPDATE mileageRates SET rate=\'50.00\' WHERE ID=\'6\'',0,'2017-12-12 16:18:31'),(120,'UPDATE mileageRates SET rate=\'50.00\' WHERE ID=\'6\'',0,'2017-12-12 16:18:31'),(121,'UPDATE mileageRates SET rate=\'50.00\' WHERE ID=\'6\'',0,'2017-12-12 16:18:31'),(122,'UPDATE mileageRates SET rate=\'50.00\' WHERE ID=\'6\'',0,'2017-12-12 16:18:31'),(123,'UPDATE users SET email=\'test5@yahoo.com\' WHERE ID=\'\'',0,'2017-12-12 16:22:11'),(124,'UPDATE customers SET zip=\'58104\' WHERE ID=\'1\'',0,'2017-12-12 16:25:11'),(125,'INSERT INTO mileageRates (customerID, rangeBottom, rangeTop) VALUES (\'1\', \'undefined\', \'undefined\')',0,'2017-12-12 16:37:40'),(126,'UPDATE mileageRates SET rate=\'50.2\' WHERE ID=\'6\'',0,'2017-12-12 16:37:40'),(127,'UPDATE mileageRates SET rate=\'50.22\' WHERE ID=\'6\'',0,'2017-12-12 16:40:28'),(128,'UPDATE mileageRates SET rate=\'50.22\' WHERE ID=\'6\'',0,'2017-12-12 16:40:28'),(129,'UPDATE mileageRates SET rangeTop=\'60\' WHERE ID=\'6\'',0,'2017-12-12 16:40:38'),(130,'UPDATE mileageRates SET rangeTop=\'60\' WHERE ID=\'6\'',0,'2017-12-12 16:40:38'),(131,'UPDATE mileageRates SET rangeTop=\'60\' WHERE ID=\'6\'',0,'2017-12-12 16:40:38'),(132,'UPDATE mileageRates SET rangeTop=\'60\' WHERE ID=\'6\'',0,'2017-12-12 16:40:38'),(133,'UPDATE mileageRates SET rangeTop=\'60\' WHERE ID=\'6\'',0,'2017-12-12 16:40:38'),(134,'UPDATE mileageRates SET rangeTop=\'60\' WHERE ID=\'6\'',0,'2017-12-12 16:40:38'),(135,'INSERT INTO jobs (name,customerID,gps,status) VALUES (\'5453\', \'4\', \'5436543\', \'0\')',0,'2017-12-12 16:56:18'),(136,'UPDATE users SET password=\'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\' WHERE ID=\'8\'',0,'2017-12-15 15:01:14'),(137,'UPDATE users SET password=\'44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9\' WHERE ID=\'8\'',0,'2017-12-15 15:01:14'),(138,'INSERT INTO loads (carrierID,jobID,originID,productID,productPO,miles,quantity,directions,notes,truckID,trailerID,driverID,status) VALUES (\'1\', \'30\', \'1\', \'1\', \'55\', \'55\', \'55\', \'55\', \'55\', \'\', \'0\', \'0\', \'0\')',0,'2017-12-15 15:09:21'),(139,'UPDATE drivers SET jobStatus=\'1\'',0,'2017-12-15 15:09:21'),(140,'INSERT INTO loads (carrierID,jobID,originID,productID,productPO,miles,quantity,directions,notes,truckID,trailerID,driverID,status) VALUES (\'1\', \'15\', \'4\', \'4\', \'43\', \'43\', \'43\', \'43\', \'43\', \'\', \'0\', \'0\', \'0\')',0,'2017-12-15 15:10:00'),(141,'UPDATE drivers SET jobStatus=\'1\'',0,'2017-12-15 15:10:00'),(142,'INSERT INTO loads (carrierID,jobID,originID,productID,productPO,miles,quantity,directions,notes,truckID,trailerID,driverID,status) VALUES (\'1\', \'15\', \'4\', \'4\', \'11\', \'11\', \'11\', \'11\', \'11\', \'\', \'0\', \'0\', \'0\')',0,'2017-12-15 15:12:35'),(143,'UPDATE drivers SET jobStatus=\'1\'',0,'2017-12-15 15:12:35'),(144,'INSERT INTO trucks (name,carrierID,type) VALUES (\'Test6\', \'1\', \'\')',0,'2017-12-21 19:24:00'),(145,'INSERT INTO trucks (name,carrierID,type) VALUES (\'Test7\', \'1\', \'\')',0,'2017-12-21 19:27:34'),(146,'INSERT INTO trucks (name,carrierID,type) VALUES (\'Test 8\', \'1\', \'Test1\')',0,'2017-12-21 19:28:26'),(147,'INSERT INTO trucks (name,carrierID,type) VALUES (\'Test9\', \'1\', \'Test1\')',0,'2017-12-21 19:30:43'),(148,'INSERT INTO trucks (name,carrierID,type) VALUES (\'Test10\', \'5\', \'Test4\')',0,'2017-12-21 19:59:54'),(149,'INSERT INTO jobs (name,customerID,gps,status) VALUES (\'TEsters\', \'1\', \'759032\', \'0\')',0,'2017-12-21 20:27:06'),(150,'UPDATE drivers SET carrierID=\'2\' WHERE ID=\'8\'',0,'2017-12-21 21:28:32'),(151,'INSERT INTO truckTypes (name) VALUES (\'\')',0,'2018-01-05 18:44:59'),(152,'INSERT INTO truckTypes (name) VALUES (\'\')',0,'2018-01-05 18:47:43'),(153,'INSERT INTO truckTypes (name) VALUES (\'Concrete\')',0,'2018-01-05 18:50:15'),(154,'INSERT INTO trucks (name,carrierID,type) VALUES (\'Test11\', \'1\', \'2\')',0,'2018-01-05 18:58:18'),(155,'INSERT INTO truckTypes (name) VALUES (\'Fuel\')',0,'2018-01-05 19:02:32'),(156,'UPDATE trucks SET type=\'3\' WHERE ID=\'9\'',0,'2018-01-05 19:21:19'),(157,'UPDATE trucks SET name=\'Rock Truck\' WHERE ID=\'3\'',0,'2018-01-05 20:52:04'),(158,'UPDATE truckTypes SET name=\'Rock Truck\' WHERE ID=\'3\'',0,'2018-01-05 20:52:48'),(159,'UPDATE trucks SET type=\'2\' WHERE ID=\'2\'',0,'2018-01-05 20:54:44');
/*!40000 ALTER TABLE `changeLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `sAddress` varchar(400) NOT NULL,
  `city` varchar(200) NOT NULL,
  `state` varchar(2) NOT NULL,
  `zip` int(5) NOT NULL,
  `mileageRate` float NOT NULL,
  `demurrageRate` float NOT NULL,
  `userID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'A&W Rootbear','5002 16th Ave S','Fargo','ND',58104,23.1,32.5,NULL),(2,'A&W Rootbeer','1414','Wahpeton','ND',58075,12.5,12.5,NULL),(3,'Farm MainY','2323 E Ealk St','Fargo','ND',58104,23.2,23.2,NULL),(4,'Jimmy Farms','','Milnor','ND',58105,34.3,43.4,NULL),(5,'Bo Jangles','','test','ME',58585,12.3,34.1,NULL),(6,'jo','tester','test','AL',58045,343.2,43.4,NULL),(7,'test','test','test','CO',58075,323,32,NULL),(8,'Tester','ye','ye','AR',56456,43,43,6),(9,'Tremer','123 ABC Street','Wahpeton','ID',58075,54,54,23),(10,'tre','tre','tre','AR',543,54,54,1),(11,'trem','tre','tre','AR',543,54,54,25),(12,'','','','St',0,0,0,25),(13,'jep','50023','Fargo','ND',58754,0,0,30),(14,'yerp','543','543','CT',543,0,0,31),(15,'yex','tes','tes','CO',6543,0,0,32),(16,'itim','543','543','DC',543,0,0,33),(17,'Test22','','','',0,0,0,37),(18,'Test22','123 ABC Street','Fargo','',58075,0,0,38),(19,'Test23','','','',0,0,0,39),(20,'Test24','123 ABC Street','Fargo','',58075,0,0,40);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `demurrageRates`
--

DROP TABLE IF EXISTS `demurrageRates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demurrageRates` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `customerID` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `notes` longtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `demurrageRates`
--

LOCK TABLES `demurrageRates` WRITE;
/*!40000 ALTER TABLE `demurrageRates` DISABLE KEYS */;
/*!40000 ALTER TABLE `demurrageRates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drivers`
--

DROP TABLE IF EXISTS `drivers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drivers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `carrierID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `jobStatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drivers`
--

LOCK TABLES `drivers` WRITE;
/*!40000 ALTER TABLE `drivers` DISABLE KEYS */;
INSERT INTO `drivers` VALUES (1,'John Appleton',1,4,1,1),(2,'Jimmers',1,5,0,0),(3,'jos',2,0,0,1),(4,'Johnny',1,19,1,1),(5,'johnnys',0,0,0,1),(7,'hyper',3,22,0,1),(8,'test13',2,24,1,1);
/*!40000 ALTER TABLE `drivers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobProduct`
--

DROP TABLE IF EXISTS `jobProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobProduct` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `jobID` int(11) NOT NULL,
  `totalRequired` int(30) NOT NULL,
  `originID` int(11) NOT NULL,
  `miles` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `currentQuantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobProduct`
--

LOCK TABLES `jobProduct` WRITE;
/*!40000 ALTER TABLE `jobProduct` DISABLE KEYS */;
INSERT INTO `jobProduct` VALUES (1,14,525,15,0,1,543),(2,15,525,15,0,4,525),(3,15,5253,15,0,1,500),(4,16,543,0,0,1,NULL),(5,17,5435,0,0,1,NULL),(6,18,5435,0,0,1,NULL),(7,19,5435,0,0,1,NULL),(8,20,5435,0,0,1,NULL),(9,26,6456,0,0,1,NULL),(10,27,523,0,0,12,NULL),(11,28,600,0,0,9,NULL),(12,29,500,0,0,15,NULL),(13,30,5435,0,0,1,NULL),(14,31,6000,0,0,1,0),(15,32,4324,0,0,1,0);
/*!40000 ALTER TABLE `jobProduct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `customerID` int(11) NOT NULL,
  `gps` varchar(500) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` VALUES (1,'Tester',5,'45192',1),(3,'test',1,'test',0),(4,'test',1,'test',0),(5,'test',1,'test',0),(7,'test',1,'test',0),(8,'test',1,'test',0),(9,'test',1,'test',0),(10,'test',1,'test',0),(11,'test',1,'test',0),(12,'test',1,'test',0),(13,'test',1,'test',0),(14,'test',1,'test',0),(15,'tester',9,'yup',0),(24,'654',1,'654',0),(25,'543',1,'543',0),(26,'Test2',4,'502532-234',0),(27,'Test3',4,'6543654',0),(29,'herk',1,'herk',0),(30,'tes',4,'test',0),(31,'5453',4,'5436543',0),(32,'TEsters',1,'759032',0);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loads`
--

DROP TABLE IF EXISTS `loads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loads` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `invoice` int(11) DEFAULT NULL,
  `carrierID` int(11) NOT NULL,
  `truckID` int(11) DEFAULT NULL,
  `trailerID` int(11) DEFAULT NULL,
  `driverID` int(11) DEFAULT NULL,
  `miles` float NOT NULL,
  `productID` int(11) NOT NULL,
  `originID` int(11) NOT NULL,
  `quantity` float NOT NULL,
  `scaleTicket` int(11) NOT NULL,
  `jobID` int(11) NOT NULL,
  `loadTimeStart` datetime DEFAULT NULL,
  `loadTimeStop` datetime DEFAULT NULL,
  `unloadTimeStart` datetime DEFAULT NULL,
  `unloadTimeStop` datetime DEFAULT NULL,
  `demurrageTimeStart` datetime DEFAULT NULL,
  `demurrageTimeStop` datetime DEFAULT NULL,
  `notes` longtext NOT NULL,
  `directions` longtext NOT NULL,
  `productPO` int(11) DEFAULT NULL,
  `currentQuantity` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loads`
--

LOCK TABLES `loads` WRITE;
/*!40000 ALTER TABLE `loads` DISABLE KEYS */;
INSERT INTO `loads` VALUES (1,'2017-12-11 15:48:22',0,1,0,0,4,54,4,3,54,0,15,NULL,NULL,NULL,NULL,NULL,NULL,'54','54',54,NULL,1),(2,'2017-12-15 15:09:21',0,1,0,0,4,55,1,1,55,0,30,NULL,NULL,NULL,NULL,NULL,NULL,'55','55',55,NULL,0),(3,'2017-12-15 15:10:00',0,1,0,0,0,43,4,4,43,0,15,NULL,NULL,NULL,NULL,NULL,NULL,'43','43',43,NULL,0),(4,'2017-12-15 15:12:35',NULL,1,3,0,4,11,4,4,11,0,15,NULL,NULL,NULL,NULL,NULL,NULL,'11','11',11,NULL,2);
/*!40000 ALTER TABLE `loads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mileageRates`
--

DROP TABLE IF EXISTS `mileageRates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mileageRates` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `customerID` int(11) DEFAULT NULL,
  `rangeBottom` int(11) DEFAULT NULL,
  `rangeTop` int(11) DEFAULT NULL,
  `rate` float DEFAULT '50',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mileageRates`
--

LOCK TABLES `mileageRates` WRITE;
/*!40000 ALTER TABLE `mileageRates` DISABLE KEYS */;
INSERT INTO `mileageRates` VALUES (1,5,0,30,200),(2,4,0,30,200),(3,9,0,43,437),(4,0,0,0,1.2),(5,0,0,0,1.2),(6,1,0,60,50.22),(7,1,0,0,50);
/*!40000 ALTER TABLE `mileageRates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `origins`
--

DROP TABLE IF EXISTS `origins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `origins` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `gps` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `phone` varchar(300) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `origins`
--

LOCK TABLES `origins` WRITE;
/*!40000 ALTER TABLE `origins` DISABLE KEYS */;
INSERT INTO `origins` VALUES (1,'Carbo Beulah 58523','47.2766851964,-101.872722429','','701-948-2262 or 406-690-1991'),(2,'Carbo Douglas 59218','47.8592976901,-101.503169336','','Rusty-701-460-0692'),(3,'Carbo Lansford 58750','48.6279675035,-101.372531281','',''),(4,'Carbo Underwood 58576','47.4566168442,-101.134791424','',''),(5,'Rockwater Williston Nd 58801','48.1441184107,-103.612606337','','701-580-8568'),(6,'Santrol Blaisdell 58718','48.3383009945,-102.082521475','','701-313-0218'),(7,'Santrol Burlington 58722','48.3234462919,-101.514874686','',''),(8,'Santrol Stanley 58784','48.3178630487,-102.396881857','','701-319-9385'),(9,'Source Energy Berthold ND 58718','48.3293493982,-101.767376064','8850 310th NW Street','(701) 818-2637'),(10,'Uniman, New Town 58763','47.9767211332,-102.472825291','','(701) 627-2088'),(11,'Watco(Dore)59221','47.9194568035,-104.031655302','','406-478-9065'),(12,'Wild cat Faiview 59221','47.8515401373,-104.032078751','3019 161st Ave. NW','(406) 478-9337'),(13,'GMHR Gascoyne 58653','46.1285542,-103.046061','12803 90th St SW ','(701) 275-8212'),(14,'Dakota Transload  Stanton Nd 58571','47.3138256299,-101.38672577','300 Ball Park St.',''),(15,'NDPS Minot ND 58701','48.2399297477,-101.252876096','4900 Railway Ave',''),(16,'Omnitrax, Bainville 59212','48.1544395419, -104.243414668','823 Hwy 2','406-769-2114'),(17,'United Grain, Culbertson MT. 59218','48.1414170678, -104.516331161','111 1st Ave SW','406-366-6570'),(18,'North Star Fairview 59221','48.147024048,-104.222007136','16105 32nd ST NW., Fairview','701-354-5457'),(19,'Wildcat New Town 58763','47.9780660001,-102.491811','New Town','JR Lium 406-559-6298'),(20,'CTAP New Town 58763','47.9749268001, -102.4509108','New Town','Adam 702-917-2872'),(21,'Dakota Gold Transload Plaza','48.0603905171,-102.607558898','Plaza','Cody Moe (CEO) 605-212-1974 (try first),Facility Office 701-509-6958 (try second), Andy Marshall (Site Manager) 360-259-0257 (try third), Brad Blesie (O) 203-554-3195 / (M) 646-774-1032'),(22,'Red River Supply Williston 58801','48.1446076424, -103.581147762','Williston','701-570-2090'),(23,'EOG Resources stanley 58784','48.3296603718, -102.347219189','Stanley','Scale House: 701-240-6572'),(24,'Sand Solutions Underwood 58576','47.4566168442,-101.134791424','Underwood','Tanner 701-500-1900, Brian 701-500-2592'),(25,'Santrol Manitou 58776','48.3376005608, -102.638997426','',''),(26,'Bakken Transload Ross ND 58776','48.3134520845, -102.528391635','',''),(27,'Halliburton Richardton ND 58652','46.8808541272, -102.331502773','',''),(28,'Epic Sands Zap ND 58580','','',''),(29,'Cal Frac Yard Williston ND 58801','48.1447420829, -103.721859942','',''),(30,'Precise Yard Minot ND 58701','48.2270661864, -101.268683044','',''),(31,'Precise Yard Arnegard ND 58835','47.7837913141, -103.426114303','','');
/*!40000 ALTER TABLE `origins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passwordReset`
--

DROP TABLE IF EXISTS `passwordReset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passwordReset` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `userKey` varchar(200) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passwordReset`
--

LOCK TABLES `passwordReset` WRITE;
/*!40000 ALTER TABLE `passwordReset` DISABLE KEYS */;
INSERT INTO `passwordReset` VALUES (1,1,'4JVisVVaq8wphBcGNFCAm12tmWjq0eREoUHftnJEPxesxX1wsm','2017-11-09 15:14:21'),(2,1,'sGU2MGWJuJnoBoAxj6cPnusaM4kEvsmXF4AvajtWAaPlHoLVbk','2017-11-09 15:14:22'),(3,1,'nVOoBj4b7eQkV4qr1kWtAbI9wgBq03ASaVrYAo8SXxU5xZgV1q','2017-11-09 15:15:39'),(4,1,'OqYWHTNoc8f1JQoL6LEiNEZAAdiVMruFTPKNjSWOIhTHzmkNCt','2017-11-09 15:19:37');
/*!40000 ALTER TABLE `passwordReset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'Frac Sand'),(2,'20/40w'),(4,'40/70w'),(5,'30/50w'),(6,'100meshe'),(7,'20/40 Resin'),(8,'30/50 Resin'),(9,'40/70 Resin'),(10,'20/40 isp'),(11,'40/70 isp'),(12,'30/50 isp'),(14,'40/70 ceramic'),(15,'30/50 ceramic'),(16,'other'),(17,'Aggregate'),(18,'Class 5'),(19,'Class 13'),(20,'washed sand'),(21,'screened sand'),(22,'Rock'),(23,'20/40 ceramic');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trailers`
--

DROP TABLE IF EXISTS `trailers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trailers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `truckID` int(11) NOT NULL,
  `carrierID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trailers`
--

LOCK TABLES `trailers` WRITE;
/*!40000 ALTER TABLE `trailers` DISABLE KEYS */;
INSERT INTO `trailers` VALUES (1,'test',1,3),(2,'test13',1,3);
/*!40000 ALTER TABLE `trailers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `truckTypes`
--

DROP TABLE IF EXISTS `truckTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `truckTypes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `truckTypes`
--

LOCK TABLES `truckTypes` WRITE;
/*!40000 ALTER TABLE `truckTypes` DISABLE KEYS */;
INSERT INTO `truckTypes` VALUES (2,'Concrete'),(3,'Rock Truck');
/*!40000 ALTER TABLE `truckTypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trucks`
--

DROP TABLE IF EXISTS `trucks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trucks` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `carrierID` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trucks`
--

LOCK TABLES `trucks` WRITE;
/*!40000 ALTER TABLE `trucks` DISABLE KEYS */;
INSERT INTO `trucks` VALUES (1,'test5',3,NULL,'3'),(2,'test1',1,NULL,'2'),(3,'Rock Truck',2,NULL,'3'),(4,'Test6',1,NULL,'3'),(5,'Test7',1,NULL,'3'),(6,'Test 8',1,NULL,'3'),(7,'Test9',1,NULL,'3'),(8,'Test10',5,NULL,'3'),(9,'Test11',1,NULL,'3');
/*!40000 ALTER TABLE `trucks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(500) NOT NULL,
  `accountType` varchar(100) NOT NULL DEFAULT 'guest',
  `relatedID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Josh','josh.h@results-unlimited.com','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','admin',0),(2,'John Fealgree','joshua.herring@outlook.com','','driver',1),(3,'Farm Main 22','josh@yahoo.com','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','customer',0),(4,'John Appleton','john@yahoo.com','','driver',1),(5,'Jimmers','jocker@yahoo.com','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','driver',1),(6,'Tester','Jon@digitalExpress.com','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','carrier',0),(8,'Andy Obermoller','andy.o@results-inlimited.com','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','dispatch',0),(10,'Bo Jangles','josh@domain.com','d6b0e30b57dfeb5cfb06da9f2f875d1093eae02dbd772ed6312cc4f277688a03','carrier',0),(13,'test','test1@outlook.com','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','customer',0),(15,'james','josh.h5@results-unlimited.com','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','carrier',0),(16,'jojpo','josh.h2@results-unlimited.com','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','carrier',0),(17,'test','josh.2h@results-unlimited.com','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','carrier',0),(18,'876','josh.h3@results-unlimited.com','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','carrier',0),(19,'Johnny','josh.42h@results-unlimited.com','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','driver',0),(20,'johnnys','josfdh.h@results-unlimited.com','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','driver',0),(21,'utum','josh.!h@results-unlimited.com','df1a5c427436c757f2f17005bfb9593003c4b9579a1689194cf8c1abe8e0a01f','driver',0),(23,'Tremer','JamesDean@outlook4.com','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','customer',0),(24,'test13','caseys@outlook.com','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','driver',0),(25,'tre','','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','customer',0),(26,'test34','test34@outlook.com','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','carrier',0),(27,'Harmen','','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','admin',0),(28,'Harmenre','','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','admin',0),(29,'tom','','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','admin',0),(30,'jep','Herrping@jdherring.com','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','customer',0),(31,'yerp','Josh@yerp.com','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','customer',0),(32,'yex','josh@yex.com','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','customer',0),(33,'itim','itim@hotmail.com','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','customer',0),(34,'Test Dispatcher','dispatcher@valleygrain.com','44caa7ba0e7277a84016b357b7439795e0796808de0edda7668131695023f5a9','dispatch',0),(35,'Test88','test87778@domain.com','079e01f045991db79fcc9d41e4afa41375b5d4264a865fe44675af0ab6812be5','dispatch',0),(36,'dispatch1','dispacth1@domain.com','079e01f045991db79fcc9d41e4afa41375b5d4264a865fe44675af0ab6812be5','dispatch',0),(37,'Test22','test22@domain.com','079e01f045991db79fcc9d41e4afa41375b5d4264a865fe44675af0ab6812be5','customer',0),(38,'Test22','test224@domain.com','079e01f045991db79fcc9d41e4afa41375b5d4264a865fe44675af0ab6812be5','customer',0),(39,'Test23','test23@domain.com','079e01f045991db79fcc9d41e4afa41375b5d4264a865fe44675af0ab6812be5','customer',0),(40,'Test24','test24@domain.com','079e01f045991db79fcc9d41e4afa41375b5d4264a865fe44675af0ab6812be5','ND',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-05 16:19:21
