<?php
    require ("assets/php/elements/require.php");
    session_start();
?>
<?php
    $key = $_GET["key"];
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/header.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/appHeader.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/footer.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/modal.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/block.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/scripts.php");

?>

    <!DOCTYPE html>
    <html lang="en">
        <?php head("Dispatch Application Dashboard");?>

        <body class="dark pace-done">
            <div class="app" id="app">
                <div class="center-block w-xxl w-auto-xs p-y-md">
                    <div class="app-header white box-shadow">
                        <div class="navbar flex-row align-items-center justify-content-between">
                            <!-- Page title - Bind to $state's title -->
                            <div class="text-center center-block" id="pageTitle">
                                <h1 class="text-center center-block">Dispatchr Sign In</h1>
                            </div>
                        </div>
                    </div>
                    <div id="alert" class="success"></div>
                    <div id="error" class="danger"></div>
                    <?php modal(); ?>
                    <div class="app-body">
                        <div class="signIn"></div>
                        <div class="padding">
                            <div class="box">
                                <h2 class="text-center" style="padding-top: .5em;">Admin Login</h2>
                                <hr>
                                <p class="text-center">email: josh.h@results-unlimited.com</p>
                                <p class="text-center" style="padding-bottom: .5em;">password: Penguin45!</p>
                            </div>
                            <div class="box">
                                <h2 class="text-center" style="padding-top: .5em;">Dispatcher Login</h2>
                                <hr>
                                <p class="text-center">email: Andy.O@Results-Inlimited.Com</p>
                                <p class="text-center" style="padding-bottom: .5em;">password: Penguin45!</p>
                            </div>
                            <div class="box">
                                <h2 class="text-center" style="padding-top: .5em;">Driver Login</h2>
                                <hr>
                                <p class="text-center">email: josh.42h@results-unlimited.com</p>
                                <p class="text-center"style="padding-bottom: .5em;">password: Penguin45!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php scripts(); ?>
            <?php footer(); ?>
            <?php
            if(isset($key)){
                $hashKey = hash('sha256',$key);
                $query = "SELECT * FROM passwordReset WHERE userKey='$hashKey'";
                if (mysqli_num_rows(mysqli_query($db, $query)) === 1) {
                    $row = mysqli_fetch_assoc(mysqli_query($db,$query));
                    $query = "DELETE FROM passwordReset WHERE userKey='$hashKey'"
                    ?>
                    <script>
                        $.ajax({
                            url: "assets/php/modules/passwordChange/module.php",
                            method: "GET",
                            success: function(response){
                                $(".modal-body").html(response);
                                $(".modal-body").attr('id',"<?= $row['userID']?>");
                                $('#modal').modal('toggle');
                            }
                        });
                        $("#submit").click(function(){
                            window.location.href = "/";
                        });
                    </script>
                <?php
                    $query = "DELETE FROM passwordReset WHERE userKey='$hashKey'";
                    mysqli_query($db,$query);
                }
            }?>

            <script>
                $.ajax({
                    url: "assets/php/modules/login/module.php",
                    method: "GET",
                    success: function(response){
                        $(".signIn").html(response);
                    }
                });
            </script>
        </body>
    </html>
