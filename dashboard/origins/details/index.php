<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 1:57 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    rrequire_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $email = $_SESSION['userInfo']['email'];
    $name = $_SESSION['userInfo']['name'];
    $account = $_SESSION['userInfo']['accountType'];
    $ID = $_SESSION['userInfo']['ID'];
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/header.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/appHeader.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/block.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/footer.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/scripts.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/modal.php");
    includes($_SESSION['userInfo']);
    if($_SESSION['userInfo']['accountType'] === "driver" || $userInfo['accountType'] === "dispatch"){
        $query = "SELECT * FROM drivers WHERE userID='$ID'";
        $driverArray = mysqli_fetch_assoc(mysqli_query($db,$query));
        $driverID = $driverArray['ID'];
        $query = "SELECT loads.ID,loads.date,jobs.name FROM loads JOIN jobs ON loads.jobID=jobs.ID WHERE loads.driverID='$driverID'";
        $loadArray = mysqli_fetch_array(mysqli_query($db,$query),MYSQLI_ASSOC);
    }
    $ID = $_GET["ID"];
    $query = "SELECT * FROM origins WHERE ID='$ID'";
    $loadInfo = mysqli_fetch_assoc(mysqli_query($db,$query));
    $loadName = $loadInfo['name'];
?>

    <!DOCTYPE html>
    <html lang="en">
        <?php head("Dashboard");?>
        <body class="dark">
            <div class="app" id="app">
                <div id="aside" class="app-aside modal nav-dropdown">
                    <?php nav($name); ?>
                </div>
                <?php modal(); ?>
                <div id="content" class="app-content box-shadow-z0" role="main">

                    <?php appHeader($loadName. " Details"); ?>


                    <?php footer(); ?>
                    <div ui-view class="app-body" id="view">
                        <div class="row flex-wrap">
                            <div class="col">
                                <?php block("details");?>
                            </div>
                            <div class="col commands">
                                <div class="padding">
                                    <div class="box">
                                        <div class="white">
                                            <button class="btn danger delete">Delete Origin</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row flex-wrap">
                            <div class="col">
                                <?php block("optionalContainer");?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php scripts(); ?>
            <script>
                $(function(){
                    $("#new").hide();
                    $(".cancel").hide();
                    data = "ID="+<?=$ID?>;
                    $.ajax({
                        url: "/assets/php/modules/origin/details/module.php",
                        method: "POST",
                        data: data,
                        success: function (response) {
                            $("#details").html(response);
                        }
                    });

                    $(".close").click(function(){
                        $(".modal-body").html("");
                        $('#modal').modal('toggle');
                        $(".modal-header").html("");
                    });
                    $(".delete").click(function(){
                        $.ajax({
                            url: "/assets/php/modules/origin/delete/controller.php",
                            method: "POST",
                            data: "ID=<?=$ID?>",
                            success: function () {
                                window.location.href = "/dashboard/origins";
                            }
                        });
                    });
                });
            </script>
        </body>
    </html>
<?php } ?>