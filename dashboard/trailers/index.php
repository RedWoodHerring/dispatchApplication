<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/8/17
 * Time: 10:56 AM
 */

session_start();

if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $email = $_SESSION['userInfo']['email'];
    $name = $_SESSION['userInfo']['name'];
    $account = $_SESSION['userInfo']['accountType'];
    $ID = $_SESSION['userInfo']['ID'];
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/header.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/appHeader.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/block.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/footer.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/scripts.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/modal.php");
    includes($_SESSION['userInfo']);
    if($_SESSION['userInfo']['accountType'] === "driver" || $userInfo['accountType'] === "dispatch"){
        $query = "SELECT * FROM drivers WHERE userID='$ID'";
        $driverArray = mysqli_fetch_assoc(mysqli_query($db,$query));
        $driverID = $driverArray['ID'];
        $query = "SELECT loads.ID,loads.date,jobs.name FROM loads JOIN jobs ON loads.jobID=jobs.ID WHERE loads.driverID='$driverID'";
        $loadArray = mysqli_fetch_array(mysqli_query($db,$query),MYSQLI_ASSOC);
    }
?>

    <!DOCTYPE html>
    <html lang="en">
        <?php head("Admin Dashboard");?>
        <body class="dark">
            <div class="app" id="app">
                <div id="aside" class="app-aside modal nav-dropdown">
                    <?php nav($name);?>

                </div>

                <div id="content" class="app-content box-shadow-z0" role="main">

                    <?php appHeader("Trailers"); ?>

                    <?php footer();?>

                    <div ui-view class="app-body" id="view">
                        <?php modal(); ?>
                        <?php block("block1")?>
                    </div>

                </div>
            </div>

            <?php scripts(); ?>
            <script>
                $.ajax({
                    url: "/assets/php/modules/trailer/table/module.php",
                    method: "GET",
                    success: function (response) {
                        $("#block1").html(response);
                    }
                });
                $("#new").click(function(){
                    $.ajax({
                        url: "/assets/php/modules/trailer/create/module.php",
                        method: "GET",
                        success: function (response) {
                            $(".modal-header").html("Create A New Trailer");
                            $(".modal-body").html(response);
                            $('#modal').modal('show');
                        }
                    });
                });
            </script>
        </body>
    </html>
<?php } ?>