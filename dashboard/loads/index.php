<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 12/5/17
 * Time: 10:33 AM
 */


session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $email = $_SESSION['userInfo']['email'];
    $name = $_SESSION['userInfo']['name'];
    $account = $_SESSION['userInfo']['accountType'];
    $ID = $_SESSION['userInfo']['ID'];
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/header.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/appHeader.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/block.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/footer.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/scripts.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/modal.php");
    includes($_SESSION['userInfo']);
    if($_SESSION['userInfo']['accountType'] === "driver" || $userInfo['accountType'] === "dispatch"){
        $query = "SELECT * FROM drivers WHERE userID='$ID'";
        $driverArray = mysqli_fetch_assoc(mysqli_query($db,$query));
        $driverID = $driverArray['ID'];
        $query = "SELECT loads.ID,loads.date,jobs.name FROM loads JOIN jobs ON loads.jobID=jobs.ID WHERE loads.driverID='$driverID'";
        $loadArray = mysqli_fetch_array(mysqli_query($db,$query),MYSQLI_ASSOC);
    }
    ?>

    <!DOCTYPE html>
    <html lang="en">
    <?php head("Dashboard");?>
    <body class="dark">
    <div class="app" id="app">
        <div id="aside" class="app-aside modal nav-dropdown">
            <?php nav($_SESSION['userInfo']['name']); ?>
        </div>
        <?php modal(); ?>
        <div id="content" class="app-content box-shadow-z0" role="main">

            <?php appHeader("Loads"); ?>


            <?php footer(); ?>
            <div ui-view class="app-body" id="view">
                <div class="row flex-wrap">
                    <div class="col">
                        <?php block("block1");?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php scripts(); ?>
    <script>
        $(function(){
            var success = function(response){block1.html(response);};
            var block1 = $("#block1");

            <?php if($account === "driver"){ ?>
                var data = "driverID=<?=$driverID?>";
                ajax("/assets/php/modules/load/table/module.php",data,success);
            <?php } else if($account === "admin") {?>

            <?php } ?>
        });
    </script>
    </body>
    </html>
<?php } ?>