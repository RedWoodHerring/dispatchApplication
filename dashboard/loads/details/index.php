<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 1:57 PM
 */

session_start();
if (!isset($_SESSION['login'])) {
    header("location:/");
} else {
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $email = $_SESSION['userInfo']['email'];
    $name = $_SESSION['userInfo']['name'];
    $account = $_SESSION['userInfo']['accountType'];
    $ID = $_SESSION['userInfo']['ID'];
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/header.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/appHeader.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/block.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/footer.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/scripts.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/modal.php");
    includes($_SESSION['userInfo']);
    if($_SESSION['userInfo']['accountType'] === "driver" || $userInfo['accountType'] === "dispatch"){
        $query = "SELECT * FROM drivers WHERE userID='$ID'";
        $driverArray = mysqli_fetch_assoc(mysqli_query($db,$query));
        $driverID = $driverArray['ID'];
        $query = "SELECT loads.ID,loads.date,jobs.name FROM loads JOIN jobs ON loads.jobID=jobs.ID WHERE loads.driverID='$driverID'";
        $loadArray = mysqli_fetch_array(mysqli_query($db,$query),MYSQLI_ASSOC);
    }
    includes($_SESSION['userInfo']);
    $load = $_GET['ID'];
    $query = "SELECT * FROM loads WHERE ID='$load'";
    $loadArray = mysqli_fetch_assoc(mysqli_query($db,$query));?>

    <!DOCTYPE html>
    <html lang="en">
        <?php head("Dashboard"); ?>
        <body class="dark">
            <div class="app" id="app">
                <div id="aside" class="app-aside modal nav-dropdown">
                    <?php nav($_SESSION['userInfo']['name']); ?>
                </div>
                <?php modal(); ?>
                <div id="content" class="app-content box-shadow-z0" role="main">
                    <?php appHeader("Load Details"); ?>
                    <?php footer(); ?>
                    <div ui-view class="app-body" id="view">
                        <div class="row">
                            <div class="padding col">
                                <div>
                                    <?php if($loadArray['status'] === "0" && $_SESSION['userInfo']['accountType'] === "driver"){?>
                                        <button class="btn success accept">Accept Load</button>
                                        <button class="btn danger decline">Decline Load</button>
                                    <?php } else if($loadArray['status'] === "2" && $_SESSION['userInfo']['accountType'] === "driver"){ ?>
                                        <button class="btn success accept">Accept Load</button>
                                    <?php } ?>
                                    <?php if($loadArray['status'] === "1" && $_SESSION['userInfo']['accountType'] === "driver"){ ?>
                                        <button class="btn success complete">Complete Load</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <?php block("details"); ?>
                            <?php if ($_SESSION['userInfo']['accountType'] === "admin" || $_SESSION['userInfo']['accountType'] === "dispatch") { ?>
                                <div class="padding">
                                    <div class="box">
                                        <div id="commands">
                                            <button class="btn danger delete col">Delete</button>
                                            <button class="btn info carrier col">Assign To A Different Carrier</button>
                                            <?php if($loadArray['driverID'] === "0"){?>
                                                <button class="btn info col aDriver">Assign A Driver</button>
                                            <?php } else { ?>
                                                <button class="btn info col">Change Assigned Driver</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php block("commands"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php scripts(); ?>
            <script>
                    var details =  $("#details");
                    var commands = $("#commands");
                    var success = function(response){details.html(response);};
                    var data = "ID=<?=$load?>";

                    <?php if ($_SESSION['userInfo']['accountType'] === "driver") { ?>
                        ajax("/assets/php/modules/load/details/driver/module.php",data,success);

                        <?php if ($loadArray['status'] === "1") { ?>
                            success = function(response){commands.html(response);};
                            ajax("/assets/php/modules/load/commands/driver/module.php",data,success);

                        <?php } else if ($loadArray['status'] === "0"){ ?>
                            $(".accept").click(function(){
                                data = "ID=<?=$load?>&driver=<?=$array['driverID']?>";
                                success = function(response){window.location.href = "/dashboard/loads/details/?ID=<?=$load?>";};
                                ajax("/assets/php/modules/load/accept/controller.php",data,success);
                            });

                            $(".decline").click(function(){
                                data = "ID=<?=$load?>";
                                success = function(response){window.location.href = "/dashboard";};
                                ajax("/assets/php/modules/load/decline/controller.php",data,success);
                            });

                        <?php } else if ($loadArray['status'] === "2"){ ?>
                            $(".accept").click(function(){
                                $.ajax({
                                    method: "POST",
                                    data: "ID=<?=$load?>",
                                    url: "/assets/php/modules/loadAccept/controller.php",
                                    success: function(){
                                        window.location.href = "/dashboard/loads/details/?ID=<?=$load?>";
                                    }
                                });
                            });
                        <?php } ?>
                    <?php } else if ($_SESSION['userInfo']['accountType'] === "admin" || $_SESSION['userInfo']['accountType'] === "dispatch"){?>
                        $.ajax({
                            method: "POST",
                            data: "ID=<?=$load?>",
                            url: "/assets/php/modules/loadDetails/admin/module.php",
                            success: function(response){
                                $("#details").html(response);
                            }
                        });
                        $(".delete").click(function(){
                            $.ajax({
                                method: "POST",
                                data: "ID=<?=$load?>",
                                url: "/assets/php/modules/load/delete/controller.php",
                                success: function(response){
                                }
                            });
                        });
                        $(".carrier").click(function(){
                            $.ajax({
                                method: "POST",
                                data: "ID=<?=$load?>",
                                url: "/assets/php/modules/load/changeCarrier/module.php",
                                success: function(response){
                                    $(".modal-body").html(response);
                                    $(".modal-header").html("Set New Carrier");
                                    $(".next").hide();
                                    $(".cancel").hide();
                                    $("#modal").modal('show');
                                    $("#submit").hide();
                                }
                            });
                            $("#modal").on("click", ".modal-footer #submit",function(){
                                var carrier = $("input[name='carrier']").val();
                                var driver = $("input[name='driver']").val();
                                var data = "ID=<?=$load?>&carrier="+carrier+"&driver="+driver;
                                $("")
                            });
                        });
                    $(".aDriver").click(function(){
                        $.ajax({
                            method: "POST",
                            data: "load=<?=$load?>",
                            url: "/assets/php/modules/load/changeDriver/module.php",
                            success: function(response){
                                $(".modal-body").html(response);
                                $(".modal-header").html("Set New Driver");
                                $(".next").hide();
                                $(".cancel").hide();
                                $("#modal").modal('show');
                                $("#submit").show();
                            }
                        });
                        $("#modal").on("click", ".modal-footer #submit",function(){
                            var driver = $("select[name='driver']").val();
                            var data = "ID=<?=$load?>&driver="+driver;
                            var success = function(response){
                                console.log(response);
                            };
                            ajax("/assets/php/modules/load/changeDriver/controller.php", data, success)
                        });
                    });
                        $("#modal").on("click", ".modal-footer .close",function(){
                            $(".modal-body").html("");
                            $(".modal-header").html("");
                            $("#modal").modal('hide');
                        });

                    <?php } ?>
            </script>
        </body>
    </html>
<?php } ?>