<?php
    session_start();
    if(!isset($_SESSION['login'])){
        header("location:/");
    } else{
        require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
        $email = $_SESSION['userInfo']['email'];
        $name = $_SESSION['userInfo']['name'];
        $account = $_SESSION['userInfo']['accountType'];
        $ID = $_SESSION['userInfo']['ID'];
        require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/header.php");
        require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/appHeader.php");
        require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/block.php");
        require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/footer.php");
        require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/scripts.php");
        require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/modal.php");
        includes($_SESSION['userInfo']);
        if($_SESSION['userInfo']['accountType'] === "driver" || $userInfo['accountType'] === "dispatch"){
            $query = "SELECT * FROM drivers WHERE userID='$ID'";
            $driverArray = mysqli_fetch_assoc(mysqli_query($db,$query));
            $driverID = $driverArray['ID'];
            $query = "SELECT loads.ID,loads.date,jobs.name FROM loads JOIN jobs ON loads.jobID=jobs.ID WHERE loads.driverID='$driverID'";
            $loadArray = mysqli_fetch_array(mysqli_query($db,$query),MYSQLI_ASSOC);
        }
?>

    <!DOCTYPE html>
    <html lang="en">
        <?php head("Dispatch Application Dashboard");?>
        <body class="dark">
            <div class="app" id="app">
                <div id="aside" class="app-aside modal nav-dropdown">
                    <?php nav($name);?>
                </div>
                <div id="content" class="app-content box-shadow-z0" role="main">

                    <?php appHeader("Dashboard") ?>

                    <?php footer();?>
                    <?php modal();?>
                    <div ui-view class="app-body" id="view">
                        <?php if($account === "driver"){
                            echo "<div style='width:90%;max-width:200px;margin:0 auto;'>";
                            if($driverArray['status'] === "0"){?>
                                <p class="disabled" style="text-align:center;">Not Available</p>
                            <?php } else if($driverArray['status'] === "1"){?>
                                <p class="success" style="text-align:center;">Available</p>
                            <?php }echo "</div>"; ?>

                        <?php } ?>
                        <div class="padding">
                            <div id="commandButtons">
                                <?php if($account === "admin" || $account === "dispatch"){ ?>
                                    <button id="createJob" class="info btn">Create Job</button>
                                    <button id="createLoad" class="info btn">Create Load</button>
                                <?php } else if($account === "carrier"){ ?>
                                <?php } else if($account === "customer"){ ?>
                                <?php } else if($account === "driver"){ ?>
                                    <button id="changeStatus" class="info btn">Change Status</button>
                                <?php } ?>
                            </div>
                        </div>
                        <?php block("block1"); ?>
                        <div class="row">
                            <div class="col">
                                <?php block("block2");?>
                            </div>
                            <div class="col">
                                <?php block("block3");?>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <?php scripts(); ?>
            <script>
                $(function(){
                    $("#new").hide();
                    var block1 = $("#block1");
                    var block2 = $("#block2");
                    var block3 = $("#block3");
                    var success = function(){};

                    //Driver Dashboard
                    <?php if($account === "driver"){?>
                        success = function(response){block1.html(response);};
                        ajax("/assets/php/modules/load/table/module.php","driverID=<?=$driverID?>",success);
                        $("#changeStatus").click(function(){
                            var id = <?=$driverID?>;
                            success = function(response){
                                $(".modal-body").html(response).attr("id", id);
                                $(".modal-header").html("Change Status");
                                $(".cancel").hide();
                                $(".next").hide();
                                $("#modal").modal("show");
                            };
                            ajax("/assets/php/modules/driver/statusChange/module.php","",success);
                        });
                    <?php } ?>

                    //Dispatch Dashboard
                    <?php if($account === "dispatch"){?>
                        success = function(response){block1.html(response);};
                        ajax("/assets/php/modules/load/table/module.php","status=0",success);
                        success = function(response){block2.html(response);};
                        ajax("/assets/php/modules/job/table/module.php","status=0",success);
                        success = function(response){block3.html(response);};
                        ajax("/assets/php/modules/driver/table/module.php","status=0",success);
                    <?php } ?>
                    $("#createJob").click(function(){
                        success = function(response){block1.html(response);};
                        ajax("/assets/php/modules/job/create/module.php","",success);
                    });
                    $("#createLoad").click(function(){
                        success = function(response){block1.html(response);};
                        ajax("/assets/php/modules/load/create/module.php","",success);
                    });
                });
            </script>
        </body>
    </html>
<?php } ?>