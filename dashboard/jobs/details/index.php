<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 1:57 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $email = $_SESSION['userInfo']['email'];
    $name = $_SESSION['userInfo']['name'];
    $account = $_SESSION['userInfo']['accountType'];
    $ID = $_SESSION['userInfo']['ID'];
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/header.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/appHeader.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/block.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/footer.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/scripts.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/modal.php");
    includes($_SESSION['userInfo']);
    if($_SESSION['userInfo']['accountType'] === "driver" || $userInfo['accountType'] === "dispatch"){
        $query = "SELECT * FROM drivers WHERE userID='$ID'";
        $driverArray = mysqli_fetch_assoc(mysqli_query($db,$query));
        $driverID = $driverArray['ID'];
        $query = "SELECT loads.ID,loads.date,jobs.name FROM loads JOIN jobs ON loads.jobID=jobs.ID WHERE loads.driverID='$driverID'";
        $loadArray = mysqli_fetch_array(mysqli_query($db,$query),MYSQLI_ASSOC);
    }
?>

    <!DOCTYPE html>
    <html lang="en">
        <?php head("Carriers Dashboard");?>
        <body class="dark">
            <div class="app" id="app">
                <div id="aside" class="app-aside modal nav-dropdown">
                    <?php nav($name); ?>
                </div>
                <?php modal(); ?>
                <div id="content" class="app-content box-shadow-z0" role="main">

                    <?php appHeader($jobName. " Details"); ?>


                    <?php footer(); ?>
                    <div ui-view class="app-body" id="view">
                        <div style="width: 200px;margin: 0 auto;">
                        <?php if($jobStatus === "0"){?>
                            <span class="btn danger jobStatus">Uncompleted</span>
                        <?php }else{?>
                            <span class="btn success jobStatus">Completed</span>
                        <?php } ?>
                    </div>
                        <div class="row flex-wrap">
                            <div class="col">
                                <?php block("details");?>
                            </div>
                            <div class="col commands">
                                <div class="padding">
                                    <div class="box">
                                        <div class="white">
                                            <button class="btn info vLoad">View Loads</button>
                                            <?php if($jobStatus === "0"){?>
                                                <button class="btn success complete">Complete Job</button>
                                                <button class="btn info cLoad">Create Load</button>
                                            <?php }else{?>
                                                <button class="btn success revert">Revert Job</button>
                                            <?php } ?>

                                            <button class="btn danger delete">Delete Job</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row flex-wrap">
                            <div id="jobSectionsContainer" class="col"></div>
                        </div>
                        <div class="row flex-wrap">
                            <div class="col">
                                <?php block("optionalContainer");?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php scripts(); ?>
            <script>
                $(function(){
                    var data = "ID="+<?=$ID?>;
                    var details = $("#details");
                    var success = function(response){details.html(response);};
                    var newButton = $("#new");
                    var jobSections = $("#jobSectionsContainer");
                    var optionalContainer = $("#optionalContainer");

                    ajax("/assets/php/modules/job/details/module.php",data,success);

                    success = function(response){jobSections.html(response);};
                    ajax("/assets/php/modules/job/sections/module.php",data,success);

                    $(".complete").on('click', function(){
                        success = function(response){
                            window.location.href = "/dashboard/jobs/details/?ID=<?=$ID?>";
                            $("#alert").html("<p>Job Completed</p>").fadeIn().fadeOut(4000);
                        };
                        ajax("/assets/php/modules/job/details/completeController.php",data,success);
                    });

                    $(".revert").on('click', function(){
                        success = function(response){
                            window.location.href = "/dashboard/jobs/details/?ID=<?=$ID?>";
                            $("#alert").html("<p>Job Reverted</p>").fadeIn().fadeOut(4000);
                        };
                        ajax("/assets/php/modules/job/details/revertController.php",data,success);
                    });

                    $(".delete").on('click', function(){
                        success = function(response){
                            window.location.href = "/dashboard/jobs";
                            $("#alert").html("<p>Job Reverted</p>").fadeIn().fadeOut(4000);
                        };
                        ajax("/assets/php/modules/job/details/deleteController.php",data,success);
                    });

                    $(".cLoad").on('click', function(){
                        success = function(response){optionalContainer.html(response);};
                        ajax("/assets/php/modules/load/create/module.php",data,success);
                    });

                    $(".vLoad").on('click', function(){
                        success = function(response){optionalContainer.html(response);};
                        ajax("/assets/php/modules/load/table/module.php",data,success);
                    });
                });
            </script>
        </body>
    </html>
<?php } ?>