<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/9/17
 * Time: 2:31 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $email = $_SESSION['userInfo']['email'];
    $name = $_SESSION['userInfo']['name'];
    $account = $_SESSION['userInfo']['accountType'];
    $ID = $_SESSION['userInfo']['ID'];
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/header.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/appHeader.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/block.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/footer.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/scripts.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/modal.php");
    includes($_SESSION['userInfo']);
    if($_SESSION['userInfo']['accountType'] === "driver" || $userInfo['accountType'] === "dispatch"){
        $query = "SELECT * FROM drivers WHERE userID='$ID'";
        $driverArray = mysqli_fetch_assoc(mysqli_query($db,$query));
        $driverID = $driverArray['ID'];
        $query = "SELECT loads.ID,loads.date,jobs.name FROM loads JOIN jobs ON loads.jobID=jobs.ID WHERE loads.driverID='$driverID'";
        $loadArray = mysqli_fetch_array(mysqli_query($db,$query),MYSQLI_ASSOC);
    }
?>

    <!DOCTYPE html>
    <html lang="en">
        <?php head("Jobs Dashboard");?>
        <body class="dark">
            <div class="app" id="app">
                <div id="aside" class="app-aside modal nav-dropdown">
                    <?php nav($name);?>
                </div>
                <div id="content" class="app-content box-shadow-z0" role="main">

                    <?php appHeader("Jobs"); ?>

                    <?php footer();?>

                    <div ui-view class="app-body" id="view">
                        <?php modal(); ?>
                        <?php block("block1")?>
                        <?php block("block2")?>
                    </div>
                </div>
            </div>
            <?php scripts(); ?>
            <script>
                $(function(){
                    var block1 = $("#block1");
                    var success = function(response){block1.html(response);};
                    var newButton = $("#new");

                    ajax("/assets/php/modules/job/table/module.php","",success);

                    newButton.click(function(){
                        ajax("/assets/php/modules/job/create/module.php","",success);
                    });
                });
            </script>
        </body>
    </html>
<?php } ?>