<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 1:57 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $email = $_SESSION['userInfo']['email'];
    $name = $_SESSION['userInfo']['name'];
    $account = $_SESSION['userInfo']['accountType'];
    $ID = $_SESSION['userInfo']['ID'];
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/header.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/appHeader.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/block.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/footer.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/scripts.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/modal.php");
    includes($_SESSION['userInfo']);
    if($_SESSION['userInfo']['accountType'] === "driver" || $userInfo['accountType'] === "dispatch"){
        $query = "SELECT * FROM drivers WHERE userID='$ID'";
        $driverArray = mysqli_fetch_assoc(mysqli_query($db,$query));
        $driverID = $driverArray['ID'];
        $query = "SELECT loads.ID,loads.date,jobs.name FROM loads JOIN jobs ON loads.jobID=jobs.ID WHERE loads.driverID='$driverID'";
        $loadArray = mysqli_fetch_array(mysqli_query($db,$query),MYSQLI_ASSOC);
    }
    $ID = $_GET["ID"];
    $query = "SELECT * FROM drivers WHERE ID='$ID'";
    $driverInfo = mysqli_fetch_assoc(mysqli_query($db,$query));
    $driverName = $driverInfo['name'];
?>

    <!DOCTYPE html>
    <html lang="en">
        <?php head("Customers Dashboard");?>
        <body class="dark">
            <div class="app" id="app">
                <div id="aside" class="app-aside modal nav-dropdown">
                    <?php nav($name); ?>
                </div>
                <?php modal(); ?>
                <div id="content" class="app-content box-shadow-z0" role="main">

                    <?php appHeader($driverName. " Details"); ?>

                    <?php footer(); ?>

                    <div ui-view class="app-body" id="view">
                        <div class="row flex-wrap">
                            <div class="col">
                                <?php block("details");?>
                            </div>
                            <div class="col commands">
                                <div class="padding">
                                    <div class="box">
                                        <div class="white">
                                            <button class="btn danger delete">Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row flex-wrap">
                            <div class="col">
                                <?php block("optionalContainer");?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php scripts(); ?>
            <script>
                $(function(){
                    $("#new").hide();
                    var data = "ID="+<?=$ID?>;
                    var details = $("#details");
                    var success = function(response){details.html(response);};

                    ajax("/assets/php/modules/driver/details/module.php",data,success);

                    $(".pChange").on('click', function(){
                        success = function(response){
                            $(".modal-body").html(response).attr("id",<?=$ID?>);
                            $('#modal').modal('toggle');
                            $(".modal-header").html("Change Password");
                        };
                        ajax("/assets/php/modules/passwordChange/module.php","",success);
                        $(".modal-body").attr("id",<?=$ID?>);
                    });

                    $(".delete").click(function(){
                        success = function(response){details.html(response);};
                        ajax("/assets/php/modules/driver/details/module.php",data,success);
                        $.ajax({
                            url: "/assets/php/modules/driver/delete/controller.php",
                            method: "POST",
                            data: "ID=<?=$ID?>",
                            success: function(){
                                window.location.href = "/dashboard/drivers";
                            }
                        });
                    });
                });
            </script>
        </body>
    </html>
<?php } ?>