<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 1:57 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $email = $_SESSION['userInfo']['email'];
    $name = $_SESSION['userInfo']['name'];
    $account = $_SESSION['userInfo']['accountType'];
    $ID = $_SESSION['userInfo']['ID'];
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/header.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/appHeader.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/block.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/footer.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/scripts.php");
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/modal.php");
    includes($_SESSION['userInfo']);
    if($_SESSION['userInfo']['accountType'] === "driver" || $userInfo['accountType'] === "dispatch"){
        $query = "SELECT * FROM drivers WHERE userID='$ID'";
        $driverArray = mysqli_fetch_assoc(mysqli_query($db,$query));
        $driverID = $driverArray['ID'];
        $query = "SELECT loads.ID,loads.date,jobs.name FROM loads JOIN jobs ON loads.jobID=jobs.ID WHERE loads.driverID='$driverID'";
        $loadArray = mysqli_fetch_array(mysqli_query($db,$query),MYSQLI_ASSOC);
    }
    $ID = $_GET["ID"];
    $query = "SELECT * FROM customers WHERE ID='$ID'";
    $customerInfo = mysqli_fetch_assoc(mysqli_query($db,$query));
    $customerName = $customerInfo['name'];

?>

    <!DOCTYPE html>
    <html lang="en">
        <?php head("Customers Dashboard");?>
        <body class="dark">
            <div class="app" id="app">
                <div id="aside" class="app-aside modal nav-dropdown">
                    <?php nav($name); ?>
                </div>
                <?php modal(); ?>
                <div id="content" class="app-content box-shadow-z0" role="main">

                    <?php appHeader($customerName. " Details"); ?>

                    <?php footer(); ?>

                    <div ui-view class="app-body" id="view">
                        <div class="row flex-wrap">
                            <div class="col">
                                <?php block("details");?>
                            </div>
                            <div class="col commands">
                                <div class="padding">
                                    <div class="box">
                                        <div class="white">
                                            <button class="btn info cJob">Create Job</button>
                                            <button class="btn disabled">Invoices</button>
                                            <button class="btn info vRates">View Rates</button>
                                            <button class="btn info aRate">Add Rate</button>
                                            <button class="btn info pChange">Change Password</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row flex-wrap">
                            <div class="col">
                                <?php block("optionalContainer");?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php scripts(); ?>
            <script>
                $(function(){
                    var details = $("#details");
                    var data = "ID="+<?=$ID?>;
                    var optionalContainer = $("#optionalContainer");
                    var success = function(){};

                    $("#new").hide();

                    success = function(response){details.html(response);};
                    ajax("/assets/php/modules/customer/details/module.php",data,success);
                    $(".cJob").click(function(){
                        success = function(response){optionalContainer.html(response);};
                        ajax("/assets/php/modules/job/create/module.php","",success);
                    });
                    $(".pChange").on('click', function(){
                        success = function(response){
                            $(".modal-body").html(response).attr("id",<?=$ID?>);
                            $('#modal').modal('toggle');
                            $(".modal-header").html("Change Password");
                        };
                        ajax("/assets/php/modules/passwordChange/module.php","",success);
                    });
                    $(".aRate").on('click', function(){
                        success = function(response){
                            $(".modal-body").html(response);
                            $('#modal').modal('toggle');
                            $(".modal-header").html("Mileage Rate");
                        };
                        ajax("/assets/php/modules/customer/rate/create/module.php",data,success);
                    });
                    $(".vRates").on('click', function(){
                        success = function(response){optionalContainer.html(response);};
                        ajax("/assets/php/modules/customer/rate/table/module.php","",success);
                    });
                    $(".close").click(function(){
                        $(".modal-body").html("");
                        $('#modal').modal('toggle');
                        $(".modal-header").html("");
                    });
                });
            </script>
        </body>
    </html>
<?php } ?>