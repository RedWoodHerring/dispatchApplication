<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/8/17
 * Time: 10:30 AM
 */


function changelog($db,$query){
    $userQuery = "SELECT ID FROM users WHERE email='".$_SESSION['login']."'";
    $userInfo = mysqli_fetch_assoc(mysqli_query($db,$userQuery));
    $userID = $userInfo[0];
    $changeQuery = "INSERT INTO changeLog (query,userID) VALUES (\"" . $query . "\", '$userID')";
    mysqli_query($db, $changeQuery);
}

function head($string){?>
    <head>
        <meta charset="utf-8" />
        <title><?=$string?></title>
        <meta name="description" content="Admin, Dashboard, Bootstrap, Bootstrap 4, Angular, AngularJS" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- for ios 7 style, multi-resolution icon of 152x152 -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
        <link rel="apple-touch-icon" href="/assets/images/logo.png">
        <meta name="apple-mobile-web-app-title" content="Flatkit">
        <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
        <meta name="mobile-web-app-capable" content="yes">

        <!-- style -->
        <link rel="shortcut icon" sizes="196x196" href="/assets/images/logo.png">
        <link rel="stylesheet" href="/assets/animate.css/animate.min.css" type="text/css" />
        <link rel="stylesheet" href="/assets/glyphicons/glyphicons.css" type="text/css" />
        <link rel="stylesheet" href="/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
        <link rel="stylesheet" href="/assets/material-design-icons/material-design-icons.css" type="text/css" />

        <link rel="stylesheet" href="/assets/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
        <!-- build:css ../assets/styles/app.min.css -->
        <link rel="stylesheet" href="/assets/styles/app.min.css" type="text/css" />
        <!-- endbuild -->
        <link rel="stylesheet" href="/assets/styles/font.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="/assets/js/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css">
    </head>
<?php } ?>

<?php
    function includes($userInfo){
        if($userInfo['accountType'] === "admin"){
            require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/nav/admin.php");
        } else if($userInfo['accountType'] === "dispatch"){
            require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/nav/dispatcher.php");
        } else if($userInfo['accountType'] === "carrier"){
            require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/nav/carrier.php");
        } else if($userInfo['accountType'] === "customer"){
            require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/nav/customer.php");
        } else if($userInfo['accountType'] === "driver"){
            require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/elements/nav/driver.php");
        }
    }
?>
