<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/8/17
 * Time: 3:10 PM
 */
function scripts (){
?>

    <!-- jQuery -->
    <script src="/assets/js/jquery/jquery/dist/jquery.js"></script>

    <!-- Bootstrap -->
    <script src="/assets/js/jquery/tether/dist/js/tether.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>

    <!-- Data Tables -->
    <script src="/assets/js/jquery/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <!-- Custom JS -->
    <script src="/assets/js/functions.js"></script>
    <script>
        $("#alert").fadeOut();
        $("#error").fadeOut();
        $(".modal-error").fadeOut();
        $("#userManagement").on('click',function(){
            window.location.href = "/dashboard/users";
        });
        $("#customerManagement").click(function(){
            window.location.href = "/dashboard/customers";
        });
        $("#carrierManagement").click(function(){
            window.location.href = "/dashboard/carriers";
        });
        $("#carrierTools").click(function(){
            if($(this).parent().hasClass("active")){
                $(this).parent().removeClass("active");
            } else {
                $(this).parent().addClass("active");
            }
        });
        $("#adminTools").click(function(){
            if($(this).parent().hasClass("active")){
                $(this).parent().removeClass("active");
            } else {
                $(this).parent().addClass("active");
            }
        });
        $("#dashboard").click(function(){
            window.location.href = "/dashboard";
        });
        $("#rateManagement").click(function(){
            window.location.href = "/dashboard/rates";
        });
        $("#driverManagement").click(function(){
            window.location.href = "/dashboard/drivers";
        });
        $("#truckManagement").click(function(){
            window.location.href = "/dashboard/trucks";
        });
        $("#trailerManagement").click(function(){
            window.location.href = "/dashboard/trailers";
        });
        $("#productManagement").click(function(){
            window.location.href = "/dashboard/products";
        });
        $("#originManagement").click(function(){
            window.location.href = "/dashboard/origins";
        });
        $("#jobManagement").click(function(){
            window.location.href = "/dashboard/jobs";
        });
        $("#truckTypeManagement").click(function(){
            window.location.href = "/dashboard/truckType";
        });
        $("#signOut").click(function(){
            $.ajax({
                method: "GET",
                url: "/assets/php/modules/signOut/module.php",
                success: function(){
                    window.location.href = "/";
                }
            });

        });
    </script>

<?php } ?>