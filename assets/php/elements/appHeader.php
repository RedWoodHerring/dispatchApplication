<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 12:56 PM
 */

function appHeader($header){
?>

<div class="app-header dark box-shadow">
    <div class="navbar flex-row align-items-center justify-content-between">
        <!-- Open side - Naviation on mobile -->
        <a data-toggle="modal" data-target="#aside" class="hidden-lg-up mr-3">
            <i class="material-icons">&#xe5d2;</i>
        </a>

        <!-- Page title - Bind to $state's title -->
        <div class="mb-0 h5 no-wrap" id="pageTitle"><?= $header ?></div>
        <div class="mb-0 h5 no-wrap">
            <a id="new" class="nav-link" href="" data-toggle="dropdown" aria-expanded="false">
                <i class="fa fa-fw fa-plus text-muted"></i>
                <span>New</span>
            </a>
        </div>

        <!-- navbar collapse -->
        <div class="collapse navbar-collapse" id="collapse">
            <!-- link and dropdown -->
            <ul class="nav navbar-nav mr-auto">
            </ul>
        </div>
    </div>
</div>
<div id="alert" class="success"></div>
<div id="error" class="danger"></div>
<?php } ?>