<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/8/17
 * Time: 2:53 PM
 */

function nav ($name){
    if(isset($name)){?>
        <div class="left navside dark dk" data-layout="column">
            <div class="navbar no-radius">
                <!-- brand -->
                <a class="navbar-brand" id="dashboard">
                    <div></div>
                    <img class="hidden-folded inline" src="/assets/images/logo.png"/>Dispatchr
                </a>
                <!-- / brand -->
            </div>
            <div class="hide-scroll" data-flex>
                <nav class="scroll nav-light">
                    <ul class="nav" >
                        <li>
                            <a id="customerManagement">
                                <span class="nav-icon">
                                    <i class="fa  fa-group">
                                     <span></span>
                                    </i>
                                 </span>
                                <span class="nav-text">Customers</span>
                            </a>
                        </li>
                        <li>
                            <a id="carrierTools">
                                <span class="nav-caret">
                                    <i class="fa fa-caret-down"></i>
                                </span>
                                <span class="nav-icon">
                                    <i class="fa fa-road">
                                     <span></span>
                                    </i>
                                 </span>
                                <span class="nav-text">Carrier Tools</span>
                            </a>
                            <ul class="nav-sub">
                                <li>
                                    <a id="carrierManagement">
                                        <span class="nav-icon">
                                            <i class="fa fa-institution">
                                             <span></span>
                                            </i>
                                         </span>
                                        <span class="nav-text">Carriers</span>
                                    </a>
                                </li>
                                <li>
                                    <a id="driverManagement">
                                        <span class="nav-icon">
                                            <i class="fa fa-group">
                                             <span></span>
                                            </i>
                                         </span>
                                        <span class="nav-text">Drivers</span>
                                    </a>
                                </li>
                                <li>
                                    <a id="truckManagement">
                                        <span class="nav-icon">
                                            <i class="fa fa-truck">
                                             <span></span>
                                            </i>
                                         </span>
                                        <span class="nav-text">Trucks</span>
                                    </a>
                                </li>
                                <li>
                                    <a id="truckTypeManagement">
                                        <span class="nav-icon">
                                            <i class="fa fa-truck">
                                             <span></span>
                                            </i>
                                         </span>
                                        <span class="nav-text">Truck Types</span>
                                    </a>
                                </li>
                                <li>
                                    <a id="trailerManagement">
                                        <span class="nav-icon">
                                            <i class="fa fa-cube">
                                                <span></span>
                                            </i>
                                         </span>
                                        <span class="nav-text">Trailers</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a id="productManagement">
                                <span class="nav-icon">
                                    <i class="fa fa-cubes">
                                        <span></span>
                                    </i>
                                </span>
                                <span class="nav-text">Products</span>
                            </a>
                        </li>
                        <li>
                            <a id="originManagement">
                                <span class="nav-icon">
                                    <i class="fa  fa-building-o">
                                     <span></span>
                                    </i>
                                 </span>
                                <span class="nav-text">Origins</span>
                            </a>
                        </li>
                        <li>
                            <a id="jobManagement">
                                <span class="nav-icon">
                                    <i class="fa  fa-building-o">
                                     <span></span>
                                    </i>
                                 </span>
                                <span class="nav-text">Jobs</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="b-t white">
                <div class="nav-fold">
                    <a id="signOut" class="lowerNav">
                        <span class="nav-icon">
                            <i class="fa fa-sign-out">
                                <span></span>
                            </i>
                        </span>
                        <span class="nav-text">Sign Out</span>
                    </a>
                    <a data-toggle="modal" data-target="#aside" class="lowerNav hidden-lg-up mr-3">
                        <span class="nav-icon">
                            <i class="fa fa-times-circle">
                                <span></span>
                            </i>
                        </span>
                        <span class="nav-text">Close</span>
                    </a>
                </div>
            </div>
        </div>
    <?php }} ?>
