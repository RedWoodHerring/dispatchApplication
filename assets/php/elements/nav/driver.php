<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/8/17
 * Time: 2:53 PM
 */

function nav ($name){
    if(isset($name)){?>
        <div class="left navside dark dk" data-layout="column">
            <div class="navbar no-radius">
                <!-- brand -->
                <a class="navbar-brand" id="dashboard">
                    <div></div>
                    <img class="hidden-folded inline" src="/assets/images/logo.png"/>Dispatchr
                </a>
                <!-- / brand -->
            </div>
            <div class="hide-scroll" data-flex>
                <nav class="scroll nav-light">
                    <ul class="nav" >
                    </ul>
                </nav>
            </div>
            <div class="b-t white">
                <div class="nav-fold">
                    <a id="signOut" class="lowerNav">
                        <span class="nav-icon">
                            <i class="fa fa-sign-out">
                                <span></span>
                            </i>
                        </span>
                        <span class="nav-text">Sign Out</span>
                    </a>
                    <a data-toggle="modal" data-target="#aside" class="lowerNav hidden-lg-up mr-3">
                        <span class="nav-icon">
                            <i class="fa fa-times-circle">
                                <span></span>
                            </i>
                        </span>
                        <span class="nav-text">Close</span>
                    </a>
                </div>
            </div>
        </div>
    <?php }} ?>
