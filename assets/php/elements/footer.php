<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/8/17
 * Time: 3:06 PM
 */

function footer (){
?>

<div class="app-footer">
    <div class="p-2 text-xs">
        <div class="pull-right text-muted py-1">
            &copy; Copyright <strong>Results Unlimited</strong> <span class="hidden-xs-down"></span>
            <a href="http://www.results-unlimited.com"><i class="fa fa-long-arrow-up p-x-sm"></i></a>
        </div>
    </div>
</div>
<?php } ?>