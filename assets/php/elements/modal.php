<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 1:03 PM
 */

function modal(){?>
    <div id="modal" class="modal" data-backdrop="true" style="display: none;">
        <div class="row-col h-v">
            <div class="row-cell v-m">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header center-block "><h5 class="modal-title"></h5></div>
                        <div class="modal-error text-center p-lg">Test</div>
                        <div class="modal-body text-center p-lg"></div>
                        <div class="modal-footer">
                            <button class="cancel btn dark-white">Cancel</button>
                            <button class="close btn dark-white">Close</button>
                            <button class="next btn success">Next</button>
                            <button id="submit" type="button" class="btn danger p-x-md">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
