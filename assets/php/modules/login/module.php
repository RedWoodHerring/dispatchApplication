<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/8/17
 * Time: 1:33 PM
 */

?>

<div class="p-a-md box-color r box-shadow-z1 text-color m-a">
    <div class="m-b text-sm center-block">
        <h4 class="text-center">Enter Your Credentials</h4>
    </div>
    <form id="loginForm" name="form">
        <div class="md-form-group float-label">
            <input id="email" type="email" name="email" class="md-input text-center" required>
            <label for="email">Email</label>
        </div>
        <div class="md-form-group float-label">
            <input id="password" type="password" name="password" class="md-input text-center" required>
            <label for="password">Password</label>
        </div>
        <button id="login" class="btn primary btn-block p-x-md">Sign in</button>
    </form>
</div>

<div class="p-v-lg text-center">
    <div class="m-b"><a id="fPassword" class="text-primary _600">Forgot password?</a></div>
</div>

<script>
    $("#login").on('click', function(event){
        event.preventDefault();
        var data = $("#loginForm").serialize();
        $.ajax({
            url: "assets/php/modules/login/controller.php",
            method: "POST",
            data: data,
            success: function(response){
                if(response === "false"){
                    console.log(response);
                    $("#alert").html("<p>Incorrect Login Please Try Again</p>").fadeIn().fadeOut(4000);
                } else {
                    window.location.href = "/dashboard";
                }
            }
        });
    });
    $("#fPassword").on('click', function(){
        $.ajax({
            url: "assets/php/modules/forgotPassword/module.php",
            method: "GET",
            success: function(response){
                $(".modal-body").html(response);
                $(".modal-title").html("Account Recovery");
                $('#modal').modal('toggle');
            }
        });
    });
    $("#signUpButton").click(function(){
        $.ajax({
            url: "assets/php/modules/signUp/module.php",
            method: "GET",
            success: function(response){
                $("#formContainer").html(response);
            }
        });
    });
</script>
