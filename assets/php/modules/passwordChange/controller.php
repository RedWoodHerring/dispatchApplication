<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/8/17
 * Time: 1:07 PM
 */
session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {

    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/connect.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/functions/changelog.php");

    $password = mysqli_real_escape_string($db, $_POST['password']);
    $ID = mysqli_real_escape_string($db, $_POST['id']);

    if (isset($password)) {
        $passwordHash = hash('sha256', $password);
        $query = "UPDATE users SET password='$passwordHash' WHERE ID='$ID'";
        if (mysqli_query($db, $query)) {
            changelog($db, $query);
            echo "true";
        } else {
            echo mysqli_error($db);
        }
    }
}