<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/8/17
 * Time: 12:52 PM
 */
session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
?>
    <label for="newPassword">Please Enter A New Password</label>
    <span><input type="password" id="newPassword" class="md-input"/></span>
    <p>Password Requirements</p>
    <ul class="requirements">
        <li>At Least 7 Characters Long</li>
        <li>Contains At Least 1 Lowercase Letter</li>
        <li>Contains At Least 1 Uppercase Letter</li>
        <li>Contains At Least 1 Special Character (!,%,&,@,#,$,^,*,?,_,~)</li>
    </ul>

    <script>
        $("#newPassword").keyup(function(){
            var password = $("#newPassword").val();
            $(".modal-body").children('span').removeClass('check');
            $(".modal-body").children('span').addClass('error');
            if (password.length > 7){strength = "check";}else{strength = "error";}
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)){strength = "check";}else{strength = "error";}
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)){ strength = "check";}else{strength = "error";}
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)){ strength = "check";}else{strength = "error";}
            if (strength === "check"){
                $(".modal-body").children('span').addClass('check');
                $(".modal-body").children('span').removeClass('error');
            }
        });
        $("#submit").click(function(){
            var password = $("#newPassword").val();
            var id = $(".modal-body").attr("id");
            $.ajax({
                url: "/assets/php/modules/passwordChange/controller.php",
                method: "POST",
                data: "id="+id+"&password="+password,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>Password Successfully Changed</p>").fadeIn().fadeOut(4000);
                        $('#modal').modal('toggle');
                    } else{
                        console.log(response);
                    }
                }
            });
        });
    </script>
<?php }
// TODO:Create null password modal error
?>