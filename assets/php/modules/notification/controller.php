<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 12/21/17
 * Time: 2:15 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
        function notification($to,$message,$subject){
            $headers = 'From: notifications@dispactApp.com';
            mail($to, $subject, $message, $headers);
    }
}