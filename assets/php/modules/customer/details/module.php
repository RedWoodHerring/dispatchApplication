<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 2:01 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
$ID = $_POST['ID'];
$query = "SELECT * FROM customers WHERE ID='$ID'";
$array = mysqli_fetch_assoc(mysqli_query($db,$query));
$query = "SELECT * FROM users WHERE name='".$array['name']."'";
$userInfo = mysqli_fetch_assoc(mysqli_query($db,$query));
?>

<div class="row detailContainer">
    <h4>Name:  </h4>
    <p><?=$array['name']?></p>
    <i class="fa fa-pencil editName"></i>
</div>
<div class="row editItem hidden">
    <div class="input-group row">
        <input class="form-control" placeholder="Input New Name Here">
        <span class="input-group-btn">
        <button id="submitName" class="btn info" type="button">Submit</button>
      </span>
    </div>
</div>
<hr>
<div class="row detailContainer">
    <h4>Street Address:</h4>
    <p><?=$array['sAddress']?></p>
    <i class="fa fa-pencil editAddress"></i>
</div>
<div class="row editItem hidden">
    <div class="input-group row">
        <input class="form-control" placeholder="Input New Address Here">
        <span class="input-group-btn">
        <button id="submitAddress" class="btn info" type="button">Submit</button>
      </span>
    </div>
</div>
<hr>
<div class="row detailContainer">
    <h4>City:</h4>
    <p><?=$array['city']?></p>
    <i class="fa fa-pencil editCity"></i>
</div>
<div class="row editItem hidden">
    <div class="input-group row">
        <input class="form-control" placeholder="Input New City Here">
        <span class="input-group-btn">
        <button id="submitCity" class="btn info" type="button">Submit</button>
      </span>
    </div>
</div>
<hr>
<div class="row detailContainer">
    <h4>State:</h4>
    <p><?=$array['state']?></p>
    <i class="fa fa-pencil editState"></i>
</div>
<div class="row editItem hidden">
    <div class="input-group select2-bootstrap-append">
        <select id="single-append-text" class="form-control select2-allow-clear select2-hidden-accessible" tabindex="-1" aria-hidden="true">
            <option value="AK">Alaska</option>
            <option value="HI">Hawaii</option>
            <option value="CA">California</option>
            <option value="NV">Nevada</option>
            <option value="OR">Oregon</option>
            <option value="WA">Washington</option>
            <option value="AZ">Arizona</option>
            <option value="CO">Colorado</option>
            <option value="ID">Idaho</option>
            <option value="MT">Montana</option>
            <option value="NE">Nebraska</option>
            <option value="NM">New Mexico</option>
            <option value="ND">North Dakota</option>
            <option value="UT">Utah</option>
            <option value="WY">Wyoming</option>
            <option value="AL">Alabama</option>
            <option value="AR">Arkansas</option>
            <option value="IL">Illinois</option>
            <option value="IA">Iowa</option>
            <option value="KS">Kansas</option>
            <option value="KY">Kentucky</option>
            <option value="LA">Louisiana</option>
            <option value="MN">Minnesota</option>
            <option value="MS">Mississippi</option>
            <option value="MO">Missouri</option>
            <option value="OK">Oklahoma</option>
            <option value="SD">South Dakota</option>
            <option value="TX">Texas</option>
            <option value="TN">Tennessee</option>
            <option value="WI">Wisconsin</option>
            <option value="CT">Connecticut</option>
            <option value="DE">Delaware</option>
            <option value="FL">Florida</option>
            <option value="GA">Georgia</option>
            <option value="IN">Indiana</option>
            <option value="ME">Maine</option>
            <option value="MD">Maryland</option>
            <option value="MA">Massachusetts</option>
            <option value="MI">Michigan</option>
            <option value="NH">New Hampshire</option>
            <option value="NJ">New Jersey</option>
            <option value="NY">New York</option>
            <option value="NC">North Carolina</option>
            <option value="OH">Ohio</option>
            <option value="PA">Pennsylvania</option>
            <option value="RI">Rhode Island</option>
            <option value="SC">South Carolina</option>
            <option value="VT">Vermont</option>
            <option value="VA">Virginia</option>
            <option value="WV">West Virginia</option>
        </select>
        <span class="input-group-btn">
            <button id="submitState" class="btn info" type="button">Submit</button>
          </span>
    </div>
</div>
<hr>
<div class="row detailContainer">
    <h4>ZipCode:</h4>
    <p><?=$array['zip']?></p>
    <i class="fa fa-pencil editZip"></i>
</div>
<div class="row editItem">
    <div class="input-group row">
        <input type="number" maxlength="5" class="form-control" placeholder="Input New ZipCode Here">
        <span class="input-group-btn">
        <button id="submitZip" class="btn info" type="button">Submit</button>
      </span>
    </div>
</div>
<hr>
<div class="row detailContainer">
    <h4>Email:</h4>
    <p><?=$userInfo['email']?></p>
    <i class="fa fa-pencil editEmail"></i>
</div>
<div class="row editItem">
    <div class="input-group row">
        <input type="email" class="form-control" placeholder="Input New Email Here">
        <span class="input-group-btn">
            <button id="submitEmail" class="btn info" type="button">Submit</button>
        </span>
    </div>
</div>

<script>
    var change;
    var detailsRefresh = function(){
        data = "ID=+<?=$ID?>";
        $.ajax({
            url: "/assets/php/modules/customer/details/module.php",
            method: "POST",
            data: data,
            success: function (response) {
                $("#details").html(response);
                $("#alert").fadeIn().fadeOut(4000);
            }
        });
    };
    $(function(){
        $(".fa-pencil").click(function(){
            $(this).parent().next("div").each(function(){
                if($(this).hasClass("active")){}
            });
            if($(this).parent().next("div").hasClass("active")){
                $(this).parent().next("div").removeClass("active");
                $(this).parent().next("div").css({"height":"0","overflow":"hidden"});
            } else {
                $(this).parent().next("div").addClass("active");
                $(this).parent().next("div").css({"height":"80px","overflow":"show"});

            }
        });
        $("#submitName").click(function(){
            change = $(this).parent().prev("input").val();
            data = "selection=name&ID=<?=$ID?>&change="+change;
            $.ajax({
                url: "/assets/php/modules/customer/details/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>Name Successfully Changed</p>");
                        detailsRefresh();
                    } else if(response === "Already Created"){
                        $("#error").html("<p>This Name Already Exists, Please Enter Another Name</p>");
                    } else {
                        console.log(response);
                    }

                }
            });
        });
        $("#submitAddress").click(function(){
            change = $(this).parent().prev("input").val();
            data = "selection=address&ID=<?=$ID?>&change="+change;
            $.ajax({
                url: "/assets/php/modules/customer/details/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>Address Successfully Changed</p>");
                        detailsRefresh();
                    } else {
                        console.log(response);
                    }

                }
            });
        });
        $("#submitCity").click(function(){
            change = $(this).parent().prev("input").val();
            data = "selection=city&ID=<?=$ID?>&change="+change;
            $.ajax({
                url: "/assets/php/modules/customer/details/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>City Successfully Changed</p>");
                        detailsRefresh();
                    } else {
                        console.log(response);
                    }

                }
            });
        });
        $("#submitState").click(function(){
            change = $(this).parent().prev("select").val();
            data = "selection=state&ID=<?=$ID?>&change="+change;
            $.ajax({
                url: "/assets/php/modules/customer/details/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>State Successfully Changed</p>");
                        detailsRefresh();
                    } else {
                        console.log(response);
                    }

                }
            });
        });
        $("#submitZip").click(function(){
            change = $(this).parent().prev("input").val();
            data = "selection=zip&ID=<?=$ID?>&change="+change;
            $.ajax({
                url: "/assets/php/modules/customer/details/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>ZipCode Successfully Changed</p>");
                        detailsRefresh();
                    } else {
                        console.log(response);
                    }

                }
            });
        });
        $("#submitEmail").click(function(){
            change = $(this).parent().prev("input").val();
            data = "selection=email&ID=<?=$ID?>&change="+change;
            $.ajax({
                url: "/assets/php/modules/customer/details/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>Email Successfully Changed</p>");
                        detailsRefresh();
                    } else if(response === "Already Created"){
                        $("#error").html("<p>This Email Already Exists, Please Enter Another</p>").fadeIn().fadeOut(4000);
                    } else {
                        console.log(response);
                    }

                }
            });
        });
    });
</script>
<?php } ?>