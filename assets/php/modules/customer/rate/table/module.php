<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 12/12/17
 * Time: 9:15 AM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $ID = $_POST['ID'];
    $query = "SELECT mileageRates.ID,mileageRates.rangeBottom,mileageRates.rangeTop,mileageRates.rate FROM mileageRates WHERE customerID='$ID'";
    $array = mysqli_fetch_all(mysqli_query($db, $query));?>

    <div id="rateTable">
        <h2 class="tableHeader text-center">Rates</h2>
        <p class="refreshIcon"></p>
        <table class="table table-striped table-bordered dataTable">
            <thead>
            <tr>
                <th>Bottom Range</th>
                <th>Top Range</th>
                <th>Rate</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($array as $rowNum => $row){?>
                <tr id="<?=$row[0]?>">
                    <?php foreach($row as $key => $value){?>
                        <?php if($key === 0){}else if($key === 3){?>
                            <td class="<?=$key?>">$<?= $value ?></td>
                        <?php } else { ?>
                            <td class="<?=$key?>"><?= $value ?> Miles</td>
                        <?php } ?>
                    <?php } ?>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <script>
        $("table").dataTable();
        $(".box").on('click', 'table tbody tr', function(){
            id = $(this).attr("id");
            $.ajax({
                url: "/assets/php/modules/customer/rate/edit/module.php",
                method: "POST",
                data: "ID="+id,
                success: function (response) {
                    $(".modal-body").html(response);
                    $(".modal-header").html("Edit Rate");
                    $(".next").hide();
                    $(".cancel").hide();
                    $("#modal").modal("show");
                }
            });
        });
        $("#rateTable").on('click', '.refreshIcon', function(){
            $.ajax({
                url: "/assets/php/modules/customer/rate/table/module.php",
                method: "POST",
                data: "ID=<?=$ID?>",
                success: function (response) {
                    $("#rateTable").parent().html(response);
                    $(".refreshIcon").css("transform","rotateZ(360deg)");
                }
            });
        });
    </script>

<?php } ?>