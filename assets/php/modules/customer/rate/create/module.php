<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 12/12/17
 * Time: 8:23 AM
 */

session_start();

if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    if(isset($_POST['ID'])){
        $ID = $_POST['ID'];
        $query = "SELECT ID,name FROM customers WHERE ID='$ID'";
        $array = mysqli_fetch_assoc(mysqli_query($db, $query));
    } else {
        $query = "SELECT ID,name FROM customers";
        $array = mysqli_fetch_all(mysqli_query($db, $query), MYSQLI_ASSOC);
    }
    ?>
    <form id="newRate">
        <?php if(!isset($_POST['ID'])){?>
            <div class="md-form-group float-label">
                <label for="customer">Customer</label>
                <select id="customer" name="customer" class="md-select" required>
                    <option></option>
                    <?php foreach($array as $row){?>
                        <option value="<?=$row['ID']?>"><?=$row['name']?></option>
                    <?php } ?>
                </select>
            </div>
        <?php } else {?>
            <h4><?=$array['name']?></h4>
        <?php } ?>
        <div class="md-form-group float-label">
            <label for="type">Type</label>
            <select id="type" name="type" class="md-select" required>
                <option>Select One</option>
                <option value="mileage">Mileage</option>
                <option value="demurrage">Demurrage</option>
            </select>
        </div>
        <div id="mileage">
            <div class="md-form-group float-label">
                <label for="bottom">Beginning Mileage</label>
                <input id="bottom" type="number" name="bottom" class="md-select" required>
            </div>
            <div class="md-form-group float-label">
                <label for="top">Ending Mileage</label>
                <input id="top" type="number" name="top" class="md-select" required>
            </div>
            <div class="md-form-group float-label">
                <label for="rate">Rate</label>
                <input id="rate" type="number" name="rate" class="md-select" required>
            </div>
        </div>
        <div id="demurrage">
            <div class="md-form-group float-label">
                <label for="name">Name</label>
                <input id="name" name="name" class="md-select" required>
            </div>
            <div class="md-form-group float-label">
                <label for="dRate">rate</label>
                <input id="dRate" type="number" name="dRate" class="md-select" required>
            </div>
            <div class="md-form-group float-label">
                <label for="notes">Notes</label>
                <textarea id="notes" name="notes" class="md-select"></textarea>
            </div>
        </div>
    </form>
    <script>
        $(".next").hide();
        $("#submit").hide();
        $("#demurrage").hide();
        $("#mileage").hide();
        var selection = "";
        $("#type").on('change', function(){
            selection = $(this).val();
            console.log(selection);
            if(selection === "demurrage"){
                $("#demurrage").show();
                $("#mileage").hide();
                $("#submit").show();
            } else if(selection === "mileage") {
                $("#mileage").show();
                $("#demurrage").hide();
                $("#submit").show();
            }
        });
        var data;
        $("#submit").click(function(){
            if(selection === "demurrage"){
                var name = $("input[name='name']").val();
                var dRate = $("input[name='dRate']").val();
                var notes = $("textarea[name='notes']").val();
                data = "name="+name+"&rate="+dRate+"&notes="+notes;
            } else if (selection === "mileage"){
                var bottom = $("input[name='bottom']").val();
                var top = $("input[name='top']").val();
                var rate = $("input[name='rate']").val();
                data = "bottom="+bottom+"&top="+top+"&rate="+rate;
            }
            <?php if(isset($_POST['ID'])){?>
                data += "&customer=<?=$array['ID']?>";
            <?php } else {?>
                data += "&customer="+$("input[name='customer']").val();
            <?php } ?>
            console.log(data);
            $.ajax({
                method: "POST",
                data: data,
                url: "/assets/php/modules/customer/rate/create/controller.php",
                success: function(response){
                    if(response === "true") {
                        $(".modal-header").html("");
                        $(".modal-body").html("");
                        $("#modal").modal("hide");
                    }
                }
            });
        });
    </script>
<?php } ?>