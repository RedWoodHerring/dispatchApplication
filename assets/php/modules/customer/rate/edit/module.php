<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 12/12/17
 * Time: 9:27 AM
 */

require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
$ID = $_POST['ID'];
$query = "SELECT * FROM mileageRates WHERE ID='$ID'";
$array = mysqli_fetch_assoc(mysqli_query($db, $query));?>

<div id="rangeBottom" class="editGroup">
    <h5>Bottom Range</h5>
    <p><?=$array['rangeBottom']?></p>
    <span></span>
</div>
<div id="rangeTop" class="editGroup">
    <h5>Top Range</h5>
    <p><?=$array['rangeTop']?></p>
    <span></span>
</div>
<div id="rate" class="editGroup">
    <h5>Rate</h5>
    <p><?=$array['rate']?></p>
    <span></span>
</div>
<script>
    $("#submit").hide();
    $(".editGroup").not("#password").on("click",function(){
        $(".cancel").show();
        $(".close").hide();

        if($(this).hasClass("active") || $(this).hasClass("locked")){

        } else {
            $(".modal-body").addClass("editMode");
            $(".modal-footer").children("#submit").show();
            $(this).addClass("active");
            $(".editGroup").not($(this)).each(function(){
                $(this).addClass("locked");
            });
            var id = $(this).attr('id');
            var placeholder = $(this).children("p").html();
            $(this).children("p").hide();
            $(this).children("span").html("<input id='input' name='" + id + "' class='md-input' value='" + placeholder + "' required>");
        }
    });
    $(".close").click(function(){
        $('#modal').modal('hide');
        $(".modal-header").html("");
        $(".modal-error").html("");
        $(".modal-body").html("");
        $(".modal-body").attr('id', "");
        $(".modal-footer").children("#submit").show();
    });
    $(".cancel").click( function(){
        $(".editGroup").each(function () {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(this).children("button").show();
                $(this).children("p").show();
                $(this).children("span").html("");
                $(".editGroup").each(function () {
                    $(this).removeClass("locked");
                });
                $(".modal-footer").children("#submit").hide();
                $(".modal-body").removeClass("editMode");
                $(".userEditPassword").parent("div").removeClass("active");
                $(".userEditPassword").show();
                $(".userEditPassword").parent("div").children("p").show();
                $(".userEditPassword").parent("div").children("span").html("");
                $(".requirements").addClass("hide");
                $(".cancel").hide();
            }
        });
        $(".close").show();
    });
    $("#submit").click(function(){
        var value = $("#input").val();
        var id = "<?=$ID?>";
        var column = $(".active").children("span").children("#input").attr('name');
        var data = "ID="+id+"&value="+value+"&column="+column;
        $.ajax({
            url: "/assets/php/modules/customer/rate/edit/controller.php",
            method: "POST",
            data: data,
            success: function(response){
                if(response === "true"){
                    $("#alert").html("<p>Successfully Changed</p>").fadeIn().fadeOut(4000);
                    $.ajax({
                        url: "/assets/php/modules/customer/rate/edit/module.php",
                        method: "POST",
                        data: "ID="+id,
                        success: function(response){
                            $(".modal-header").html("Edit Rate");
                            $(".modal-body").html(response);
                            $('#modal').modal('show');
                            $(".close").show();
                            $(".cancel").hide();
                            $("#submit").hide();
                        }
                    });
                } else {
                    console.log(response);
                }
            }
        });
    });
</script>
