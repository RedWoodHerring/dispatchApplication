<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/9/17
 * Time: 2:54 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $query = "SELECT customers.name,users.email FROM customers JOIN users ON customers.userID=users.ID";
    $array = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);
    $json = json_encode($array);
?>

    <form class="p-a-md box-color r box-shadow-z1 text-color m-a" id="newCustomerForm">
        <div id="partOne">
            <div class="md-form-group float-label">
                <input id="name" name="name" class="md-input" required>
                <label for="name">Name</label>
            </div>
            <div class="md-form-group float-label">
                <input type="email" id="email" name="email" class="md-input" required>
                <label for="email">Email</label>
            </div>
            <div class="md-form-group float-label">
            <span>
                <input type="password" id="newPassword" class="md-input"/>
                <label for="newPassword">Password</label>
            </span>

                <p>Password Requirements</p>
                <ul class="requirements">
                    <li>At Least 7 Characters Long</li>
                    <li>Contains At Least 1 Lowercase Letter</li>
                    <li>Contains At Least 1 Uppercase Letter</li>
                    <li>Contains At Least 1 Special Character (!,%,&,@,#,$,^,*,?,_,~)</li>
                </ul>
            </div>
        </div>
        <div id="partTwo">
            <div class="md-form-group float-label">
                <input id="sAddress" name="sAddress" class="md-input" required>
                <label for="sAddress">Street Address</label>
            </div>
            <div class="md-form-group float-label">
                <input id="city" name="city" class="md-input" required>
                <label for="city">City</label>
            </div>
            <div class="md-form-group float-label">
                <label for="state"></label>
                <select id="state" name="state" class="md-input select2" required>
                    <option class="md-input">State</option>
                    <option value="AL">Alabama</option>
                    <option value="AK">Alaska</option>
                    <option value="AZ">Arizona</option>
                    <option value="AR">Arkansas</option>
                    <option value="CA">California</option>
                    <option value="CO">Colorado</option>
                    <option value="CT">Connecticut</option>
                    <option value="DE">Delaware</option>
                    <option value="DC">District Of Columbia</option>
                    <option value="FL">Florida</option>
                    <option value="GA">Georgia</option>
                    <option value="HI">Hawaii</option>
                    <option value="ID">Idaho</option>
                    <option value="IL">Illinois</option>
                    <option value="IN">Indiana</option>
                    <option value="IA">Iowa</option>
                    <option value="KS">Kansas</option>
                    <option value="KY">Kentucky</option>
                    <option value="LA">Louisiana</option>
                    <option value="ME">Maine</option>
                    <option value="MD">Maryland</option>
                    <option value="MA">Massachusetts</option>
                    <option value="MI">Michigan</option>
                    <option value="MN">Minnesota</option>
                    <option value="MS">Mississippi</option>
                    <option value="MO">Missouri</option>
                    <option value="MT">Montana</option>
                    <option value="NE">Nebraska</option>
                    <option value="NV">Nevada</option>
                    <option value="NH">New Hampshire</option>
                    <option value="NJ">New Jersey</option>
                    <option value="NM">New Mexico</option>
                    <option value="NY">New York</option>
                    <option value="NC">North Carolina</option>
                    <option value="ND">North Dakota</option>
                    <option value="OH">Ohio</option>
                    <option value="OK">Oklahoma</option>
                    <option value="OR">Oregon</option>
                    <option value="PA">Pennsylvania</option>
                    <option value="RI">Rhode Island</option>
                    <option value="SC">South Carolina</option>
                    <option value="SD">South Dakota</option>
                    <option value="TN">Tennessee</option>
                    <option value="TX">Texas</option>
                    <option value="UT">Utah</option>
                    <option value="VT">Vermont</option>
                    <option value="VA">Virginia</option>
                    <option value="WA">Washington</option>
                    <option value="WV">West Virginia</option>
                    <option value="WI">Wisconsin</option>
                    <option value="WY">Wyoming</option>
                </select>
            </div>
            <div class="md-form-group float-label">
                <input type="number" maxlength="5" id="zip" name="zip" class="md-input" required>
                <label for="zip">ZipCode</label>
            </div>
        </div>
    </form>

    <script>
        var customers = <?= $json ?>;
        var passCheck = 0;
        $("#partTwo").hide();
        $(".cancel").hide();
        $("#submit").hide();
        $("#newPassword").keyup(function(){
            var container = $(this);
            var password = $("#newPassword").val();
            container.parent('span').removeClass('check');
            container.parent('span').addClass('error');
            if (password.length > 7){strength = "check";}else{strength = "error";}
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)){strength = "check";}else{strength = "error";}
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)){ strength = "check";}else{strength = "error";}
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)){ strength = "check";}else{strength = "error";}
            if (strength === "check"){
                container.parent('span').addClass('check');
                container.parent('span').removeClass('error');
                passCheck = 0;
            } else {
                passCheck = 1;
            }
        });
        $("#account").click(function(){
            $("#newCustomerForm").children(".md-form-group").each(function () {
                if ($(this).hasClass("hide")){
                    $(this).removeClass("hide");
                    $(this).addClass("triggered");
                }
            });
        });
        $(".next").click(function(){
            var inputCheck = 0;
            $(".modal-body input").each(function(){
                if($(this).val() === "" || $(this).val().length === 0){
                    $(this).parent().children("label").css("color","red");
                    inputCheck += 1;
                } else {
                    $(this).parent().children("label").css("color","black");
                    inputCheck -= 1;
                }
            });
            if(inputCheck !== 0){
                $(".modal-error").html("Please Fill All Fields").fadeIn().fadeOut(4000);
                return;
            } else if(passCheck === 1){
                $(".modal-error").html("Please Enter A Valid Password").fadeIn().fadeOut(4000);
            }

            var nameCheck = 0;
            var emailCheck = 0;
            $.each(customers, function(topIndex,topValue){
                $.each(topValue, function(index,value){
                    if(index === "name"){
                        if($("input[name='name']").val() === value){
                            nameCheck = 1;
                        }
                    } else if(index === "email"){
                        if($("input[name='email']").val() === value){
                            emailCheck = 1;
                        }
                    }
                });
            });
            if(nameCheck === 0 && emailCheck === 0) {
                $(".next").hide();
                $("#submit").show();
                $("#partOne").hide();
                $("#partTwo").show();
            } else if(nameCheck === 1) {
                $(".modal-error").html("Name Has Already Been Taken Please Try Another").fadeIn().fadeOut(4000);
            } else if(emailCheck === 1) {
                $(".modal-error").html("Email Has Already Been Taken Please Try Another").fadeIn().fadeOut(4000);
            }
        });
        $("#submit").click(function(){
            var data = $("#newCustomerForm").serialize();
            $.ajax({
                method: "POST",
                data: data,
                url: "/assets/php/modules/customer/create/controller.php",
                success: function (response) {
                    if (response) {
                        id = response;
                        window.location.href = "/dashboard/customers/details/?ID="+id;
                    }
                }
            });
        });
        $(".close").click(function(){
            $("#modal").modal('hide');
            $(".modal-body").html("");
            $(".modal-header").html("");
        });
    </script>
<?php }

?>