<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/9/17
 * Time: 3:37 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/connect.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/functions/changelog.php");
    $email = mysqli_real_escape_string($db,$_POST['email']);
    if(isset($_POST['email'])){
        $query = "SELECT ID FROM users WHERE email='$email'";
        if(mysqli_num_rows(mysqli_query($db,$query)) === 0) {
            $name = mysqli_real_escape_string($db, $_POST['name']);
            $password = mysqli_real_escape_string($db, $_POST['password']);
            $passwordHash = hash('sha256', $password);
            $query = "INSERT INTO users (name,email,password,accountType) VALUES ('$name', '$email', '$passwordHash', 'customer')";
            mysqli_query($db, $query);
            changelog($db, $query);
        } else {
            echo "Already Created";
        }
    }
    if(isset($_POST['name'])) {
        $query = "SELECT ID FROM users WHERE email='$email'";
        $userInfo = mysqli_fetch_assoc(mysqli_query($db,$query));
        $userID = $userInfo['ID'];
        $name = mysqli_real_escape_string($db,$_POST['name']);
        $address = mysqli_real_escape_string($db,$_POST['sAddress']);
        $city = mysqli_real_escape_string($db,$_POST['city']);
        $state = mysqli_real_escape_string($db,$_POST['state']);
        $zip = mysqli_real_escape_string($db,$_POST['zip']);
        $mileage = mysqli_real_escape_string($db,$_POST['mileage']);
        $demurrage = mysqli_real_escape_string($db,$_POST['demurrage']);
        $query = "SELECT * FROM customers WHERE name='$name'";
        if (mysqli_num_rows(mysqli_query($db, $query)) === 0) {
            $query = "INSERT INTO customers (name,sAddress,city,state,zip,mileageRate,demurrageRate,userID) VALUE ('$name', '$address', '$city', '$state', '$zip', '$mileage', '$demurrage', '$userID')";
            if(mysqli_query($db,$query)){
                echo mysqli_insert_id ($db);
                changelog($db, $query);
            } else {
                echo mysqli_error($db);
            }
        }
    }

}