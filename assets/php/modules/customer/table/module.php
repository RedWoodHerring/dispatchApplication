<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/9/17
 * Time: 2:34 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $query = "SELECT * FROM customers";
    $array = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);
?>
    <div id="customerTable">
        <h2 class="tableHeader text-center">Customer Table</h2>
        <p class="refreshIcon"></p>
        <table class="table table-striped table-bordered dataTable">
            <thead>
            <tr>
                <th>Name</th>
                <th>Street Address</th>
                <th>City</th>
                <th>State</th>
                <th>ZipCode</th>
                <th>Mileage Rate</th>
                <th>Demurrage Rate</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($array as $rowNum => $row){?>
                <tr id="<?=$row['ID']?>">
                    <?php foreach($row as $key => $value){?>
                        <?php if($key === 'ID'){}else if($key === 'userID'){
                            $query = "SELECT email FROM users WHERE ID='$value'";
                            $userInfo = mysqli_fetch_assoc(mysqli_query($db,$query));
                            ?>
                            <td class="<?=$key?>"><?=$userInfo['email']?></td>
                        <?php }else{ ?>
                            <td class="<?=$key?>"><?= $value ?></td>
                        <?php } ?>
                    <?php } ?>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <script>
        $("table").dataTable();
        $(".box").on('click', 'table tbody tr', function(){
            id = $(this).attr("id");
                window.location.href = "/dashboard/customers/details/?ID="+id;
        });
        $("#customerTable").on('click', '.refreshIcon', function(){
            $.ajax({
                url: "/assets/php/modules/customer/table/module.php",
                method: "GET",
                success: function (response) {
                    $("#customerTable").parent().html(response);
                    $(".refreshIcon").css("transform","rotateZ(360deg)");
                }
            });
        });
    </script>
<?php } ?>