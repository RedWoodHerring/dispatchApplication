<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/8/17
 * Time: 3:21 PM
 */

require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");

?>
<div class="p-a-md box-color r box-shadow-z1 text-color m-a">
    <div class="m-b text-sm">
        Create Your Account
    </div>
    <form id="signUpForm" name="form">
        <div class="md-form-group float-label">
            <input id="fName" name="fName" class="md-input" required>
            <label for="fName">First Name</label>
        </div>
        <div class="md-form-group float-label">
            <input id="lName" name="lName" class="md-input" required>
            <label for="lName">Last Name</label>
        </div>
        <div class="md-form-group float-label">
            <select id="type" name="type" class="md-input" required>
                <option value=""></option>
                <option value="carrier">Carrier</option>
                <option value="carrier">Driver</option>
            </select>
            <label for="type">Account Type</label>
        </div>
        <div class="md-form-group float-label">
            <span>
                <input id="email" type="email" name="email" class="md-input" required>
                <label for="email">Email</label>
            </span>
        </div>
        <div class="md-form-group float-label">
            <span><input id="password" type="password" name="password" class="md-input" required><label for="password">Password</label></span>

            <ul class="requirements">
                <li>At Least 7 Characters Long</li>
                <li>Contains At Least 1 Lowercase Letter</li>
                <li>Contains At Least 1 Uppercase Letter</li>
                <li>Contains At Least 1 Special Character (!,%,&,@,#,$,^,*,?,_,~)</li>
            </ul>
        </div>
        <button id="signUp" class="btn primary btn-block p-x-md">Sign Up</button>
    </form>
</div>

<style>
    .md-form-group .check,.error{
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .md-form-group .check:after{
        content: "\f058";
        font-family: 'FontAwesome';
        font-size: 1.5em;
        color: green;

    }
    .md-form-group .error:after{
        content: "\f057";
        font-family: 'FontAwesome';
        font-size: 1.5em;
        color: red;

    }
    .md-form-group .requirements{
        color: red;
        font-size:.8em;
        text-align: left;
    }
    .md-form-group .modal-footer{
        display: flex;
        justify-content:center;
    }
    .md-form-group .modal-header{
        display: flex;
        justify-content:center;
    }
    .required:after{
        content: "This Item Is Required";
        color: red;
        font-size:.8em;
        text-align: left;
    }
</style>

<script>
    var check = 0;
    var emailcheck = 0;
    var data = "";
    $("#password").keyup(function(){
        var password = $("#password").val();
        var container = $(this);
        container.parent('span').removeClass('check');
        container.parent('span').addClass('error');
        if (password.length > 7){strength = "check";}else{strength = "error";check = 1;}
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)){strength = "check";}else{strength = "error";check = 1;}
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)){ strength = "check";}else{strength = "error";check = 1;}
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)){ strength = "check";}else{strength = "error";check = 1;}
        if (strength === "check"){
            container.parent('span').addClass('check');
            container.parent('span').removeClass('error');
            check = 0;
        }
    });
    $("#email").keyup(function(){
        var container = $(this);
        container.parent('span').removeClass('check');
        container.parent('span').addClass('error');
        var email = $(this).val();
        if(isValidEmailAddress(email)){
            emailcheck = 0;
            container.parent('span').addClass('check');
            container.parent('span').removeClass('error');
        } else{
            emailcheck = 1;
            container.parent('span').removeClass('check');
            container.parent('span').addClass('error');
        }
    });
    $("#signUp").click(function(){
        $(".md-form-group").children("input").each(function() {
            if ($(this).val().length < 1) {
                check = 1;
                $(this).addClass("required");
            } else if (emailcheck === 1){
                $("#alert").html("<p>Please Enter A Valid Email Address</p>").fadeIn(4000).fadeOut(4000);
            } else if(check === 1){
                $("#alert").html("<p>Please Enter A Valid Password</p>").fadeIn(4000).fadeOut(4000);
            } else if(check === 1 && emailcheck === 1){
                $("#alert").html("<p>Please Enter A Valid Password And Email</p>").fadeIn(4000).fadeOut(4000);
            } else if(check === 0 && emailcheck === 0){
                $(this).removeClass("required");
                data = $("#signUpForm").serialize();
                $.ajax({
                    url: "/assets/php/modules/signUp/controller.php",
                    method: "POST",
                    data: data,
                    success: function(response){
                        if(response === "true"){
                            $("#alert").html("<p>Please Check Your Email For Confirmation</p>").fadeIn(4000).fadeOut(4000);
                            $.ajax({
                                url: "/assets/php/modules/login/module.php",
                                method: "GET",
                                success: function(response){
                                    $("#loginContainer").html(response);
                                }
                            });
                        } else{
                            console.log(response);
                        }
                    }
                });
            }
        });
    });

</script>
