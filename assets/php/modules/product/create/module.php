<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/9/17
 * Time: 2:54 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {?>

    <form id="newProduct">
        <div class="md-form-group float-label">
            <input id="name" name="name" class="md-input" required>
            <label for="name">Product Name</label>
        </div>

    </form>

    <script>
        $(function(){
            $(".next").hide();
            $(".cancel").hide();
            $("#submit").click(function(){
                if($("input[name='name']").val() === ""){
                    $(".modal-error").html("<p>Please Enter A Valid Name</p>").fadeIn().fadeOut(4000);
                } else {
                    var data = $("#newProduct").serialize();
                    $.ajax({
                        url: "/assets/php/modules/product/create/controller.php",
                        method: "POST",
                        data: data,
                        success: function(response){
                            if(response === "true"){
                                $("#alert").html("<p>A Product Has Been Created</p>").fadeIn().fadeOut(4000);
                                $.ajax({
                                    url: "/assets/php/modules/product/table/module.php",
                                    method: "GET",
                                    success: function (response) {
                                        $("#block1").html(response);
                                        $('#modal').modal('hide');
                                        $(".modal-header").html("");
                                        $(".modal-body").html("");
                                    }
                                });
                            } else if(response === "Already Created") {
                                $(".modal-error").html("<p>This Product's Name Is Already Taken</p>").fadeIn().fadeOut(4000);
                            }

                        }
                    });
                }
            });
            $(".close").click(function(){
                $('#modal').modal('hide');
                $(".modal-header").html("");
                $(".modal-body").html("");
                $(".modal-body").attr('id', "");
                $(".modal-footer").children("#submit").show();
            });
        });
    </script>

<?php } ?>