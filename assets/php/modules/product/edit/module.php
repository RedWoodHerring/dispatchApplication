<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 12/8/17
 * Time: 2:14 PM
 */

require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
$ID = $_POST['ID'];
$query = "SELECT name FROM products  WHERE ID='$ID'";
$truckArray = mysqli_fetch_array(mysqli_query($db,$query));?>

<div id="name" class="editGroup">
    <h5>Name: </h5>
    <p><?=$truckArray[0]?></p>
    <span></span>
</div>
<button class="btn danger delete">Delete</button>

<script>
    $(".editGroup").not("#password").on("click",function(){
        $(".cancel").show();
        $(".close").hide();
        if($(this).hasClass("active") || $(this).hasClass("locked")){

        } else {
            $(".modal-body").addClass("editMode");
            $(".modal-footer").children("#submit").show();
            $(this).addClass("active");
            $(".editGroup").not($(this)).each(function(){
                $(this).addClass("locked");
            });
            var id = $(this).attr('id');
            var placeholder = $(this).children("p").html();
            $(this).children("p").hide();
            if(id === "accountType"){
                $(this).children("span").html("<select id='input' name='" + id + "' class='md-input' required><option></option><option value='customer'>Customer</option><option value='driver'>Driver</option><option value='carrier'>Carrier</option><option value='dispatch'>Dispatch</option><option value='admin'>Admin</option></select>");
            }else {
                $(this).children("span").html("<input id='input' name='" + id + "' class='md-input' value='" + placeholder + "' required>");
            }
        }
    });
    $(".close").click(function(){
        $('#modal').modal('hide');
        $(".modal-header").html("");
        $(".modal-error").html("");
        $(".modal-body").html("");
        $(".modal-body").attr('id', "");
        $(".modal-footer").children("#submit").show();
    });
    $(".cancel").click( function(){
        $(".editGroup").each(function () {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(this).children("button").show();
                $(this).children("p").show();
                $(this).children("span").html("");
                $(".editGroup").each(function () {
                    $(this).removeClass("locked");
                });
                $(".modal-footer").children("#submit").hide();
                $(".modal-body").removeClass("editMode");
                $(".userEditPassword").parent("div").removeClass("active");
                $(".userEditPassword").show();
                $(".userEditPassword").parent("div").children("p").show();
                $(".userEditPassword").parent("div").children("span").html("");
                $(".requirements").addClass("hide");
                $(".cancel").hide();

            }
        });
        $(".close").show();

    });
    $(".delete").click(function(){
        $.ajax({
            url: "/assets/php/modules/product/delete/controller.php",
            method: "POST",
            data: "ID=<?=$ID?>",
            success: function () {
                window.location.href = "/dashboard/products";
            }
        });
    });
    $("#submit").click(function(){
        var value = $("#input").val();
        var id = $(".modal-body").attr('id');
        var itemName = $(".active").children("h5").html();
        var column = $(".active").children("span").children("#input").attr('name');
        var data = "ID="+id+"&value="+value+"&column="+column;
        $.ajax({
            url: "/assets/php/modules/product/edit/controller.php",
            method: "POST",
            data: data,
            success: function(response){
                if(response === "true"){
                    $("#alert").html("<p>"+itemName+" Successfully Changed</p>").fadeIn().fadeOut(4000);
                    $.ajax({
                        url: "/assets/php/modules/product/edit/module.php",
                        method: "POST",
                        data: "ID="+id,
                        success: function (response) {
                            $(".next").hide();
                            $(".close").show();
                            $("#submit").hide();
                            $(".cancel").hide();
                            $(".modal-header").html("Edit Truck");
                            $(".modal-body").html(response);
                            $(".modal-body").attr("id",id);
                            $("#modal").modal("show");
                        }
                    });
                } else {
                    console.log(response);
                }

            }
        });
    });
</script>