<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 2:01 PM
 */
require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
$ID = $_POST['ID'];
$query = "SELECT * FROM carriers WHERE ID='$ID'";
$array = mysqli_fetch_assoc(mysqli_query($db,$query));
$query = "SELECT * FROM users WHERE name='".$array['name']."'";
$userInfo = mysqli_fetch_assoc(mysqli_query($db,$query));

?>

<div class="row detailContainer">
    <h4>Name:  </h4>
    <p><?=$array['name']?></p>
    <i class="fa fa-pencil editName"></i>
</div>
<div class="row editItem hidden">
    <div class="input-group row">
        <input class="form-control" placeholder="Input New Name Here">
        <span class="input-group-btn">
        <button id="submitName" class="btn info" type="button">Submit</button>
      </span>
        <span class="error"></span>
    </div>
</div>
<hr>
<div class="row detailContainer">
    <h4>Email:</h4>
    <p><?=$userInfo['email']?></p>
    <i class="fa fa-pencil editEmail"></i>
</div>
<div class="row editItem">
    <div class="input-group row">
        <input type="email" class="form-control" placeholder="Input New Email Here">
        <span class="input-group-btn">
            <button id="submitEmail" class="btn info" type="button">Submit</button>
        </span>
        <span class="error"></span>
    </div>
</div>

<style>
    .detailContainer{
        justify-content: space-between;
        width: 90%;
        margin: 0 auto;
        padding-top: 10px;
    }
    .detailContainer h4{
        flex-grow: 2;
        margin-bottom:0;
    }
    .detailContainer p{
        flex-grow: 1;
        text-align: right;
    }
    .detailContainer .fa{
        cursor: pointer;
        padding: 5px;
        margin-left: 15px;
    }
    .detailContainer .fa:hover{
        background-color: rgba(0,0,0,.3);
        border-radius: 2px;
    }
    .input-group{
        padding-top:10px;
        width: 80%;
        margin: 0 auto;
    }
    .input-group select{
        font-size: 24px;
    }
    .editItem{
        height: 0;
        overflow: hidden;
        transition: height .5s;
    }
    hr{
        margin:0;
    }
    .error{
    }
</style>

<script>
    var change;
    var detailsRefresh = function(){
        data = "ID=+<?=$ID?>";
        $.ajax({
            url: "/assets/php/modules/carrier/details/module.php",
            method: "POST",
            data: data,
            success: function (response) {
                $("#customerDetails").html(response);
                $("#alert").fadeIn().fadeOut(4000);
            }
        });
    };
    $(function(){
        $(".fa-pencil").click(function(){
            $(this).parent().next("div").each(function(){
                if($(this).hasClass("active")){}
            });
            if($(this).parent().next("div").hasClass("active")){
                $(this).parent().next("div").removeClass("active");
                $(this).parent().next("div").css({"height":"0","overflow":"hidden"});
            } else {
                $(this).parent().next("div").addClass("active");
                $(this).parent().next("div").css({"height":"80px","overflow":"show"});

            }
        });
    });
</script>

<?php
?>