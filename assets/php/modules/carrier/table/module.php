<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 12:11 PM
 */

require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
$query = "SELECT carriers.ID,carriers.name,users.email FROM carriers JOIN users ON carriers.name = users.name";
$array = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);

?>
<div id="carrierTable">
    <h2 class="tableHeader text-center">Carrier Table</h2>
    <p class="refreshIcon"></p>
    <table class="table table-striped table-bordered dataTable">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($array as $rowNum => $row){?>
            <tr id="<?=$row['ID']?>">
                <?php foreach($row as $key => $value){?>
                    <?php if($key === 'ID'){}else if($key === 'password'){?>
                        <td class="<?=$key?>"><button class="btn btn-sm white pChange">Change Password</button></td>
                    <?php }else{ ?>
                        <td class="<?=$key?>"><?= $value ?></td>
                    <?php } ?>
                <?php } ?>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    $("table").dataTable();
    $(".box").on('click', 'table tbody tr', function(){
        id = $(this).attr("id");
        window.location.href = "/dashboard/carriers/details/?ID="+id;
    });
    $("#carrierTable").on('click', '.refreshIcon', function(){
        $.ajax({
            url: "/assets/php/modules/carrier/table/module.php",
            method: "GET",
            success: function (response) {
                $("#carrierTable").parent().html(response);
                $(".refreshIcon").css("transform","rotateZ(360deg)");
            }
        });
    });
</script>
