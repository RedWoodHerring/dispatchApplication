<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/14/17
 * Time: 2:37 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/connect.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/functions/changelog.php");
    $check = 0;
    $created = 0;
    $name = mysqli_real_escape_string($db, $_POST['name']);
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $password = mysqli_real_escape_string($db, $_POST['password']);
    $passwordHash = hash('sha256', $password);

    $userQuery = "SELECT * from users WHERE email='$email'";
    $carrierQuery = "SELECT * from carriers WHERE name='$name'";

    if(mysqli_num_rows(mysqli_query($db,$userQuery)) > 0){
        $created = 1;
    }

    if(mysqli_num_rows(mysqli_query($db,$carrierQuery)) > 0){
        $created = 1;
    }

    if($created === 0){
        $query = "INSERT INTO carriers (name) VALUES ('$name')";
        mysqli_query($db,$query);
        changelog($db,$query);
        $query = "INSERT INTO users (name,email,password,accountType) VALUES ('$name', '$email', '$passwordHash', 'carrier')";
        mysqli_query($db,$query);
        changelog($db,$query);
        echo "true";
    } else {
        echo "Already Created";
    }
}