<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/10/17
 * Time: 1:46 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {?>

    <form id="newCarrier">
        <div class="md-form-group float-label">
            <input id="name" name="name" class="md-input" required>
            <label for="name">Name</label>
        </div>
        <div class="md-form-group float-label">
            <span>
                <input type="email" id="email" name="email" class="md-input" required>
                <label for="email">Email</label>
            </span>
        </div>
        <div class="md-form-group float-label">
            <span>
                <input name="password" type="password" id="newPassword" class="md-input" required/>
                <label for="newPassword">Password</label>
            </span>
        </div>
        <ul class="requirements light padding box-shadow-z0">
            <p>Password Requirements</p>
            <li>At Least 7 Characters Long</li>
            <li>Contains At Least 1 Lowercase Letter</li>
            <li>Contains At Least 1 Uppercase Letter</li>
            <li>Contains At Least 1 Special Character (!,%,&,@,#,$,^,*,?,_,~)</li>
        </ul>
    </form>

<script>
    $(function(){
        $(".next").hide();
        $(".cancel").hide();
        $("#email").keyup(function(){
            var container = $(this);
            container.parent('span').removeClass('check');
            container.parent('span').addClass('error');
            var email = $(this).val();
            if(isValidEmailAddress(email)){
                emailcheck = 0;
                container.parent('span').addClass('check');
                container.parent('span').removeClass('error');
            } else{
                emailcheck = 1;
                container.parent('span').removeClass('check');
                container.parent('span').addClass('error');
            }
        });
        $("#newPassword").keyup(function(){
            var container = $(this);
            var password = $("#newPassword").val();
            container.parent('span').removeClass('check');
            container.parent('span').addClass('error');
            if (password.length > 7){strength = "check";}else{strength = "error";}
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)){strength = "check";}else{strength = "error";}
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)){ strength = "check";}else{strength = "error";}
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)){ strength = "check";}else{strength = "error";}
            if (strength === "check"){
                container.parent('span').addClass('check');
                container.parent('span').removeClass('error');
            }
        });
    });
    $("#submit").click(function(){
        if($("#email").parent('span').hasClass("error") || $("#newPassword").parent('span').hasClass("error") ){
            $(".modal-error").html("<p>Please Enter a Valid Email or Password</p>").fadeIn().fadeOut(4000);
            $(".modal-error").css({"height":"50px","overflow":"show"});
        } else if($("#email").val() === "" || $("#newPassword").val() === "" ){
            $(".modal-error").html("<p>Please Enter a Valid Email or Password</p>").fadeIn().fadeOut(4000);
        }else {
            var data = $("#newCarrier").serialize();
            $.ajax({
                url: "/assets/php/modules/carrier/create/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>Carrier Account Has Been Created</p>").fadeIn().fadeOut(4000);
                        $.ajax({
                            url: "/assets/php/modules/carrier/table/module.php",
                            method: "GET",
                            success: function (response) {
                                $("#block1").html(response);
                                $('#modal').modal('hide');
                                $(".modal-header").html("");
                                $(".modal-body").html("");
                            }
                        });
                    } else if(response === "Already Created") {
                        $(".modal-error").html("<p>This Users Name or Email Is Already Taken</p>").fadeIn().fadeOut(4000);
                    }

                }
            });
        }
    });
    $(".close").click(function(){
        $('#modal').modal('hide');
        $(".modal-header").html("");
        $(".modal-body").html("");
        $(".modal-body").attr('id', "");
        $(".modal-footer").children("#submit").show();
    });
</script>

<?php } ?>