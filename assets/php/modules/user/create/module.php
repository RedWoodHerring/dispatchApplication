<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/10/17
 * Time: 10:45 AM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/connect.php");
    $query = "SELECT * FROM carriers";
    $array = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);
?>
            <form id="newUserForm">
                <div class="md-form-group float-label">
                    <select id="type" name="type" class="md-input" required>
                        <option>Account Type</option>
                        <option value="dispatch">Dispatcher</option>
                        <option value="admin">Administrator</option>
                    </select>
                </div>
                <div class="md-form-group float-label hide">
                        <input id="name" name="name" class="md-input" required>
                        <label for="name">Name</label>
                </div>
                <div class="md-form-group float-label hide">
                    <span>
                        <input type="email" id="email" name="email" class="md-input" required>
                        <label for="email">Email</label>
                    </span>
                </div>
                <div class="md-form-group float-label hide">
                    <span>
                        <input name="password" type="password" id="newPassword" class="md-input" required/>
                        <label for="newPassword">Password</label>
                    </span>
                </div>
                <ul class="requirements light padding box-shadow-z0 hide">
                    <p>Password Requirements</p>
                    <li>At Least 7 Characters Long</li>
                    <li>Contains At Least 1 Lowercase Letter</li>
                    <li>Contains At Least 1 Uppercase Letter</li>
                    <li>Contains At Least 1 Special Character (!,%,&,@,#,$,^,*,?,_,~)</li>
                </ul>
            </form>
    <script>
            $(".cancel").hide();
            $(".next").hide();
            $("#email").keyup(function(){
                var container = $(this);
                container.parent('span').removeClass('check');
                container.parent('span').addClass('error');
                var email = $(this).val();
                if(isValidEmailAddress(email)){
                    emailcheck = 0;
                    container.parent('span').addClass('check');
                    container.parent('span').removeClass('error');
                } else{
                    emailcheck = 1;
                    container.parent('span').removeClass('check');
                    container.parent('span').addClass('error');
                }
            });
            $("#newPassword").keyup(function(){
                var container = $(this);
                var password = $("#newPassword").val();
                container.parent('span').removeClass('check');
                container.parent('span').addClass('error');
                if (password.length > 7){strength = "check";}else{strength = "error";}
                if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)){strength = "check";}else{strength = "error";}
                if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)){ strength = "check";}else{strength = "error";}
                if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)){ strength = "check";}else{strength = "error";}
                if (strength === "check"){
                    container.parent('span').addClass('check');
                    container.parent('span').removeClass('error');
                }
            });
            $("#type").change(function(){
                var type = $(this).val();
                $("input[name='name']").parent().removeClass("hide");
                $("input[name='password']").parent().parent().removeClass("hide");
                $("input[name='email']").parent().parent().removeClass("hide");
                $(".requirements").removeClass("hide");
            });
            $(".close").click(function(){
                $("#modal").modal("hide");
                $(".modal-body").html("");
                $(".modal-header").html("");
            });
            $("#submit").click(function(){
                var data = $("#newUserForm").serialize();
                $.ajax({
                    method: "POST",
                    data: data,
                    url: "/assets/php/modules/user/create/controller.php",
                    success: function(response){
                        if(response === "true") {
                            $("#alert").html("<p>User Created</p>").fadeIn().fadeOut(4000);
                            $.ajax({
                                url: "/assets/php/modules/user/table/module.php",
                                method: "GET",
                                success: function (response) {
                                    $("#block1").html(response);
                                }
                            });
                        } else if(response === "Already Created"){
                            $("#alert").html("<p>This User Has Already Been Created . Please Try A Different Name Or Edit The Current User By This Name.</p>").fadeIn().fadeOut(4000);
                        } else{
                            console.log(response);
                        }
                    }
                });
            });
    </script>
<?php } ?>