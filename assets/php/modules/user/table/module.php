<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/8/17
 * Time: 10:20 AM
 */
session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $query = "SELECT ID,name,email,accountType,relatedID FROM users";
    $array = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);
?>
    <div id="userTable">
        <h2 class="text-center tableHeader">User Table</h2>
        <p class="refreshIcon"></p>
        <table class="table table-striped table-bordered dataTable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Account Type</th>
                    <th>Carrier(For Driver Accounts)</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($array as $rowNum => $row){?>
                    <tr id="<?=$row['ID']?>">
                        <?php foreach($row as $key => $value){?>
                            <?php if($key === 'ID'){}else if($key === 'password'){?>
                                <td class="<?=$key?>"><button class="btn btn-sm white pChange">Change Password</button></td>
                            <?php }else if($key === 'relatedID'){ ?>
                                <?php if($row['accountType'] === 'driver'){
                                    $query = "SELECT * FROM carriers WHERE ID='$value'";
                                    $carrier = mysqli_fetch_assoc(mysqli_query($db,$query));
                                    ?>
                                    <td class="<?=$key?>"><?=$carrier['name']?></td>
                                <?php }else{ ?>
                                    <td class="<?=$key?>">N/A</td>
                                <?php } ?>
                            <?php }else{ ?>
                                <td class="<?=$key?>"><?= $value ?></td>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <script>
        $(function(){
            $("table").dataTable();
        });
    </script>

    <script>

        $(".pChange").on('click', function(){
                var id = $(this).parent().parent("tr").attr("id");
                $(".modal-body").attr("id",id);
                $.ajax({
                    url: "/assets/php/modules/passwordChange/module.php",
                    method: "GET",
                    success: function(response){
                        $(".modal-body").html(response);
                        $('#modal').modal('toggle');
                        $(".modal-header").html("Change Password");
                    }
                });
        });
        $(".box").on('click', 'table tbody tr', function(){
            id = $(this).attr("id");
            $.ajax({
                url: "/assets/php/modules/user/edit/module.php",
                method: "POST",
                data: "ID="+id,
                success: function(response){
                    $(".modal-header").html("Select The Item To Edit");
                    $(".modal-body").html(response);
                    $(".modal-body").attr('id', id);
                    $('#modal').modal('show');
                }
            });
        });
        $("#userTable").on('click', '.refreshIcon', function(){
            $.ajax({
                url: "/assets/php/modules/user/table/module.php",
                method: "GET",
                success: function (response) {
                    $("#userTable").parent().html(response);
                    $(".refreshIcon").css("transform","rotateZ(360deg)");
                }
            });
        });
    </script>
<?php } ?>