<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/9/17
 * Time: 12:17 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/connect.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/functions/changelog.php");

    $ID = mysqli_real_escape_string($db, $_POST['ID']);
    $delete = mysqli_real_escape_string($db, $_POST['delete']);

    if (isset($_POST['delete'])) {
        $email = $_SESSION['login'];
        $userInfo = mysqli_fetch_row(mysqli_query($db, "SELECT * FROM users WHERE email='$email'"));
        if ($userInfo['0'] !== $ID) {
            $query = "DELETE FROM users WHERE ID='$ID'";
            if (mysqli_query($db, $query)) {
                echo "true";
                changelog($db,$query);
            } else {
                echo "false";
            }
        } else {
            echo "Current User";
        }
    } else if (isset($ID)) {
        $value = mysqli_real_escape_string($db, $_POST['value']);
        $column = mysqli_real_escape_string($db, $_POST['column']);
        if ($column === "password") {
            $value = hash('sha256', $value);
        }
        $query = "UPDATE users SET $column='$value' WHERE ID='$ID'";
        if (mysqli_query($db, $query)) {
            echo "true";
            changelog($db,$query);
        } else {
            echo "false";
        }
    }
}