<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/8/17
 * Time: 12:47 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $ID = $_POST['ID'];
    if(isset($_POST['ID'])) {
        $query = "SELECT email,name,accountType FROM users WHERE ID='$ID'";
        $row = mysqli_fetch_assoc(mysqli_query($db, $query));
?>
    <div id="email" class="editGroup">
        <h5>Email</h5>
        <p><?=$row['email']?></p>
        <span></span>
    </div>
    <div id="name" class="editGroup">
        <h5>Name</h5>
        <p><?=$row['name']?></p>
        <span></span>
    </div>
    <div id="accountType" class="editGroup">
        <h5>AccountType</h5>
        <p><?=$row['accountType']?></p>
        <span></span>
    </div>
    <div id="password">
        <h5>Password</h5>
        <button class="btn btn-sm dark userEditPassword">Change Password</button>
        <span></span>
        <ul class="requirements hide">
            <li>At Least 7 Characters Long</li>
            <li>Contains At Least 1 Lowercase Letter</li>
            <li>Contains At Least 1 Uppercase Letter</li>
            <li>Contains At Least 1 Special Character (!,%,&,@,#,$,^,*,?,_,~)</li>
        </ul>
    </div>
    <button class="delete btn btn-sm danger">Delete User</button>

    <script>
        $(".modal-footer").children("#submit").hide();
        $(".cancel").hide();
        $("#submit").hide();
        $(".next").hide();
        $(".editGroup").not("#password").on("click",function(){
            $(".cancel").show();
            $(".close").hide();
            if($(this).hasClass("active") || $(this).hasClass("locked")){

            } else {
                $(".modal-body").addClass("editMode");
                $(".modal-footer").children("#submit").show();
                $(this).addClass("active");
                $(".editGroup").not($(this)).each(function(){
                    $(this).addClass("locked");
                });
                var id = $(this).attr('id');
                var placeholder = $(this).children("p").html();
                $(this).children("p").hide();
                if(id === "accountType"){
                    $(this).children("span").html("<select id='input' name='" + id + "' class='md-input' required><option></option><option value='customer'>Customer</option><option value='driver'>Driver</option><option value='carrier'>Carrier</option><option value='dispatch'>Dispatch</option><option value='admin'>Admin</option></select>");
                }else {
                    $(this).children("span").html("<input id='input' name='" + id + "' class='md-input' value='" + placeholder + "' required>");
                }
            }
        });
        $(".userEditPassword").on("click", function(){
            if($(this).parent("div").hasClass("active") || $(this).parent("div").hasClass("locked")){

            } else {
                $(".cancel").show();
                $(".close").hide();
                $(".modal-body").addClass("editMode");
                $(".modal-footer").children("#submit").show();
                $(this).parent("div").addClass("active");
                $(".editGroup").not($(this)).each(function(){
                    $(this).addClass("locked");
                });
                var id = $(this).parent("div").attr('id');
                var placeholder = $(this).children("p").html();
                $(this).hide();
                $(this).parent("div").children("span").html("<input id='input' name='" + id + "' class='md-input' required>");
                $(".requirements").removeClass("hide");
            }
        });
        $(".close").click(function(){
            $('#modal').modal('hide');
            $(".modal-header").html("");
            $(".modal-error").html("");
            $(".modal-body").html("");
            $(".modal-body").attr('id', "");
            $(".modal-footer").children("#submit").show();
        });
        $(".cancel").click( function(){
            $(".editGroup").each(function () {
                if ($(this).hasClass("active")) {
                    $(this).removeClass("active");
                    $(this).children("button").show();
                    $(this).children("p").show();
                    $(this).children("span").html("");
                    $(".editGroup").each(function () {
                        $(this).removeClass("locked");
                    });
                    $(".modal-footer").children("#submit").hide();
                    $(".modal-body").removeClass("editMode");
                    $(".userEditPassword").parent("div").removeClass("active");
                    $(".userEditPassword").show();
                    $(".userEditPassword").parent("div").children("p").show();
                    $(".userEditPassword").parent("div").children("span").html("");
                    $(".requirements").addClass("hide");
                    $(".cancel").hide();

                }
            });
            $(".close").show();

        });
        $("#submit").click(function(){
            var value = $("#input").val();
            var id = $(".modal-body").attr('id');
            var itemName = $(".active").children("h5").html();
            var column = $(".active").children("span").children("#input").attr('name');
            var data = "ID="+id+"&value="+value+"&column="+column;
            $.ajax({
                url: "/assets/php/modules/user/edit/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>"+itemName+" Successfully Changed</p>").fadeIn().fadeOut(4000);
                        $.ajax({
                            url: "/assets/php/modules/user/edit/module.php",
                            method: "POST",
                            data: "ID="+id,
                            success: function(response){
                                $(".modal-header").html("Select The Item To Edit");
                                $(".modal-body").html(response);
                                $(".modal-body").attr('id', id);
                                $('#modal').modal('show');
                                $(".close").show();
                            }
                        });
                    } else {
                        console.log(response);
                    }

                }
            });
        });
        //TODO:redirect after deletion of User & display confirmation
        $(".delete").click(function(){
            var id = $(".modal-body").attr('id');
            $.ajax({
                url: "/assets/php/modules/user/edit/controller.php",
                method: "POST",
                data: "ID="+id+"&delete=YES",
                success: function(response){
                    if(response === "true") {
                        $(".modal-header").html("");
                        $(".modal-body").html("");
                        $(".modal-body").attr('id', "");
                        $('#modal').modal('hide');
                    } else if(response === "Current User"){
                        $(".modal-error").html("<p>This Is Your Account. Please Select Another Account Or Login With Another Account To Delete.").fadeIn().fadeOut(4000);
                    } else {
                        console.log(response);
                    }
                }
            });
        });
    </script>
<?php }} ?>