<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/8/17
 * Time: 1:51 PM
 */

?>

<label for="forgotPassword">Please Enter Your Email Address</label>
<span><input type="email" id="forgotPassword" class="md-input"/></span>

<script>
    $(".cancel").hide();
    $(".close").click(function(){
        $('#modal').modal('toggle');
        $(".modal-body").html("");
        $(".modal-header").html("");
    });
    $("#submit").on('click', function(){
        var email = $("#forgotPassword").val();
        $.ajax({
            url: "/assets/php/modules/forgotPassword/controller.php",
            method: "POST",
            data: "email=" + email,
            success: function(response){
                if(response){
                    $("#alert").html("<p>An Email Has Been Sent To Your Email. Please Click The Link To Reset Your Password</p>").fadeIn().fadeOut(4000);
                    $('#modal').modal('toggle');
                    console.log(response);
                } else{
                    $("#alert").html("<p>Your Email Was Not Found In The System. Please Try Another Or Signup</p>").fadeIn().fadeOut(4000);
                }
            }
        });
    });
</script>
