<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/8/17
 * Time: 2:02 PM
 */

require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
session_start();

$email = mysqli_real_escape_string($db,$_POST['email']);

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

if(isset($email)) {

    $query = "SELECT * FROM users WHERE email='$email'";

    if (mysqli_num_rows(mysqli_query($db, $query)) === 1) {
        $row = mysqli_fetch_assoc(mysqli_query($db,$query));
        $key = generateRandomString(50);
        $hashKey = hash('sha256',$key);
        $query = "INSERT INTO passwordReset (userID,userKey) VALUES ('".$row['ID']."', '$hashKey')";
        mysqli_query($db,$query);
        echo "http://localhost:8888/?key=".$key;
        echo "true";
    } else {
        echo "false";
    }
}