<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 12/11/17
 * Time: 10:51 AM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $ID = $_POST['ID'];
    $query = "SELECT ID,name FROM carriers";
    $array = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);
?>
    <div class="inputGroup">
        <select name="carrier">
            <option>Select A New Carrier</option>
            <?php foreach($array as $row){?>
                <option value="<?=$row['ID']?>"><?=$row['name']?></option>
            <?php } ?>
        </select>
    </div>
    <div class="inputGroup drivers"></div>

    <script>
        $("select[name='carrier'").change(function(){
            var carrier = $(this).val();
            $.ajax({
                url: "/assets/php/modules/load/changeDriver/module.php",
                data: "ID="+carrier,
                method: "POST",
                success: function(response){
                    $(".drivers").html(response);
                }
            });
        });
    </script>
<?php } ?>