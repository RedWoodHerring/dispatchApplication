<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 12/14/17
 * Time: 8:55 AM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $query = "SELECT * FROM truckTypes";
    $typeArray = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);

    if(isset($_POST['type'])){
        $type = $_POST['type'];
        $status = $_POST['status'];
        $query = "SELECT loads.ID,carriers.name,jobs.name,customers.name,products.name,loads.status,trucks.type FROM loads JOIN carriers ON loads.carrierID=carriers.ID JOIN jobs ON loads.jobID=jobs.ID JOIN customers ON jobs.customerID=customers.ID JOIN products ON loads.productID=products.ID JOIN trucks ON loads.truckID=trucks.ID WHERE loads.status='$status' AND trucks.type='$type'";
    } else if(isset($_POST['status'])) {
        $status = $_POST['status'];
        $query = "SELECT loads.ID,carriers.name,jobs.name,customers.name,products.name,loads.status FROM loads JOIN carriers ON loads.carrierID=carriers.ID JOIN jobs ON loads.jobID=jobs.ID JOIN customers ON jobs.customerID=customers.ID JOIN products ON loads.productID=products.ID WHERE loads.status='$status'";
    } else {
        $query = "SELECT loads.ID,customers.name,jobs.name,carriers.name,products.name,loads.status FROM loads JOIN carriers ON loads.carrierID=carriers.ID JOIN jobs ON loads.jobID=jobs.ID JOIN customers ON jobs.customerID=customers.ID JOIN products ON loads.productID=products.ID";
    }
    $array = mysqli_fetch_all(mysqli_query($db,$query));?>
    <div id="loadTable">
        <h2 class="tableHeader text-center">Loads Table</h2>
        <p class="refreshIcon"></p>
        <button id="unassignedLoads" class="btn info">Unassigned Loads</button>
        <button id="waitingLoads" class="btn info">Waiting On Approval</button>
        <button id="activeLoads" class="btn info">Assigned Active Loads</button>
        <button id="completedLoads" class="btn info">Completed</button>
        <button id="declinedLoads" class="btn info">Declined Loads</button>
        <div id="typeButtons">
            <p>Truck Types</p>
            <button class="btn info type" id="allTypes">All</button>
            <?php foreach ($typeArray as $row){?>
                <button class="btn info type" id="<?=$row['ID']?>"><?=$row['name']?></button>
            <?php } ?>
        </div>
        <table class="table table-striped table-bordered dataTable">
            <thead>
                <tr>
                    <th>Customer</th>
                    <th>Job</th>
                    <th>Product</th>
                    <th>Carrier</th>
                    <th>Status</th>
                    <?php if(isset($_POST['type'])){?>
                        <th>Truck Type</th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php foreach($array as $rowNum => $row){?>
                <tr id="<?=$row[0]?>">
                    <?php foreach($row as $key => $value){?>
                        <?php if($key === 0){}else if($key === 5){
                            if($value === "0"){?>
                                <td class="info <?=$key?>">Needs Assignment</td>
                            <?php } else if($value === "1"){?>
                                <td class="info <?=$key?>">Waiting On Approval</td>
                            <?php } else if($value === "2"){?>
                                <td class="info <?=$key?>">Assigned Active</td>
                            <?php } else if($value === "3"){?>
                                <td class="info <?=$key?>">Completed</td>
                            <?php } else { ?>
                                <td class="info <?=$key?>">Declined</td>
                            <?php } ?>
                        <?php } else if($key === 6){
                            foreach($typeArray as $truckType){
                                if($truckType['ID'] === $value){?>
                                    <td class="<?=$key?>"><?=$truckType['name']?></td>
                                <?php } ?>
                            <?php } ?>
                        <?php } else{ ?>
                            <td class="<?=$key?>"><?= $value ?></td>
                        <?php } ?>
                    <?php } ?>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <script>
        var loadTable = $("#loadTable");
        var loadUnassigned = $("#unassignedLoads");
        var loadWaiting = $("#waitingLoads");
        var loadActive = $("#activeLoads");
        var loadCompleted = $("#completedLoads");
        var loadDeclined = $("#declinedLoads");
        var buttonContainer = $("#typeButtons");
        var loadTableRefresh = function(status){
            var success = function(response){loadTable.parent().html(response);};
            ajax("/assets/php/modules/load/table/module.php","status="+status,success);
        };
        var loadTableRefreshWithType = function(status,type){
            var success = function(response){loadTable.parent().html(response);};
            ajax("/assets/php/modules/load/table/module.php","status="+status+"&type="+type,success);
        };


        loadTable.children("table").dataTable();

        <?php if($status === "0"){?>
            loadUnassigned.addClass("disabled");
            loadTable.on('click', '.refreshIcon', function(){
                loadTableRefresh("0");
            });
            buttonContainer.hide();
        <?php } else if($status === "1"){ ?>
            loadWaiting.addClass("disabled");
            loadTable.on('click', '.refreshIcon', function(){
                loadTableRefresh("1");
            });
            buttonContainer.hide();
        <?php } else if($status === "2"){ ?>
            loadActive.addClass("disabled");
            loadTable.on('click', '.refreshIcon', function () {
                loadTableRefresh("2");
            });
            $(".type").click(function(){
                type = $(this).attr("id");
                if(type === "allTypes"){
                    loadTableRefresh("2");
                } else {
                    loadTableRefreshWithType("2", type);
                }
            });
        <?php } else if($status === "3"){ ?>
            loadCompleted.addClass("disabled");
            loadTable.on('click', '.refreshIcon', function () {
                loadTableRefresh("3");
            });
            $(".type").click(function(){
                type = $(this).attr("id");
                loadTableRefreshWithType("3",type);
            });
        <?php } else {?>
            loadDeclined.addClass("disabled");
            loadTable.on('click', '.refreshIcon', function(){
                loadTableRefresh("4");
            });
            buttonContainer.hide();
        <?php } ?>

        loadTable.on('click', '#unassignedLoads', function(){
            loadUnassigned.addClass("disabled");
            loadCompleted.removeClass("disabled");
            loadWaiting.removeClass("disabled");
            loadActive.removeClass("disabled");
            loadDeclined.removeClass("disabled");
            loadTableRefresh("0");
        });

        loadTable.on('click', '#waitingLoads', function(){
            loadWaiting.addClass("disabled");
            loadCompleted.removeClass("disabled");
            loadUnassigned.removeClass("disabled");
            loadActive.removeClass("disabled");
            loadDeclined.removeClass("disabled");
            loadTableRefresh("1");
        });

        loadTable.on('click', '#activeLoads', function(){
            loadActive.addClass("disabled");
            loadCompleted.removeClass("disabled");
            loadUnassigned.removeClass("disabled");
            loadWaiting.removeClass("disabled");
            loadDeclined.removeClass("disabled");
            loadTableRefresh("2");
        });

        loadTable.on('click', '#declinedLoads', function(){
            loadDeclined.addClass("disabled");
            loadCompleted.removeClass("disabled");
            loadUnassigned.removeClass("disabled");
            loadWaiting.removeClass("disabled");
            loadActive.removeClass("disabled");
            loadTableRefresh("4");
        });

        loadTable.on('click', '#completedLoads', function(){
            loadActive.removeClass("disabled");
            loadCompleted.addClass("disabled");
            loadUnassigned.removeClass("disabled");
            loadWaiting.removeClass("disabled");
            loadActive.removeClass("disabled");
            loadTableRefresh("3");
        });

        $(".box").on('click', 'table tbody tr', function(){
            id = $(this).attr("id");
            window.location.href = "/dashboard/loads/details/?ID="+id;
        });

    </script>
<?php } ?>
