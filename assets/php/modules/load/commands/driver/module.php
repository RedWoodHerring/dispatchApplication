<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 12/6/17
 * Time: 10:14 AM
 */

require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
$load = $_POST['ID'];
$query = "SELECT loadTimeStart,loadTimeStop,unloadTimeStart,unloadTimeStop,demurrageTimeStart,demurrageTimeStop FROM loads WHERE ID='$load'";
$timeArray = mysqli_fetch_array(mysqli_query($db,$query));
?>
<div class="col">
    <?php if(isset($timeArray[0]) && isset($timeArray[1])){?>
        <h3>Load Times</h3>
        <div class="row">
            <h4>Start:</h4>
            <p><?=$timeArray[0]?></p>
            <div class="editContainer">
                <input name="date" type="date" value="<?=date("Y-m-d", strtotime($timeArray[0]))?>" class="form-control datepicker">
                <input name="time" type="time" value="<?=date("H:m", strtotime($timeArray[0]))?>" class="form-control datepicker">
                <button class="change">Submit</button>
            </div>
            <button id="startLoadChange" class="btn info edit">Edit</button>
        </div>
        <hr>
        <div class="row">
            <h4>Stop:</h4>
            <p><?=$timeArray[1]?></p>
            <div class="editContainer">
                <input name="date" type="date" value="<?=date("Y-m-d", strtotime($timeArray[1]))?>" class="form-control datepicker">
                <input name="time" type="time" value="<?=date("H:m", strtotime($timeArray[1]))?>" class="form-control datepicker">
                <button class="change">Submit</button>
            </div>
            <button id="stopLoadChange" class="btn info edit">Edit</button>
        </div>
    <?php } else if(isset($timeArray[0])){?>
        <button id="stopLoad" class="btn success">Stop Load</button>
        <div class="row">
            <h4>Start:</h4>
            <p><?=$timeArray[0]?></p>
            <div class="editContainer">
                <input name="date" type="date" value="<?=date("Y-m-d", strtotime($timeArray[0]))?>" class="form-control datepicker">
                <input name="time" type="time" value="<?=date("H:m", strtotime($timeArray[0]))?>" class="form-control datepicker">
                <button class="change">Submit</button>
            </div>
            <button id="stopLoadChange" class="btn info edit">Edit</button>
        </div>
    <?php } else { ?>
        <button id="startLoad" class="btn success">Start Load</button>
    <?php } ?>
</div>
<div class="col">
    <?php if(isset($timeArray[2]) && isset($timeArray[3])){?>
        <h3>Unload Times</h3>
        <div class="row">
            <h4>Start:</h4>
            <p><?=$timeArray[2]?></p>
            <div class="editContainer">
                <input name="date" type="date" value="<?=date("Y-m-d", strtotime($timeArray[2]))?>" class="form-control datepicker">
                <input name="time" type="time" value="<?=date("H:m", strtotime($timeArray[2]))?>" class="form-control datepicker">
                <button class="change">Submit</button>
            </div>
            <button id="stopLoadChange" class="btn info edit">Edit</button>
        </div>
        <hr>
        <div class="row">
            <h4>Stop:</h4>
            <p><?=$timeArray[3]?></p>
            <div class="editContainer">
                <input name="date" type="date" value="<?=date("Y-m-d", strtotime($timeArray[3]))?>" class="form-control datepicker">
                <input name="time" type="time" value="<?=date("H:m", strtotime($timeArray[3]))?>" class="form-control datepicker">
                <button class="change">Submit</button>
            </div>
            <button id="stopLoadChange" class="btn info edit">Edit</button>
        </div>
    <?php } else if(isset($timeArray[2])){?>
        <button id="stopUnload" class="btn success">Stop Unload</button>
        <div class="row">
            <h4>Start:</h4>
            <p><?=$timeArray[2]?></p>
            <div class="editContainer">
                <input name="date" type="date" value="<?=date("Y-m-d", strtotime($timeArray[2]))?>" class="form-control datepicker">
                <input name="time" type="time" value="<?=date("H:m", strtotime($timeArray[2]))?>" class="form-control datepicker">
                <button class="change">Submit</button>
            </div>
            <button id="stopLoadChange" class="btn info edit">Edit</button>
        </div>
    <?php } else { ?>
        <button id="startUnload" class="btn success">Start Unload</button>
    <?php } ?>
</div>
<div class="col">
    <?php if(isset($timeArray[4]) && isset($timeArray[5])){?>
        <h3>Demmurage Times</h3>
        <div class="row">
            <h4>Start:</h4>
            <p><?=$timeArray[4]?></p>
            <div class="editContainer">
                <input name="date" type="date" value="<?=date("Y-m-d", strtotime($timeArray[4]))?>" class="form-control datepicker">
                <input name="time" type="time" value="<?=date("H:m", strtotime($timeArray[4]))?>" class="form-control datepicker">
                <button class="change">Submit</button>
            </div>
            <button id="stopLoadChange" class="btn info edit">Edit</button>
        </div>
        <hr>
        <div class="row">
            <h4>Stop:</h4>
            <p><?=$timeArray[5]?></p>
            <div class="editContainer">
                <input name="date" type="date" value="<?=date("Y-m-d", strtotime($timeArray[5]))?>" class="form-control datepicker">
                <input name="time" type="time" value="<?=date("H:m", strtotime($timeArray[5]))?>" class="form-control datepicker">
                <button class="change">Submit</button>
            </div>
            <button id="stopLoadChange" class="btn info edit">Edit</button>
        </div>
    <?php } else if(isset($timeArray[4])){?>
        <button id="stopDemurrage" class="btn success">Stop Demurrage</button>
        <div class="row">
            <h4>Start:</h4>
            <p><?=$timeArray[4]?></p>
            <div class="editContainer">
                <input name="date" type="date" value="<?=date("Y-m-d", strtotime($timeArray[4]))?>" class="form-control datepicker">
                <input name="time" type="time" value="<?=date("H:m", strtotime($timeArray[4]))?>" class="form-control datepicker">
                <button class="change">Submit</button>
            </div>
            <button id="stopLoadChange" class="btn info edit">Edit</button>
        </div>
    <?php } else { ?>
        <button id="startDemurrage" class="btn success">Start Demurrage</button>
    <?php } ?>
</div>
<div>
    <button id="aNote" class="btn disabled">Add Notes</button>
</div>
<div>
    <h3>Upload Scale Ticket</h3>
    <button id="upload" class="btn disabled">Upload</button>
</div>

<script>
    $("#commands input").hide();
    var refresh = function(){
        window.location.href = "/dashboard/loads/details/?ID=<?=$load?>";
    };
    $(".change").hide();
    $("#startLoad").on('click', function(){
        $.ajax({
            method: "POST",
            data: "job=<?=$load?>",
            url: "/assets/php/modules/load/time/load/start/controller.php",
            success: function(response){
                refresh();
            }
        });
    });

    $("#stopLoad").on('click', function(){
        $.ajax({
            method: "POST",
            data: "job=<?=$load?>",
            url: "/assets/php/modules/load/time/load/stop/controller.php",
            success: function(response){
                refresh();
            }
        });
    });
    $("#startUnload").on('click', function(){
        $.ajax({
            method: "POST",
            data: "job=<?=$load?>",
            url: "/assets/php/modules/load/time/unload/start/controller.php",
            success: function(response){
                refresh();
            }
        });
    });
    $("#stopUnload").on('click', function(){
        $.ajax({
            method: "POST",
            data: "job=<?=$load?>",
            url: "/assets/php/modules/load/time/unload/stop/controller.php",
            success: function(response){
                refresh();
            }
        });
    });
    $("#startDemurrage").on('click', function(){
        $.ajax({
            method: "POST",
            data: "job=<?=$load?>",
            url: "/assets/php/modules/load/time/demmurage/start/controller.php",
            success: function(response){
                refresh();
            }
        });
    });
    $("#stopDemurrage").on('click', function(){
        $.ajax({
            method: "POST",
            data: "job=<?=$load?>",
            url: "/assets/php/modules/load/time/demmurage/stop/controller.php",
            success: function(response){
                refresh();

            }
        });
    });
</script>