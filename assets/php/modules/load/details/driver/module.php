<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 2:01 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $ID = $_POST['ID'];
    $query = "SELECT loads.date,loads.quantity,loads.directions,products.name,origins.name,jobs.name FROM loads JOIN products ON loads.productID=products.ID JOIN origins ON loads.originID=origins.ID JOIN jobs on loads.jobID=jobs.ID WHERE loads.ID='$ID'";
    $array = mysqli_fetch_array(mysqli_query($db,$query));
?>
    <div class="row detailContainer">
        <h4>Date Created: </h4>
        <p><?= $array[0] ?></p>
    </div>
    <hr>
    <div class="row detailContainer">
        <h4>Job Name: </h4>
        <p><?= $array[5] ?></p>
    </div>
    <hr>
    <div class="row detailContainer">
        <h4>Product: </h4>
        <p><?= $array[3] ?></p>
    </div>
    <hr>
    <div class="row detailContainer">
        <h4>Quantity: </h4>
        <p><?= $array[1] ?></p>
    </div>
    <hr>
    <div class="row detailContainer">
        <h4>Origin: </h4>
        <p><?= $array[4] ?></p>
    </div>
    <hr>
    <div class="row detailContainer">
        <h4>Directions: </h4>
        <p><?= $array[2] ?></p>
    </div>
<?php } ?>
