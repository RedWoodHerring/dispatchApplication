<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 2:01 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $ID = $_POST['ID'];
    $query = "SELECT loads.date,loads.quantity,loads.directions,products.name,origins.name,jobs.name,jobs.ID,loads.status FROM loads JOIN products ON loads.productID=products.ID JOIN origins ON loads.originID=origins.ID JOIN jobs on loads.jobID=jobs.ID WHERE loads.ID='$ID'";
    $array = mysqli_fetch_array(mysqli_query($db,$query));
    $query = "SELECT jobProduct.ID,jobProduct.totalRequired,jobProduct.currentQuantity,products.name FROM jobProduct JOIN products ON jobProduct.productID=products.ID  WHERE jobProduct.jobID='".$array[6]."'";
    $productArray = mysqli_fetch_all(mysqli_query($db,$query));
    $query = "SELECT ID,name FROM origins";
    $originArray = mysqli_fetch_all(mysqli_query($db,$query));
    $query = "SELECT loads.status,drivers.name FROM loads JOIN drivers ON loads.driverID=drivers.ID WHERE loads.ID='$ID'";
    $driverInfo = mysqli_fetch_array(mysqli_query($db,$query));
    $query = "SELECT loads.carrierID,carriers.name FROM loads JOIN carriers ON loads.carrierID=carriers.ID";
    $carrierInfo = mysqli_fetch_array(mysqli_query($db,$query));
    $query = "SELECT ID,name FROM drivers WHERE carrierID='".$carrierInfo[0]."' AND status='1' AND jobStatus='0'";
    $driverArray = mysqli_fetch_all(mysqli_query($db,$query));
?>
    <div class="row detailContainer">
        <h4>Date Created: </h4>
        <p><?= date("M j,Y", strtotime($array[0])); ?></p>
    </div>
    <hr>
    <div class="row detailContainer">
        <h4>Job Name: </h4>
        <p><?= $array[5] ?></p>
    </div>
    <hr>
    <div class="row detailContainer">
        <h4>Carrier: </h4>
        <p><?= $carrierInfo[1] ?></p>
    </div>
    <hr>
    <div class="row detailContainer">
        <h4>Product: </h4>
        <p><?= $array[3] ?></p>
    </div>
    <div class="row editItem hidden">
        <div class="input-group row">
            <select>
                <option value="0">Please Select A New Product Relation</option>
                <?php foreach($productArray as $row){?>
                    <option value="<?=$row[0]?>"><?=$row[3]." Total ".$row[1]."-lbs. (Current ".$row[2]."-lbs.)"?></option>
                <?php } ?>
            </select>
            <span class="input-group-btn">
            <button id="submitproduct" class="btn info" type="button">Submit</button>
        </span>
        </div>
    </div>
    <hr>
    <div class="row detailContainer">
        <h4>Load Quantity: </h4>
        <p><?= $array[1] ?></p>
    </div>
    <div class="row editItem hidden">
        <div class="input-group row">
            <input class="form-control" placeholder="Input New Quantity Here">
            <span class="input-group-btn">
            <button id="submitQuantity" class="btn info" type="button">Submit</button>
        </span>
        </div>
    </div>
    <hr>
    <div class="row detailContainer">
        <h4>Origin: </h4>
        <p><?= $array[4] ?></p>
    </div>
    <div class="row editItem hidden">
        <div class="input-group row">
            <select>
                <option value="0">Please Select A New Origin</option>
                <?php foreach($originArray as $row){?>
                    <option value="<?=$row[0]?>"><?=$row[1]?></option>
                <?php } ?>
            </select>
            <span class="input-group-btn">
            <button id="submitOrigin" class="btn info" type="button">Submit</button>
        </span>
        </div>
    </div>
    <hr>
    <div class="row detailContainer">
        <h4>Directions: </h4>
        <p><?= $array[2] ?></p>
    </div>
    <div class="row editItem hidden">
        <div class="input-group row">
            <textarea class="form-control" placeholder="Input New Directions Here"></textarea>
            <span class="input-group-btn">
            <button id="submitDirections" class="btn info" type="button">Submit</button>
        </span>
        </div>
    </div>
    <hr>
    <div class="row detailContainer">
        <h4>Driver: </h4>
        <p><?php if($driverInfo[0] === "1"){?>
                <span class="info">Accepted</span>
            <?php } else if($driverInfo[0] === "0"){ ?>
                <span class="disabled">Waiting on Approval</span>
            <?php } else if($driverInfo[0] === "2"){ ?>
                <span class="black">Declined</span>
            <?php } else if($array[7] === "0"){?>
                <span class="disabled">Waiting on Assignment</span>
            <?php } else if($array[7] === "1"){ ?>
                <span class="success">Job Completed</span>
            <?php } ?>
            <?= $driverInfo[1] ?></p>
    </div>
    <script>
        $(".fa-pencil").click(function () {
            if ($(this).parent().next("div").hasClass("active")) {
                $(this).parent().next("div").removeClass("active");
                $(this).parent().next("div").css({"height": "0", "overflow": "hidden"});
            } else {
                $(this).parent().next("div").addClass("active");
                $(this).parent().next("div").css({"height": "80px", "overflow": "show"});

            }
        });
        $("#submitDriver").click(function(){
            var change = $(this).parent().parent().children("select").val();
            var data = "change="+change+"&load=<?=$ID?>";
            $.ajax({
                method: "POST",
                url: "/assets/php/modules/load/details/controllerDriver.php",
                data: data,
                success: function(){
                    <?php if ($_SESSION['userInfo']['accountType'] === "dispatch"){?>
                        window.location.href = "/dashboard";
                    <?php } else { ?>
                        window.location.href = "/dashboard/loads/details/?ID=<?=$ID?>";
                    <?php } ?>
                }
            })
        });
    </script>
<?php } ?>
