<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 2:01 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $ID = $_POST['ID'];
    $query = "SELECT loads.ID,carriers.name,customers.name,products.name FROM loads JOIN carriers ON loads.carrierID=carriers.ID JOIN jobs ON loads.jobID=jobs.ID JOIN customers ON jobs.customerID=customers.ID JOIN products ON loads.productID=products.ID WHERE jobID='$ID'";
    $array = mysqli_fetch_all(mysqli_query($db,$query));
    $query = "SELECT ID,name FROM carriers";
    $carrierArray = mysqli_fetch_all(mysqli_query($db,$query));
?>

<div class="row detailContainer">
    <h4>Load ID:</h4>
    <p><?=$array[0][0]?></p>
</div>
<hr>
<div class="row detailContainer">
    <h4>GPS Coordinates:</h4>
    <p><?=$array[0][1]?></p>
    <i class="fa fa-pencil editGPS"></i>
</div>
<div class="row editItem hidden">
    <div class="input-group row">
        <input class="form-control" placeholder="Input New GPS Coordinates Here">
        <span class="input-group-btn">
        <button id="submitGPS" class="btn info" type="button">Submit</button>
      </span>
    </div>
</div>
<hr>
<div class="row detailContainer">
    <h4>Carrier:</h4>
    <p><?=$array[0][2]?></p>
    <i class="fa fa-pencil editCustomer"></i>
</div>
<div class="row editItem hidden">
    <div class="input-group row">
        <div class="input-group select2-bootstrap-append">
            <select id="single-append-text" class="form-control select2-allow-clear select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                <?php foreach($carrierArray as $row){?>
                    <option value="<?=$row[0]?>"><?=$row[1]?></option>
                <?php } ?>
            </select>
            <span class="input-group-btn">
                <button id="submitCustomer" class="btn info" type="button">Submit</button>
            </span>
        </div>
    </div>
</div>
<hr>

<script>
    var change;
    var detailsRefresh = function(){
        data = "ID=+<?=$ID?>";
        $.ajax({
            url: "/assets/php/modules/job/details/module.php",
            method: "POST",
            data: data,
            success: function (response) {
                $("#details").html(response);
                $("#alert").fadeIn().fadeOut(4000);
            }
        });
    };
    $(function(){
        $(".fa-pencil").click(function(){
            $(this).parent().next("div").each(function(){
                if($(this).hasClass("active")){}
            });
            if($(this).parent().next("div").hasClass("active")){
                $(this).parent().next("div").removeClass("active");
                $(this).parent().next("div").css({"height":"0","overflow":"hidden"});
            } else {
                $(this).parent().next("div").addClass("active");
                $(this).parent().next("div").css({"height":"80px","overflow":"show"});

            }
        });
        $("#submitName").click(function(){
            change = $(this).parent().prev("input").val();
            data = "selection=name&ID=<?=$ID?>&change="+change;
            $.ajax({
                url: "/assets/php/modules/job/details/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>Name Successfully Changed</p>").fadeIn().fadeOut(4000);
                        detailsRefresh();
                    } else if(response === "Already Created"){
                        $("#error").html("<p>This Name Already Exists, Please Enter Another Name</p>").fadeIn().fadeOut(4000);
                    } else {
                        console.log(response);
                    }

                }
            });
        });
        $("#submitGPS").click(function(){
            change = $(this).parent().prev("input").val();
            data = "selection=gps&ID=<?=$ID?>&change="+change;
            console.log(data);
            $.ajax({
                url: "/assets/php/modules/job/details/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>Coordinates Successfully Changed</p>").fadeIn().fadeOut(4000);
                        detailsRefresh();
                    } else {
                        console.log(response);
                    }

                }
            });
        });
        $("#submitCustomer").click(function(){
            change = $(this).parent().prev("select").val();
            data = "selection=customer&ID=<?=$ID?>&change="+change;
            console.log(data);
            $.ajax({
                url: "/assets/php/modules/job/details/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>Customer Successfully Changed</p>").fadeIn().fadeOut(4000);
                        detailsRefresh();
                    } else {
                        console.log(response);
                    }

                }
            });
        });
    });
</script>
<?php } ?>