<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 12/6/17
 * Time: 12:13 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $carrier = $_POST['carrier'];
    $query = "SELECT ID,name FROM trucks WHERE carrierID='$carrier'";
    $carrierArray = mysqli_fetch_all(mysqli_query($db,$query));?>
    <label for="trailer">Truck</label>
    <select id="trailer" name="trailer" class="md-input">
        <option value="0">Optional - Select A Truck</option>
        <?php foreach($carrierArray as $row){?>
            <option value="<?=$row[0]?>"><?=$row[1]?></option>
        <?php } ?>
    </select>
<?php } ?>
