<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/20/17
 * Time: 10:35 AM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $query = "SELECT ID,name FROM jobs";
    $jobArray = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);
    $query = "SELECT ID,name FROM carriers";
    $carrierArray = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);
    $query = "SELECT ID,name FROM origins";
    $originArray = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);
    $ID = $_POST['ID'];
    ?>
    <div class="padding">
        <h2>Create A Load</h2>
        <form class="newLoad">
            <div class="md-form-group float-label">
                <label for="job">Job</label>
                <select id="job" name="job" class="md-input">
                    <?php if(isset($_POST['ID'])){?>
                        <?php foreach($jobArray as $row){
                            if($row['ID'] === $ID){?>
                                <option></option>
                                <option value="<?=$row['ID']?>"><?=$row['name']?></option>
                            <?php } ?>
                        <?php } ?>
                    <?php } else {?>
                        <option></option>
                        <?php foreach($jobArray as $row){ ?>
                            <option value="<?=$row['ID']?>"><?=$row['name']?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
            <div class="md-form-group float-label">
                <label for="origin">Origin</label>
                <select id="origin" name="origin" class="md-input">
                    <option></option>
                    <?php foreach($originArray as $row){ ?>
                        <option value="<?=$row['ID']?>"><?=$row['name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="md-form-group float-label">
                <label for="carrier">Carrier</label>
                <select id="carrier" name="carrier" class="md-input">
                    <option></option>
                    <?php foreach($carrierArray as $row){ ?>
                        <option value="<?=$row['ID']?>"><?=$row['name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div id="driverContainer" class="md-form-group float-label"></div>
            <div id="truckContainer" class="md-form-group float-label"></div>
            <div id="trailerContainer" class="md-form-group float-label"></div>
            <div id="productContainer" class="md-form-group float-label"></div>
            <div class="md-form-group float-label">
                <input id="productPO" name="productPO" class="md-input" required>
                <label for="productPO">Product PO#</label>
            </div>

            <div class="md-form-group float-label">
                <input type="number" id="miles" name="miles" class="md-input" required>
                <label for="miles">Miles</label>
            </div>
            <div class="md-form-group float-label">
                <input type="number" id="allotted" name="allotted" class="md-input" required>
                <label for="allotted">Pounds Allotted</label>
            </div>
            <div class="md-form-group float-label">
                <label for="directions">Directions</label>
                <textarea id="directions" name="directions" class="md-input" required></textarea>
            </div>
            <div class="md-form-group float-label">
                <label for="notes">Notes</label>
                <textarea id="notes" name="notes" class="md-input" required></textarea>
            </div>

            <button class="btn danger" id="submit">Submit</button>
        </form>
    </div>

    <script>
        $(".next").hide();
        $(".newLoad").children("#submit").on("click", function(event){
            event.preventDefault();
            var data = $(".newLoad").serialize();
            console.log(data);
            $.ajax({
                method: "POST",
                data: data,
                url: "/assets/php/modules/load/create/controller.php",
                success: function(response){
                    $("#alert").html("<p>Load Created</p>").fadeIn().fadeOut(4000);
                    window.location.href = "/dashboard/loads/details/?ID="+response;
                }
            });
        });
        $("#job").change(function(){
            var data = "job="+$(this).val();
            console.log(data);
            $.ajax({
                method: "POST",
                data: data,
                url: "/assets/php/modules/load/create/productController.php",
                success: function(response){
                    $("#productContainer").html(response);
                }
            });
        });
        $("#carrier").change(function() {
            var data = "carrier="+$(this).val();
            console.log(data);
            $.ajax({
                method: "POST",
                data: data,
                url: "/assets/php/modules/load/create/driverModule.php",
                success: function (response) {
                    $("#driverContainer").html(response);
                }
            });
            $.ajax({
                method: "POST",
                data: data,
                url: "/assets/php/modules/load/create/truckModule.php",
                success: function (response) {
                    $("#truckContainer").html(response);
                }
            });
            $.ajax({
                method: "POST",
                data: data,
                url: "/assets/php/modules/load/create/trailerModule.php",
                success: function (response) {
                    $("#trailerContainer").html(response);
                }
            });
        });

    </script>

<?php } ?>
