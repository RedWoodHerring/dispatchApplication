<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/20/17
 * Time: 12:49 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $job = $_POST['job'];
    $query = "SELECT productID,totalRequired,currentQuantity FROM jobProduct WHERE jobID='$job'";
    $jobArray = mysqli_fetch_all(mysqli_query($db,$query));?>
    <label for="product">Product</label>
    <select id="product" name="product" class="md-input">
        <option value="0">Please Select A Product</option>
    <?php foreach($jobArray as $row){
        $query = "SELECT ID,name FROM products WHERE ID='".$row[0]."'";
        $product = mysqli_fetch_array(mysqli_query($db,$query));
        ?>
        <option value="<?=$product[0]?>"><?=$product[1]." Total ".$row[1]."-lbs. (Current ".$row[2]."-lbs.)"?></option>
    <?php } ?>
    </select>
<?php } ?>
