<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 12/6/17
 * Time: 12:13 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $carrier = $_POST['carrier'];
    $query = "SELECT ID,name FROM drivers WHERE carrierID='$carrier' AND status='1' AND jobStatus='0'";
    $carrierArray = mysqli_fetch_all(mysqli_query($db,$query));?>
    <label for="driver">Drivers</label>
    <select id="driver" name="driver" class="md-input">
        <option value="0">Optional - Select A Driver</option>
        <?php foreach($carrierArray as $row){?>
            <option value="<?=$row[0]?>"><?=$row[1]?></option>
        <?php } ?>
    </select>
<?php } ?>
