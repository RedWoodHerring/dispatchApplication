<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/21/17
 * Time: 3:21 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $ID = $_POST["ID"];
    $query = "SELECT jobProduct.ID,jobProduct.totalRequired,origins.name,jobProduct.miles,products.name,jobProduct.currentQuantity FROM jobProduct JOIN origins ON jobProduct.originID=origins.ID JOIN products ON jobProduct.productID=products.ID WHERE jobID='$ID'";
    $array = mysqli_fetch_all(mysqli_query($db,$query));?>
    <div class="row">
        <?php foreach($array as $row){?>
        <div class="padding col">
            <div class="box">
                <div class="padding col jobSectionInfo">
                    <div class="row">
                        <h6 class="col">Product:</h6>
                        <p class="col"><?=$row[4]?></p>
                    </div>
                    <div class="row">
                        <h6 class="col">Origin:</h6>
                        <p class="col"><?=$row[2]?></p>
                    </div>
                    <div class="row">
                        <h6 class="col">Quantity Required:</h6>
                        <p class="col"><?=$row[1]?></p>
                    </div>
                    <div class="row">
                        <h6 class="col">Currently Delivered:</h6>
                        <p class="col"><?=$row[5]?></p>
                    </div>
                    <?php
                        $percent = $row[5]/$row[1];
                        $percent_friendly = number_format( $percent * 100, 0 ) . '%';
                        if($percent === 1){?>
                            <i class="checkmark material-icons md-24"></i>
                        <?php }  ?>
                    <div class="progress mb-4">
                        <div class="progress-bar progress-bar-striped progress-bar-animated success" style="width: <?=$percent_friendly?>;"><?=$percent_friendly?></div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    </div>
    <script>
        $(function(){
            $("#jobSectionInfo").click(function(){

            });
        });
    </script>
    <style>
        .checkmark{
            color: green;
            position: absolute;
            top: -10px;
            right: -10px;
        }
        .jobSectionInfo{
            cursor: pointer;
        }
        .jobSectionInfo:hover{
            box-shadow: #000 0 1px 10px;
        }
        .jobSectionInfo p{
            text-align: right;
        }
    </style>
<?php } ?>