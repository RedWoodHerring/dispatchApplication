<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/21/17
 * Time: 8:52 AM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    if(isset($_POST['status'])){
        $status = $_POST['status'];
        $query = "SELECT jobs.ID,jobs.name,customers.name,jobs.gps FROM jobs JOIN customers ON jobs.customerID=customers.ID WHERE status='$status'";
    } else {
        $query = "SELECT jobs.ID,jobs.name,customers.name,jobs.gps FROM jobs JOIN customers ON jobs.customerID=customers.ID WHERE status='0'";
    }
    $array = mysqli_fetch_all(mysqli_query($db,$query)); ?>

    <div id="jobTable">
        <h2 class="tableHeader text-center">Job Table</h2>
        <p class="refreshIcon"></p>
        <button id="activeJobs" class="btn info">Active</button>
        <button id="completedJobs" class="btn info">Completed</button>
        <table class="table table-striped table-bordered dataTable">
            <thead>
            <tr>
                <th>Name</th>
                <th>Customer</th>
                <th>GPS</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($array as $rowNum => $row){?>
                <tr id="<?=$row[0]?>">
                    <?php foreach($row as $key => $value){?>
                        <?php if($key === 0){}else if($key === 'userID'){
                            $query = "SELECT email FROM users WHERE ID='$value'";
                            $userInfo = mysqli_fetch_assoc(mysqli_query($db,$query));
                            ?>
                            <td class="<?=$key?>"><?=$userInfo['email']?></td>
                        <?php }else{ ?>
                            <td class="<?=$key?>"><?= $value ?></td>
                        <?php } ?>
                    <?php } ?>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <script>
        var jobTable = $("#jobTable");
        var jobActive = $("#activeJobs");
        var jobCompleted = $("#completedJobs");
        var jobTableRefresh = function(status){
            var success = function(response){jobTable.parent().html(response);};
            ajax("/assets/php/modules/job/table/module.php","status="+status,success);
        };

        jobTable.children("table").dataTable();

        <?php if($status === "0"){?>
            jobActive.addClass("disabled");
            jobTable.on('click', '.refreshIcon', function(){
                jobTableRefresh("0");
            });
        <?php } else{ ?>
            jobCompleted.addClass("disabled");
            jobTable.on('click', '.refreshIcon', function(){
                jobTableRefresh("1");
            });
        <?php } ?>

        jobTable.on('click', '#activeJobs', function(){
            jobActive.addClass("disabled");
            jobCompleted.removeClass("disabled");
            jobTableRefresh("0");
        });

        jobTable.on('click', '#completedJobs', function(){
            jobActive.removeClass("disabled");
            jobCompleted.addClass("disabled");
            jobTableRefresh("1");
        });

        $(".box").on('click', 'table tbody tr', function(){
            id = $(this).attr("id");
            window.location.href = "/dashboard/jobs/details/?ID="+id;
        });
    </script>
<?php } ?>
