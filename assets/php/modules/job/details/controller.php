<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/9/17
 * Time: 12:17 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/connect.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/functions/changelog.php");

    $ID = mysqli_real_escape_string($db, $_POST['ID']);
    $change = mysqli_real_escape_string($db, $_POST['change']);
    $selection = mysqli_real_escape_string($db, $_POST['selection']);
    $check = 0;

    if($selection === "name"){
        $checkQuery = "SELECT * FROM jobs WHERE name='$change' AND status='0'";
        if(mysqli_num_rows(mysqli_query($db,$checkQuery)) === 0){
            $query = "UPDATE jobs SET name='$change' WHERE ID='$ID'";
            if(mysqli_query($db,$query)){
                changelog($db,$query);
            } else {
                $check = 1;
            }
        } else {
            $check = 2;
        }
    }

    if($selection === "gps"){
        $query = "UPDATE jobs SET gps='$change' WHERE ID='$ID'";
        if(mysqli_query($db,$query)){
            changelog($db,$query);
        } else {
            $check = 1;
        }
    }

    if($selection === "customer"){
        $query = "UPDATE jobs SET customerID='$change' WHERE ID='$ID'";
        if(mysqli_query($db,$query)){
            changelog($db,$query);
        } else {
            $check = 1;
        }
    }

    if($check === 0){
        echo "true";
    } else if($check === 1){
        echo mysqli_error($db);
    } else if($check === 2){
        echo "Already Created";
    }
}

