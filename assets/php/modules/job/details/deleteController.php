<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/21/17
 * Time: 12:52 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/connect.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/functions/changelog.php");

    $ID = mysqli_real_escape_string($db, $_POST['ID']);
    $check = 0;

    $query = "DELETE FROM jobs WHERE ID='$ID'";
    if(mysqli_query($db,$query)){
        changelog($db,$query);
    } else {
        $check = 1;
    }

    if($check === 0){
        echo "true";
    } else if($check === 1){
        echo mysqli_error($db);
    } else if($check === 2){
        echo "Already Created";
    }
}