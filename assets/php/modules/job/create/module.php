<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/17/17
 * Time: 10:23 AM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $query = "SELECT ID,name FROM customers";
    $customerArray = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);
?>
    <div class="padding">
        <h2>Create A Job</h2>
        <form class="newJob">
            <div class="md-form-group float-label">
                <label for="customer">Customer</label>
                <select id="customer" name="customer" class="md-input">
                    <?php foreach($customerArray as $row){ ?>
                        <option value="<?=$row['ID']?>"><?=$row['name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="md-form-group float-label">
                <input id="wellName" name="wellName" class="md-input" required>
                <label for="wellName">Well Name</label>
            </div>
            <div class="md-form-group float-label">
                <input id="gps" name="gps" class="md-input" required>
                <label for="gps">GPS Coordinates</label>
            </div>
            <button id="addProduct" class="info btn">Add Product</button>
            <div id="productsContainer">
                <div id="productsSubmitted">
                    <table>
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Total Required</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div id="productsForm"></div>
            </div>
            <button class="btn danger center-block" id="submit" style="margin-top:3em;">Submit</button>
        </form>
    </div>

    <script>
        $("#productsSubmitted").hide();
        var count = 0;
        $("#addProduct").click(function(event){
            event.preventDefault();
            $.ajax({
                method: "POST",
                data: "count="+count,
                url: "/assets/php/modules/job/create/productModule.php",
                success: function(response){
                    if(response) {
                        $(".next").hide();
                        $(".cancel").hide();
                        $(".modal-body").html(response);
                        $(".modal-header").html("Job Creation");
                        $("#modal").modal("show");
                        count++;
                    }
                }
            });
        });
        $(".close").click(function(){
            $(".next").show();
            $(".cancel").show();
            $(".modal-body").html("");
            $(".modal-header").html("");
            $("#modal").modal("hide");
        });
        $(".modal-footer").children("#submit").on('click', function(event){
            event.preventDefault();
            var value = $('#product').val();
            var text = $("#product option[value='"+value+"']").text();
            var product = "<td id='"+value+"'>"+text;+"</td>";
            var total = "<td>"+$("#required").val()+"</td>";
            $("#productsSubmitted").children('table').children('tbody').append("<tr id='"+count+"'>"+product+total+"</tr>");
            $("#productsSubmitted").children("table").attr("id", count);
            console.log(text);
            $(".modal-body").html("");
            $(".modal-header").html("");
            $("#modal").modal("hide");
            $("#productsSubmitted").show();
        });
        $(".newJob").children("#submit").on('click', function(event){
            event.preventDefault();
            var main = $(".newJob").serialize();
            $.ajax({
                method: "POST",
                data: main,
                url: "/assets/php/modules/job/create/controller.php",
                success: function(response){
                    if(response) {
                        $("#productsSubmitted").children("table").children("tbody").children("tr").each(function(){
                            var product = $(this).children(":nth-child(1)").attr('id');
                            var total = $(this).children(":nth-child(2)").html();
                            $.ajax({
                                method: "POST",
                                data: "jobID="+response+"&total="+total+"&product="+product,
                                url: "/assets/php/modules/job/create/productController.php",
                                success: function(){
                                    window.location.href = "/dashboard/jobs/details/?ID="+response;
                                }
                            });
                        });
                    }
                }
            });
            //$("#productsSubmitted").children("table").children("tr").each(function(){

            //});
            //var data = main+""
        });
    </script>

<?php } ?>
