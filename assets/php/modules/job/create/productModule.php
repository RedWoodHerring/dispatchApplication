<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/17/17
 * Time: 10:30 AM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $query = "SELECT ID,name FROM products";
    $productArray = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);
    $query = "SELECT ID,name FROM origins";
    $originArray = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);
    ?>
    <div class="md-form-group float-label">
        <label for="product">Product</label>
        <select id="product" name="product" class="md-input">
            <?php foreach($productArray as $row){ ?>
                <option value="<?=$row['ID']?>"><?=$row['name']?></option>
            <?php } ?>
        </select>
    </div>
    <div class="md-form-group float-label">
        <input type="number" id="required" name="required" class="md-input" required>
        <label for="required">Total Required</label>
    </div>
    <script>

    </script>
<?php } ?>