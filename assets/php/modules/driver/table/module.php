<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/9/17
 * Time: 2:34 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else{
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    if(isset($_POST['ID'])){
        $ID = $_POST['ID'];
        $query = "SELECT drivers.ID,drivers.name,users.email,carriers.name,drivers.jobStatus FROM drivers JOIN users ON drivers.userID = users.ID JOIN carriers ON drivers.carrierID = carriers.ID WHERE drivers.carrierID='$ID'";
        $array = mysqli_fetch_all(mysqli_query($db, $query));
    } else if(isset($_POST['status'])){
        $status = $_POST['status'];
        if($status === "-1"){
            $query = "SELECT drivers.ID,drivers.name,users.email,carriers.name,drivers.jobStatus,drivers.status FROM drivers JOIN users ON drivers.userID=users.ID JOIN carriers ON drivers.carrierID=carriers.ID WHERE drivers.status='0'";
        } else {
            $query = "SELECT drivers.ID,drivers.name,users.email,carriers.name,drivers.jobStatus,drivers.status FROM drivers JOIN users ON drivers.userID=users.ID JOIN carriers ON drivers.carrierID=carriers.ID WHERE drivers.jobStatus='$status' AND drivers.status='1'";
        }
        $array = mysqli_fetch_all(mysqli_query($db, $query));
    } else {
        $query = "SELECT drivers.ID,drivers.name,users.email,carriers.name,drivers.jobStatus FROM drivers JOIN users ON drivers.userID = users.ID JOIN carriers ON drivers.carrierID = carriers.ID";
        $array = mysqli_fetch_all(mysqli_query($db, $query));
    }
    ?>
        <div id="driverTable">
            <h2 class="tableHeader text-center">Drivers Table</h2>
            <p class="refreshIcon"></p>
            <button id="activeDriver" class="btn info">Available</button>
            <button id="waitingDriver" class="btn info">Waiting On Load Approval</button>
            <button id="onLoadDriver" class="btn info">On Load</button>
            <button id="unavailableDriver" class="btn info">Unavailable</button>
            <table class="table table-striped table-bordered dataTable">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Carrier</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($array as $rowNum => $row){?>
                    <tr id="<?=$row[0]?>">
                        <?php foreach($row as $key => $value){?>
                            <?php if($key === 0 || $key === 5){}else if($key === 4){
                                if($value === '0'){
                                    if($row[5] === "0"){?>
                                        <td class="<?=$key?> disabled"><b>Unavailable</b></td>
                                    <?php }else{ ?>
                                        <td class="<?=$key?> info"><b>Waiting On Job</b></td>
                                    <?php } ?>
                                <?php }else if($value === '1'){ ?>
                                    <td class="<?=$key?> success"><b>Waiting on Job Approval</b></td>
                                <?php }else if($value === '2'){ ?>
                                    <td class="<?=$key?> black"><b>On Job</b></td>
                                <?php } ?>
                            <?php }else{ ?>
                                <td class="<?=$key?>"><?= $value ?></td>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

        <script>
            var driverTable = $("#driverTable");
            var driverActive = $("#activeDriver");
            var driverWaiting = $("#waitingDriver");
            var driverOnLoad = $("#onLoadDriver");
            var driverUnavailable = $("#unavailableDriver");
            var driverTableRefresh = function(status){
                var success = function(response){driverTable.parent().html(response);};
                ajax("/assets/php/modules/driver/table/module.php","status="+status,success);
            };

            driverTable.children("table").dataTable();

            <?php if($status === "0"){?>
                driverActive.addClass("disabled");
                driverTable.on('click', '.refreshIcon', function(){
                    driverTableRefresh("0");
                });
            <?php } else if($status === "1"){ ?>
                driverWaiting.addClass("disabled");
                driverTable.on('click', '.refreshIcon', function(){
                    driverTableRefresh("1");
                });
            <?php } else if($status === "2"){ ?>
                driverOnLoad.addClass("disabled");
                driverTable.on('click', '.refreshIcon', function(){
                    driverTableRefresh("1");
                });
            <?php } else if($status === "-1"){ ?>
                driverUnavailable.addClass("disabled");
                driverTable.on('click', '.refreshIcon', function(){
                    driverTableRefresh("-1");
                });
            <?php } ?>

            driverTable.on('click', '#activeDriver', function(){
                driverTableRefresh("0");
            });

            driverTable.on('click', '#waitingDriver', function(){
                driverTableRefresh("1");
            });

            driverTable.on('click', '#onLoadDriver', function(){
                driverTableRefresh("2");
            });

            driverTable.on('click', '#unavailableDriver', function(){
                driverTableRefresh("-1");
            });

            driverTable.on('click', "table tbody tr", function(){
                var id = $(this).attr("id");
                window.location.href = "/dashboard/drivers/details/?ID="+id;
            });
        </script>
    <?php } ?>