<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/9/17
 * Time: 12:17 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/connect.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/functions/changelog.php");

    $ID = mysqli_real_escape_string($db, $_POST['ID']);
    $change = mysqli_real_escape_string($db, $_POST['change']);
    $selection = mysqli_real_escape_string($db, $_POST['selection']);
    $check = 0;
    $query = "SELECT * FROM drivers WHERE ID='$ID'";
    $driverInfo = mysqli_fetch_assoc(mysqli_query($db,$query));

    if($selection === "name"){
        $checkQuery = "SELECT * FROM drivers WHERE name='$change'";
        if(mysqli_num_rows(mysqli_query($db,$checkQuery)) === 0){
            $query = "UPDATE drivers SET name='$change' WHERE ID='$ID'";
            if(mysqli_query($db,$query)){
                changelog($db,$query);
            } else {
                $check = 1;
            }
            $query = "UPDATE users SET name='$change' WHERE ID='".$driverInfo['userID']."'";
            if(mysqli_query($db,$query)){
                changelog($db,$query);
            } else {
                $check = 1;
            }
        } else {
            $check = 2;
        }
    }

    if($selection === "carrier"){
        $query = "UPDATE drivers SET carrierID='$change' WHERE ID='$ID'";
        if(mysqli_query($db,$query)){
            changelog($db,$query);
        } else {
            $check = 1;
        }
    }

    if($selection === "email"){
        $checkQuery = "SELECT * FROM users WHERE email='$change'";
        if(mysqli_num_rows(mysqli_query($db,$checkQuery)) === 0){
            $query = "UPDATE users SET email='$change' WHERE ID='".$driverInfo['userID']."'";
            if(mysqli_query($db,$query)){
                changelog($db,$query);
            } else {
                $check = 1;
            }
        } else {
            $check = 2;
        }
    }

    if($check === 0){
        echo "true";
    } else if($check === 1){
        echo mysqli_error($db);
    } else if($check === 2){
        echo "Already Created";
    }
}

