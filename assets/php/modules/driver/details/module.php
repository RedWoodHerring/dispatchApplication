<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 2:01 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/connect.php");
    $ID = $_POST['ID'];
    $query = "SELECT drivers.name,carriers.name,users.email,drivers.carrierID FROM drivers JOIN carriers ON drivers.carrierID=carriers.ID JOIN users ON drivers.userID=users.ID WHERE drivers.ID='$ID'";
    $array = mysqli_fetch_all(mysqli_query($db, $query));
    $query = "SELECT * FROM carriers";
    $carrierArray = mysqli_fetch_all(mysqli_query($db, $query));
    ?>

    <div class="row detailContainer">
        <h4>Name: </h4>
        <p><?= $array[0][0] ?></p>
        <i class="fa fa-pencil editName"></i>
    </div>
    <div class="row editItem hidden">
        <div class="input-group row">
            <input class="form-control" placeholder="Input New Name Here">
            <span class="input-group-btn">
        <button id="submitName" class="btn info" type="button">Submit</button>
      </span>
            <span class="error"></span>
        </div>
    </div>
    <hr>
    <div class="row detailContainer">
        <h4>Carrier:</h4>
        <p><?= $array[0][1] ?></p>
        <i class="fa fa-pencil editState"></i>
    </div>
    <div class="row editItem hidden">
        <div class="input-group select2-bootstrap-append">
            <select id="single-append-text" class="form-control select2-allow-clear select2-hidden-accessible"
                    tabindex="-1" aria-hidden="true">
                <?php foreach ($carrierArray as $rowNum => $row) {
                    if ($row[0] === $array[0][3]) {
                        ?>
                        <option value="<?= $row[0] ?>" selected><?= $row[1] ?></option>
                    <?php } else { ?>
                        <option value="<?= $row[0] ?>"><?= $row[1] ?></option>
                    <?php } ?>
                <?php } ?>
            </select>
            <span class="input-group-btn">
                <button id="submitCarrier" class="btn info" type="button">Submit</button>
            </span>
        </div>
    </div>
    <hr>
    <div class="row detailContainer">
        <h4>Email:</h4>
        <p><?= $array[0][2] ?></p>
        <i class="fa fa-pencil editEmail"></i>
    </div>
    <div class="row editItem">
        <div class="input-group row">
            <input class="form-control" placeholder="Input New Email Here">
            <span class="input-group-btn">
                <button id="submitCarrier" class="btn info" type="button">Submit</button>
            </span>
            <span class="error"></span>
        </div>
    </div>

    <script>
        var change;
        var detailsRefresh = function () {
            data = "ID=+<?=$ID?>";
            $.ajax({
                url: "/assets/php/modules/driver/details/module.php",
                method: "POST",
                data: data,
                success: function (response) {
                    $("#details").html(response);
                    $("#alert").fadeIn().fadeOut(4000);
                }
            });
        };
        $(function () {
            $(".fa-pencil").click(function () {
                $(this).parent().next("div").each(function () {
                    if ($(this).hasClass("active")) {
                    }
                });
                if ($(this).parent().next("div").hasClass("active")) {
                    $(this).parent().next("div").removeClass("active");
                    $(this).parent().next("div").css({"height": "0", "overflow": "hidden"});
                } else {
                    $(this).parent().next("div").addClass("active");
                    $(this).parent().next("div").css({"height": "80px", "overflow": "show"});

                }
            });
            $("#submitName").click(function () {
                change = $(this).parent().prev("input").val();
                data = "selection=name&ID=<?=$ID?>&change=" + change;
                $.ajax({
                    url: "/assets/php/modules/driver/details/controller.php",
                    method: "POST",
                    data: data,
                    success: function (response) {
                        if (response === "true") {
                            $("#alert").html("<p>Name Successfully Changed</p>");
                            detailsRefresh();
                        } else if (response === "Already Created") {
                            $("#error").html("<p>This Name Already Exists, Please Enter Another Name</p>").fadeIn().fadeOut(4000);
                        } else {
                            console.log(response);
                        }

                    }
                });
            });
            $("#submitEmail").click(function () {
                change = $(this).parent().prev("input").val();
                data = "selection=email&ID=<?=$ID?>&change=" + change;
                $.ajax({
                    url: "/assets/php/modules/driver/details/controller.php",
                    method: "POST",
                    data: data,
                    success: function (response) {
                        if (response === "true") {
                            $("#alert").html("<p>Email Successfully Changed</p>");
                            detailsRefresh();
                        } else if (response === "Already Created") {
                            $("#error").html("<p>This Email Already Exists, Please Enter Another</p>").fadeIn().fadeOut(4000);
                        } else {
                            console.log(response);
                        }

                    }
                });
            });
            $("#submitCarrier").click(function () {
                change = $(this).parent().prev("select").val();
                data = "selection=carrier&ID=<?=$ID?>&change=" + change;
                $.ajax({
                    url: "/assets/php/modules/driver/details/controller.php",
                    method: "POST",
                    data: data,
                    success: function (response) {
                        if (response === "true") {
                            $("#alert").html("<p>Carrier Successfully Changed</p>");
                            detailsRefresh();
                        } else {
                            console.log(response);
                        }

                    }
                });
            });
        });
    </script>

<?php } ?>