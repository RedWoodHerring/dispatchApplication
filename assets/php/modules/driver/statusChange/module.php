<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 12/5/17
 * Time: 8:27 AM
 */

?>
<form id="statusForm">
    <label for="status">Please Select a Status</label>
    <select id="status" name="status" class="md-input" required>
        <option>Status</option>
        <option value="0">Not Available</option>
        <option value="1">Available</option>
    </select>
</form>

<script>
    $("#submit").click(function(){
        var html = $("#status").html();
        if($("#status").val().length === 1){
            var data = "id="+$(".modal-body").attr("id")+"&status="+$("#status").val();
            $.ajax({
                method: "POST",
                data: data,
                url: "/assets/php/modules/driver/statusChange/controller.php",
                success: function(response){
                    if(response === "true") {
                        $("#modal").modal("hide");
                        $(".modal-body").html("");
                        $(".modal-header").html("");
                        window.location.href = "/dashboard";
                        $("#alert").html("<p>Your Status Has Been Changed</p>").fadeIn().fadeOut(4000);
                    } else {
                        console.log(response);
                    }
                }
            });
        }
    });
    $(".close").click(function(){
        $(".modal-header").html("");
        $(".modal-body").html("");
        $("#modal").modal("hide");
    });
</script>