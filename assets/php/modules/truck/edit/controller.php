<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 12/8/17
 * Time: 2:31 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/connect.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/functions/changelog.php");

    $ID = mysqli_real_escape_string($db, $_POST['ID']);
    $delete = mysqli_real_escape_string($db, $_POST['delete']);

    if (isset($_POST['delete'])) {
        $query = "DELETE FROM trucks WHERE ID='$ID'";
        if (mysqli_query($db, $query)) {
            echo "true";
            changelog($db,$query);
        } else {
            echo "false";
        }
    } else if (isset($ID)) {
        $value = mysqli_real_escape_string($db, $_POST['value']);
        $column = mysqli_real_escape_string($db, $_POST['column']);
        $query = "UPDATE trucks SET $column='$value' WHERE ID='$ID'";
        if (mysqli_query($db, $query)) {
            echo "true";
            changelog($db,$query);
        } else {
            echo "false";
        }
    }
}