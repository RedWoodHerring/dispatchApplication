<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/9/17
 * Time: 2:34 PM
 */
session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $query = "SELECT trailers.ID,trailers.name,trucks.name,carriers.name FROM trailers JOIN trucks ON trailers.truckID = trucks.ID JOIN carriers ON trailers.carrierID = carriers.ID";
    $array = mysqli_fetch_all(mysqli_query($db,$query));
?>
    <div id="trailerTable">
        <h2 class="tableHeader text-center">Trailers Table</h2>
        <p class="refreshIcon"></p>
        <table class="table table-striped table-bordered dataTable">
            <thead>
            <tr>
                <th>Name</th>
                <th>Truck</th>
                <th>Carrier</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($array as $rowNum => $row){?>
                <tr id="<?=$row[0]?>">
                    <?php foreach($row as $key => $value){?>
                        <?php if($key === 0){}else if($key === 'password'){?>
                            <td class="<?=$key?>"><button class="btn btn-sm white pChange">Change Password</button></td>
                        <?php }else{ ?>
                            <td class="<?=$key?>"><?= $value ?></td>
                        <?php } ?>
                    <?php } ?>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>


    <script>
        $("table").dataTable();
        $(".box").on('click', 'table tbody tr', function(){
            id = $(this).attr("id");
            console.log(id);
            $.ajax({
                url: "/assets/php/modules/trailer/edit/module.php",
                method: "POST",
                data: "ID="+id,
                success: function (response) {
                    $("#submit").hide();
                    $(".next").hide();
                    $(".cancel").hide();
                    $(".modal-header").html("Edit Truck");
                    $(".modal-body").html(response);
                    $(".modal-body").attr("id",id);
                    $("#modal").modal("show");
                }
            });
        });
        $("#trailerTable").on('click', '.refreshIcon', function(){
            $.ajax({
                url: "/assets/php/modules/trailer/table/module.php",
                method: "GET",
                success: function (response) {
                    $("#trailerTable").parent().html(response);
                    $(".refreshIcon").css("transform","rotateZ(360deg)");
                }
            });
        });
    </script>

<?php } ?>