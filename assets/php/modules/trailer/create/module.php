<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/9/17
 * Time: 2:54 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $query = "SELECT * FROM carriers";
    $array = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);
?>

    <form id="newTrailer">
        <div class="md-form-group float-label">
            <input id="name" name="name" class="md-input" required>
            <label for="name">Name</label>
        </div>
        <div class="md-form-group float-label">
            <select id="carrier" name="carrier" class="md-input" required>
                <?php if(!isset($_POST['ID'])){?>
                    <option>Select a Carrier</option>
                <?php } ?>
                <?php foreach($array as $row){
                    if(isset($_POST['ID'])){
                        $ID = $_POST['ID'];
                        if($row['ID'] === $ID){?>
                            <option value="<?=$row['ID']?>"><?=$row['name']?></option>
                        <?php } ?>
                    <?php } else{ ?>
                        <option value="<?=$row['ID']?>"><?=$row['name']?></option>
                    <?php } ?>
                <?php } ?>
            </select>
        </div>
        <div id="trucksContainer" class="md-form-group float-label">
        </div>
    </form>

    <script>
        $(function(){
            $(".next").hide();
            $(".cancel").hide();
            $("#carrier").change(function(){
                var carrier = $(this).val();
                if(carrier !== "Select a Carrier") {
                    $.ajax({
                        url: "/assets/php/modules/trailer/create/moduleTrucks.php",
                        data: "carrier=" + carrier,
                        method: "POST",
                        success: function (response) {
                            $("#trucksContainer").html(response);
                        }
                    });
                }
            });
            $("#submit").click(function(){
                if($("input[name='name']").val() === ""){
                    $(".modal-error").html("<p>Please Enter A Valid Name</p>").fadeIn().fadeOut(4000);
                } else if($("select[name='carrier']").val() === "Select a Carrier"){
                    $(".modal-error").html("<p>Please Select A Carrier</p>").fadeIn().fadeOut(4000);
                } else {
                    var data = $("#newTrailer").serialize();
                    $.ajax({
                        url: "/assets/php/modules/trailer/create/controller.php",
                        method: "POST",
                        data: data,
                        success: function(response){
                            if(response === "true"){
                                $("#alert").html("<p>A Trailer Has Been Created</p>").fadeIn().fadeOut(4000);
                                $.ajax({
                                    url: "/assets/php/modules/trailer/table/module.php",
                                    method: "GET",
                                    success: function (response) {
                                        $("#block1").html(response);
                                        $('#modal').modal('hide');
                                        $(".modal-header").html("");
                                        $(".modal-body").html("");
                                    }
                                });
                            } else if(response === "Already Created") {
                                $(".modal-error").html("<p>This Trailer's Name Is Already Taken</p>").fadeIn().fadeOut(4000);
                            }

                        }
                    });
                }
            });
            $(".close").click(function(){
                $('#modal').modal('hide');
                $(".modal-header").html("");
                $(".modal-body").html("");
                $(".modal-body").attr('id', "");
                $(".modal-footer").children("#submit").show();
            });
        });
    </script>

<?php } ?>