<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/14/17
 * Time: 4:14 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
    $carrierID = $_POST['carrier'];
    require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
    $query = "SELECT * FROM trucks WHERE carrierID='$carrierID'";
    $array = mysqli_fetch_all(mysqli_query($db,$query),MYSQLI_ASSOC);
?>

<select id="truck" name="truck" class="md-input" required>
    <option>Select a Truck</option>
    <?php foreach($array as $row){?>
        <option value="<?=$row['ID']?>"><?=$row['name']?></option>
    <?php } ?>
</select>
<?php } ?>