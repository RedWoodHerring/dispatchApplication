<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/9/17
 * Time: 2:54 PM
 */

session_start();
if(!isset($_SESSION['login'])) {
    header("location:/");
}else{?>

<form id="newOrigin">
    <div class="md-form-group float-label">
        <input id="name" name="name" class="md-input" required>
        <label for="name">Name</label>
    </div>
    <div class="md-form-group float-label">
        <input id="gps" name="gps" class="md-input" required>
        <label for="gps">GPS Location</label>
    </div>
    <div class="md-form-group float-label">
        <input id="address" name="address" class="md-input" required>
        <label for="address">Street Address</label>
    </div>
    <div class="md-form-group float-label">
        <textarea id="phone" name="phone" class="md-input" required></textarea>
        <label for="phone">Phone/Contact Information</label>
    </div>

</form>

<script>
    $(function(){
        $(".next").hide();
    $(".cancel").hide();
    $("#submit").click(function(){
        if($("input[name='name']").val() === ""){
            $(".modal-error").html("<p>Please Enter A Valid Name</p>").fadeIn().fadeOut(4000);
        } else {
            var data = $("#newOrigin").serialize();
            $.ajax({
                url: "/assets/php/modules/origin/create/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>An Origin Has Been Created</p>").fadeIn().fadeOut(4000);
                        $.ajax({
                            url: "/assets/php/modules/origin/table/module.php",
                            method: "GET",
                            success: function (response) {
                                $("#block1").html(response);
                                $('#modal').modal('hide');
                                $(".modal-header").html("");
                                $(".modal-body").html("");
                            }
                        });
                    } else if(response === "Already Created") {
                        $(".modal-error").html("<p>This Origin's Name Is Already Taken</p>").fadeIn().fadeOut(4000);
                    }

                }
            });
        }
    });
    $(".close").click(function(){
        $('#modal').modal('hide');
        $(".modal-header").html("");
        $(".modal-body").html("");
        $(".modal-body").attr('id', "");
        $(".modal-footer").children("#submit").show();
    });
    });
</script>
<?php } ?>