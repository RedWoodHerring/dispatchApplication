<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/13/17
 * Time: 2:01 PM
 */
require_once ($_SERVER['DOCUMENT_ROOT']."/assets/php/connect.php");
$ID = $_POST['ID'];
$query = "SELECT * FROM origins WHERE ID='$ID'";
$array = mysqli_fetch_assoc(mysqli_query($db,$query));
?>

    <div class="row detailContainer">
        <h4>Name:  </h4>
        <p><?=$array['name']?></p>
        <i class="fa fa-pencil editName"></i>
    </div>
    <div class="row editItem hidden">
        <div class="input-group row">
            <input class="form-control" placeholder="Input New Name Here">
            <span class="input-group-btn">
                <button id="submitName" class="btn info" type="button">Submit</button>
            </span>
        </div>
    </div>
    <div class="row detailContainer">
        <h4>GPS Coordinates:  </h4>
        <p><?=$array['gps']?></p>
        <i class="fa fa-pencil editName"></i>
    </div>
    <div class="row editItem hidden">
        <div class="input-group row">
            <input class="form-control" placeholder="Input New GPS Coordinates Here">
            <span class="input-group-btn">
            <button id="submitGps" class="btn info" type="button">Submit</button>
        </span>
        </div>
    </div>
    <div class="row detailContainer">
        <h4>Street Address:  </h4>
        <p><?=$array['address']?></p>
        <i class="fa fa-pencil editName"></i>
    </div>
    <div class="row editItem hidden">
        <div class="input-group row">
            <input class="form-control" placeholder="Input New Address Here">
            <span class="input-group-btn">
            <button id="submitAddress" class="btn info" type="button">Submit</button>
        </span>
        </div>
    </div>
    <div class="row detailContainer">
        <h4>Phone:  </h4>
        <p><?=$array['phone']?></p>
        <i class="fa fa-pencil editName"></i>
    </div>
    <div class="row editItem hidden">
        <div class="input-group row">
            <input class="form-control" placeholder="Input New Phone Number Here">
            <span class="input-group-btn">
            <button id="submitPhone" class="btn info" type="button">Submit</button>
        </span>
        </div>
    </div>

<script>
    var change;
    var detailsRefresh = function(){
        data = "ID=+<?=$ID?>";
        $.ajax({
            url: "/assets/php/modules/origin/details/module.php",
            method: "POST",
            data: data,
            success: function (response) {
                $("#details").html(response);
                $("#alert").fadeIn().fadeOut(4000);
            }
        });
    };
    $(function(){
        $(".fa-pencil").click(function(){
            $(this).parent().next("div").each(function(){
                if($(this).hasClass("active")){}
            });
            if($(this).parent().next("div").hasClass("active")){
                $(this).parent().next("div").removeClass("active");
                $(this).parent().next("div").css({"height":"0","overflow":"hidden"});
            } else {
                $(this).parent().next("div").addClass("active");
                $(this).parent().next("div").css({"height":"80px","overflow":"show"});
            }
        });
        $("#submitName").click(function(){
            change = $(this).parent().prev("input").val();
            data = "selection=name&ID=<?=$ID?>&change="+change;
            $.ajax({
                url: "/assets/php/modules/origin/details/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>Name Successfully Changed</p>").fadeIn().fadeOut(4000);
                        detailsRefresh();
                    } else if(response === "Already Created"){
                        $(".error").html("This Name Already Exists, Please Enter Another Name").fadeIn().fadeOut(4000);
                    } else {
                        console.log(response);
                    }

                }
            });
        });
        $("#submitGps").click(function(){
            change = $(this).parent().prev("input").val();
            data = "selection=gps&ID=<?=$ID?>&change="+change;
            $.ajax({
                url: "/assets/php/modules/origin/details/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>Coordinates Successfully Changed</p>").fadeIn().fadeOut(4000);
                        detailsRefresh();
                    } else {
                        console.log(response);
                    }

                }
            });
        });
        $("#submitAddress").click(function(){
            change = $(this).parent().prev("input").val();
            data = "selection=address&ID=<?=$ID?>&change="+change;
            $.ajax({
                url: "/assets/php/modules/origin/details/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>Street Address Successfully Changed</p>").fadeIn().fadeOut(4000);
                        detailsRefresh();
                    } else {
                        console.log(response);
                    }

                }
            });
        });
        $("#submitPhone").click(function(){
            change = $(this).parent().prev("input").val();
            data = "selection=phone&ID=<?=$ID?>&change="+change;
            $.ajax({
                url: "/assets/php/modules/origin/details/controller.php",
                method: "POST",
                data: data,
                success: function(response){
                    if(response === "true"){
                        $("#alert").html("<p>Phone Number Successfully Changed</p>").fadeIn().fadeOut(4000);
                        detailsRefresh();
                    } else {
                        console.log(response);
                    }

                }
            });
        });
    });
</script>

<?php
?>