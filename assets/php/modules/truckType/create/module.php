<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/9/17
 * Time: 2:54 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {

?>

    <form id="newTruckType">
        <div class="md-form-group float-label">
            <input id="name" name="name" class="md-input" required>
            <label for="name">Name</label>
        </div>
    </form>

    <script>
        $(function(){
            $(".next").hide();
            $(".cancel").hide();
            $("#submit").click(function(){
                var data = $("#newTruckType").serialize();
                $.ajax({
                    url: "/assets/php/modules/truckType/create/controller.php",
                    method: "POST",
                    data: data,
                    success: function(response){
                        if(response === "true"){
                            $("#alert").html("<p>Truck Type Created</p>").fadeIn().fadeOut(4000);
                            $.ajax({
                                url: "/assets/php/modules/truckType/table/module.php",
                                method: "GET",
                                success: function (response) {
                                    $("#block1").html(response);
                                    $('#modal').modal('hide');
                                    $(".modal-header").html("");
                                    $(".modal-body").html("");
                                }
                            });
                        } else if(response === "Already Created") {
                            $(".modal-error").html("<p>This Type Is Already Taken</p>").fadeIn().fadeOut(4000);
                        }

                    }
                });
            });
            $(".close").click(function(){
                $('#modal').modal('hide');
                $(".modal-header").html("");
                $(".modal-body").html("");
                $(".modal-body").attr('id', "");
                $(".modal-footer").children("#submit").show();
            });
        });
    </script>

<?php } ?>