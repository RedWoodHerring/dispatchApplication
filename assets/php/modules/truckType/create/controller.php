<?php
/**
 * Created by PhpStorm.
 * User: josh.h
 * Date: 11/9/17
 * Time: 3:37 PM
 */

session_start();
if(!isset($_SESSION['login'])){
    header("location:/");
} else {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/connect.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/assets/php/functions/changelog.php");
    $check = 0;
    $created = 0;
    $name = mysqli_real_escape_string($db, $_POST['name']);

    $typeQuery = "SELECT * from truckTypes WHERE name='$name'";

    if(mysqli_num_rows(mysqli_query($db,$typeQuery)) > 0){
        $created = 1;
    }

    if($created === 0){

        $query = "INSERT INTO truckTypes (name) VALUES ('$name')";
        mysqli_query($db,$query);
        changelog($db,$query);
        echo "true";
    } else {
        echo "Already Created";
    }
}